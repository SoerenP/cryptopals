﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using PROFILE = System.Collections.Generic.Dictionary<string, string>;


namespace Crypto
{
    /* since we only use 12 bytes, a simple struct is enough */
    public struct EditResult
    {
        public float normalized_edit_distance;
        public int edit_distance;
        public int keysize;

        public EditResult(float normalized_edit_distance, int edit_distance, int keysize)
        {
            this.normalized_edit_distance = normalized_edit_distance;
            this.edit_distance = edit_distance;
            this.keysize = keysize;
        }
    }

    /* could be a class, but eh.. */
    public struct FreqResult
    {
        public int score;
        public char SingleKeyCharacter;
        public string plaintext;

        public FreqResult(int score, char SingleKeyCharacter, string plaintext)
        {
            this.score = score;
            this.SingleKeyCharacter = SingleKeyCharacter;
            this.plaintext = plaintext;
        }
    }

    public static class Analysis
    {

        public delegate byte[] BlackBoxProfileCreator(string email);
        public delegate PROFILE BlackBoxProfileDecryptor(byte[] data);
        public delegate byte[] BlackBoxEncryptor(byte[] data);
        public delegate byte[] BlackBoxPaddingEncryptor(string data);
        public delegate AESWrapper.CBC.Cookie BlackBoxCookie();
        public delegate bool PaddingOracle(AESWrapper.CBC.Cookie c);



        private static int guessedBlocksize;
        public static int GuessedBlocksize
        {
            get
            {
                return guessedBlocksize;
            }
            set
            {
                guessedBlocksize = value;
            }
        }

        public static byte[] CBCPaddingOracleAttack(AESWrapper.CBC.Cookie cookie, PaddingOracle isPaddingValid, int blocksize)
        {
            AESWrapper.CBC.Cookie workingCopy = new AESWrapper.CBC.Cookie(cookie);

            int blockCount = cookie.ciphertext.Length / blocksize;
            byte[] plaintext = new byte[cookie.ciphertext.Length];

            for(int b = 0; b < blockCount - 1; b++)
            {
                var attackedBlock = workingCopy.ciphertext.Reverse().Skip(b * blocksize).Take(blocksize).Reverse().ToArray();
                var priorBlock = workingCopy.ciphertext.Reverse().Skip((b + 1) * blocksize).Take(blocksize).Reverse().ToArray();

                var plaintextBlock = CBCPaddingAttackOnSingleBlock(attackedBlock, priorBlock, isPaddingValid, blocksize);

                Array.Copy(plaintextBlock, 0, plaintext, plaintext.Length - ((b + 1) * blocksize) , blocksize);
            }

            // Last block needs iv as previous, so handle it seperately. 
            var LastAttackedBlock = workingCopy.ciphertext.Take(blocksize).ToArray();
            var LastPriorBlock = workingCopy.iv;
            var lastPlaintextBlock = CBCPaddingAttackOnSingleBlock(LastAttackedBlock, LastPriorBlock, isPaddingValid, blocksize);
            Array.Copy(lastPlaintextBlock, 0, plaintext, 0, blocksize);
            
            return plaintext;
        }

        /// <summary>
        /// Only works for the last byte of the last block currently. Need to generalize. But you gotta start somewhere, its quite the bit fiddling,
        /// and byte xor in C# is a fickle mistress.. 
        /// </summary>
        /// <param name="cookie"></param>
        /// <param name="isPaddingValid"></param>
        /// <returns></returns>
        public static byte[] CBCPaddingAttackOnSingleBlock(byte[] attackedBlock, byte[] priorBlock, PaddingOracle isPaddingValid, int blocksize)
        {
            byte[] plaintextBlock = new byte[blocksize];

            var forgedCookie = new AESWrapper.CBC.Cookie(attackedBlock, priorBlock);

            int upper = blocksize + 1;

            for (int b = 1; b < upper; b++)
            {
                byte paddingByte = Convert.ToByte(b);
                byte plaintextByte = 0x00;

                // Guess the value of that plaintext byte - when padding is valid, we found it?
                for (short i = 0; i < 256; i++)
                {
                    /* Copy the Ci-1 | Ci */
                    var current = new AESWrapper.CBC.Cookie(forgedCookie);
                    
                    /* increments the current byte under attack */
                    IncrementBlock(current, Convert.ToByte(i), paddingByte, blocksize - b);

                    /* Since we xor onto the padding byte, its only that result that counts. We will also hit the original ciphertext at some point since the padding byte */
                    /* and i will cancel out inside the increment function. So that would give the original padding. We want our padding of 0x01 and so on. */
                    if (isPaddingValid(current) && Convert.ToByte(DetectPaddingByte(current, isPaddingValid)) == paddingByte)
                    {
                        /* Valid padding, save the one that worked (e.g. the R block in the IV is needed for detecting the padding byte */
                        plaintextByte = Convert.ToByte(i);
                        /* for debugging */
                        break;
                    }
                }

                plaintextBlock[blocksize - b] = plaintextByte;

                /* prepare padding for next round, since we now the plaintext of last b bytes, so do the xor yall!*/
                for(int j = 1; j <= b; j++)
                {
                    /* use orig previous block */
                    /* We know that Ci-1 xor AES(Ci) xor plaintextbyte xor 0x01 = 0x01 during decryption. So if we make Ci-1 = Ci-1 xor plaintextbyte xor 0x02 */
                    /* for example, we get 0x02 padding instead of 0x01 */

                    forgedCookie.iv[blocksize - j] = Convert.ToByte(priorBlock[blocksize - j] ^ 
                                                     Convert.ToByte(plaintextBlock[blocksize - j] ^ 
                                                     Convert.ToByte(b + 1))); // next padding byte for next round (current plus one) 

                }
            }

            return plaintextBlock;
        }


        /// <summary>
        /// Assume that there are only two blocks in cookie, namely random | block under attack. So the random block is the IV block. 
        /// </summary>
        /// <param name="cookie"></param>
        /// <param name="i"></param>
        private static void IncrementBlock(AESWrapper.CBC.Cookie cookie, byte i, byte paddingByte, int index)
        {
            cookie.iv[index] ^= i;
            cookie.iv[index] ^= paddingByte;
        }

        private static int DetectPaddingByte(AESWrapper.CBC.Cookie cookie, PaddingOracle isPaddingValid)
        {
            short blocksize = 16;
            
            var workingCopy = new AESWrapper.CBC.Cookie(cookie);

            short terminatingI = 0;
            for(short i = 0; i < blocksize; i++)
            {
                workingCopy.iv[i] += 1;

                if (!isPaddingValid(workingCopy))
                {
                    terminatingI = i;
                    break;
                }
            }

            return blocksize - terminatingI;
        }

        public static PROFILE ProfileForAdminGenerator(BlackBoxProfileCreator oracle, BlackBoxProfileDecryptor decr)
        {
            int blocksize = 16; //should we detech this from the oracle? Probably yes

            string fishing_email = "bbbbbbbbbbadmin\0\0\0\0\0\0\0\0\0\0\0";
            byte[] fishing_email_encrypted = ProfileFactory.ProfileForEncrypted(fishing_email);

            PROFILE fishing_user = ProfileFactory.DecryptAndParseUserCookie(fishing_email_encrypted);

            /* take the second block of the encrypted email */
            byte[] admin_block = fishing_email_encrypted.Skip(blocksize).Take(blocksize).ToArray();
            
            string admin_email = "bacon22@jub.dk";
            byte[] admin_email_encrypted = ProfileFactory.ProfileForEncrypted(admin_email);
            
            // take everything up until role= */
            byte[] final_data = admin_email_encrypted.Take(blocksize*2).Concat(admin_block).ToArray();


            PROFILE admin = ProfileFactory.DecryptAndParseUserCookie(final_data);
            return admin;
        }

        public static AESWrapper.BlockMode ECB_CBC_Oracle(BlackBoxEncryptor oracle)
        {
            /* prepare message */
            int blocksize_bytes = 16;
            int blocks = 6;
            int message_length = blocks * blocksize_bytes;

            byte[] message = Enumerable.Repeat((byte)'A', message_length).ToArray();
            
            byte[] cipher = oracle(message);

            /* ECB is stateless, the same plaintext block will always give the same ciphertext block. So, we should find duplicate blocks in ciphertext */
            /* if we dont, its probably CBC */
            int cipher_length = cipher.Length;
            int cipher_blocks = cipher_length / blocksize_bytes;
            List<byte[]> cipher_blocked = new List<byte[]>();
            for(int i = 0; i < cipher_blocks; i++)
            {
                byte[] temp = new byte[blocksize_bytes];
                for(int j = 0; j < blocksize_bytes; j++)
                {
                    temp[j] = cipher[i * blocksize_bytes + j];
                }
                cipher_blocked.Add(temp);
            }

            int duplicates = 0;
            for(int i = 0; i < cipher_blocks-1; i++)
            {
                byte[] left = cipher_blocked[i];
                byte[] right = cipher_blocked[i + 1];
                int distance = Operations.EditDistance(left, right);
                if (distance == 0) duplicates++;
            }

            AESWrapper.BlockMode res;
            if (duplicates > 0) res = AESWrapper.BlockMode.ECB;
            else res = AESWrapper.BlockMode.CBC;
            return res;
        }

        /// <summary>
        /// Raw is the first character of each transposed block. These were all XORd with the same key bytes. 
        /// So for each of them, we try a key character, and we save the one that results in the most english letters
        /// </summary>
        /// <param name="raw"></param>
        /// <param name="alphabet"></param>
        /// <returns></returns>
        public static FreqResult SingleByteKeyAnalysis(byte[] raw, string alphabet)
        {
            string asc = Encoding.ASCII.GetString(raw);

            int keyspace_size = 255; //255 ascii characters
            char c = (char)0;
            char top_char = c;
            int top_score = 0;
            string top_ascii = "dummy";
            for (int index = 0; index < keyspace_size; index++)
            {
                string temp = new string(c, asc.Length);
                byte[] temp_raw = Encoding.ASCII.GetBytes(temp);
                byte[] res = Operations.FixedBitwiseXOR(raw, temp_raw); 
                string res_ascii = Encoding.ASCII.GetString(res);
                int[] frequencies = Analysis.FrequencyAnalysis(res_ascii,alphabet);
                int score = Analysis.FrequencyScore(frequencies,alphabet);

                if (score >= top_score)
                {
                    top_score = score;
                    top_char = c;
                    top_ascii = res_ascii;
                }

                c++;
            }

            return new FreqResult(top_score,top_char,top_ascii); 
        }

        public static FreqResult SingleByteKeyAnalysis(byte[] raw, List<Tuple<char, int>> weights)
        {
            string asc = Encoding.ASCII.GetString(raw);

            int keyspace_size = 255; //255 ascii characters
            char c = (char)0;
            char top_char = c;
            int top_score = 0;
            string top_ascii = "dummy";
            for (int index = 0; index < keyspace_size; index++)
            {
                string temp = new string(c, asc.Length);
                byte[] temp_raw = Encoding.ASCII.GetBytes(temp);
                byte[] res = Operations.FixedBitwiseXOR(raw, temp_raw);
                string res_ascii = Encoding.ASCII.GetString(res);
                int value = Analysis.FrequencyAnalysisWithWeights(res_ascii, weights);

                if (value >= top_score)
                {
                    top_score = value;
                    top_char = c;
                    top_ascii = res_ascii;
                }

                c++;
            }

            return new FreqResult(top_score, top_char, top_ascii);
        }

        /*
         *  Simply gives 1 point for each character in alphabet if it is found.
         *  We could instead do a dictionary, where a character is rewarded by frequency instead.
         *  And why dont we just reward here when we find a match, based on the match? Instead of returning an int array.. just add up the score here? TODO
         * */
        public static int[] FrequencyAnalysis(string buffer, string alphabet)
        {
            int[] res = new int[alphabet.Length];

            for (int index = 0; index < buffer.Length; index++)
            {
                for (int inner = 0; inner < alphabet.Length; inner++)
                {
                    if (buffer[index] == alphabet[inner])
                    {
                        res[inner]++; //why not just add up a score here? 
                        break;
                    }
                }
            }
            return res;
        }

        public static int FrequencyAnalysisRunningCounter(string buffer, string alphabet)
        {
            int res = 0;

            for (int index = 0; index < buffer.Length; index++)
            {
                for (int inner = 0; inner < alphabet.Length; inner++)
                {
                    if (buffer[index] == alphabet[inner])
                    {
                        res++;
                        break;
                    }
                }
            }
            return res;
        }

        public static int FrequencyAnalysisWithWeights(string buffer, List<Tuple<char, int>> frequencies)
        {
            int res = 0;

            for(int index = 0; index < buffer.Length; index++)
            {
                foreach(var t in frequencies)
                {
                    if(buffer[index] == t.Item1)
                    {
                        res += t.Item2;
                        break;
                    }
                }
            }

            return res;
        }

        /* istf at tælle kunne vi tildele hver karakter en score? */
        private static int FrequencyLookUp(int[] frequencies, string alphabet, char index)
        {
            int final_index = alphabet.IndexOf(index); //what does this return if the thing isnt there? 
            if (final_index >= 0) return frequencies[final_index];
            else return 0;
        }

        public static int FrequencyScore(int[] frequencies, string alphabet)
        {
            int score = 0;
            for (int letter = 0; letter < alphabet.Length; letter++)
            {
                score += Analysis.FrequencyLookUp(frequencies, alphabet, alphabet.ElementAt(letter));
            }

            return score;
        }

        /* Kinda hardcoded values, but they work. Could also try the best X keysizes instead of just the one */
        public static byte[] BreakRepeatingKeyXOR(byte[] inputRaw)
        {
            int keysize_low = 2;
            int keysize_high = 40;
            int blocks = 20;

            EditResult[] results = GuessKeylength(inputRaw, keysize_low, keysize_high, blocks);
            EditResult[] sortedResults = results.OrderBy(i => i.normalized_edit_distance).ToArray();

            int best_keysize = sortedResults[0].keysize;

            return TryKeyLength(inputRaw, best_keysize);
        }
        
        /// <summary>
        /// If the Nonce is zero for all the encryptions of the ciphertexts, they had the same keystream. 
        /// This means that c_i = p_i ^ k_i, and k_i is the same across all ciphertexts
        /// This implies that p_i = c_i ^k_i
        /// 
        /// It is now the same as the fixed XOR breakage - we are not looking for the key. The key is used to encrypt the counter,
        /// which gives us the keystream. What we can find, one block at a time by alligning, is the keystream, and then we can decrypt by an xor.
        /// So, we will not find the secret key (we could) - but what we will find is the plaintext, since we find the entire keystream
        /// upto the length of the smallest ciphertext. 
        /// 
        /// What we do is tranpose, allign the blocks that were XORd with the same keystream, XOR them with all possible bytes as the key,
        /// check the resulting plaintext for frequency of common english letters - the one with the most common is probably the keystream used for that byte.
        /// Do this for all bytes, and decrypt the ciphertexts now you know the keystream (NOT THE KEY). You cannot necc. find the key, you dont know what the nonce was.
        /// Only that it was the same nonce. (not sure if this can be detected - other than if this succeeds.
        /// </summary>
        /// <param name="ciphertexts"></param>
        public static void BreakFixedNonceCTR(List<byte[]> ciphertexts, int truncatedLength)
        {
            var answer = new byte[truncatedLength];

            /* transpose, and break it byte by byte by trying all possible key bytes, and looking at the character frequency */
            var transposed = Converter.TransposeCiphers(ciphertexts);

            for (int keystreamIndex = 0; keystreamIndex < truncatedLength; keystreamIndex++)
            {
                List<Tuple<byte, double>> scores = new List<Tuple<byte, double>>();

                for(int c = 0; c < 256; c++)
                {
                    var temp = Operations.FixedBitwiseXOR(transposed.ElementAt(keystreamIndex), (byte)c);

                    if(temp.Any(b => (b < 32 || b > 128) && b != '\r' && b != '\n'))
                    {
                        continue;
                    }

                    var str = Encoding.UTF8.GetString(temp);

                    var score = ScoreStringByLetterFreq(Encoding.UTF8.GetString(temp)) / temp.Length;

                    scores.Add(new Tuple<byte, double>((byte)c, score));
                }

                var best = scores.OrderByDescending(s => s.Item2).Take(5).ToArray();

                if (best.Any())
                {
                    Console.WriteLine("====== encoded:");
                    foreach (var s in best)
                    {
                        Console.WriteLine("{0}: {1}", s.Item1, s.Item2);
                    }
                    answer[keystreamIndex] = (byte)best.First().Item1;
                }
            }

            if (answer.All(b => b != 0))
            {
                foreach (var m in ciphertexts)
                {
                    var decrypted = Operations.FixedBitwiseXOR(answer, m);
                    Console.WriteLine("{0}", Encoding.UTF8.GetString(decrypted));
                }
            }
        }
        
        public static double ScoreStringByLetterFreq(string str)
        {
            var letterFreq = new[]
            {
                8.2, 1.5, 2.8, 4.3, 12.7, 2.2, 2.0, 6.1, 7.0, 0.2, 0.8, 4.0, 2.4,
                6.7, 7.5, 1.9, 0.1, 6.0, 6.3, 9.1, 2.8, 1.0, 2.4, 0.2, 2.0, 0.1
            };

            return str.ToUpper()
                .ToCharArray()
                .Where(c => (c >= 'A' && c <= 'Z') || c == ' ')
                .Sum(c => c == ' ' ? 10 : letterFreq[c - 'A']); //the -A is a way to index a character to 0 (even though its int value might be 80 or something).
        }

        public static byte[] BreakFixedNonceCTR(byte[] buffer, int keysize, string alphabet)
        {
            return TryKeyLength(buffer, keysize, alphabet);
        }


        /* Once we figure out how big the blocksize is, and how big the prefix is, the rest is just a matter of alligning the blocks */
        /* in fact, we can use the same helper functions as in the easy ECB byte by byte without the prepadding! I didnt even plan for that :D */
        /* Todo: Sometimes the guess blocksize becomes 32 for some reason. Then this fails. But if we guess it correcly (16) then it succeeds */
        /* further, if the prefix padding is only 1, we get division by zero? In the guess blocksize stuff. */
        public static byte[] ECBByteByByteRandomPrefix(BlackBoxEncryptor oracle)
        {
            byte[] target_bytes = new byte[10];

            /* initial analysis */
            AESWrapper.BlockMode oracle_mode = Analysis.ECB_CBC_Oracle(oracle);
            int total_padding_length = oracle(new byte[0]).Length;
            int prefix_pad_length = AESWrapper.RandomPrefixPaddingLength;
            int guessed_blocksize = GuessECBPrefixPaddingBlocksize(oracle); //once in a while it says 32? 
            GuessedBlocksize = guessed_blocksize; //so the test can extract

            int expected_blocksize = 16;
            if (guessed_blocksize != expected_blocksize) guessed_blocksize = expected_blocksize; //just so we dont crash until we track down the bug

            int guessed_prefix_length = GuessECBPrefixPaddedSize(oracle, guessed_blocksize);

            /* we know the prefix length, we can then allign our single block accordingly. And then do as the easy version did */
            int post_pad_length = total_padding_length - guessed_prefix_length;
            int padding_short_by = guessed_blocksize - (guessed_prefix_length % guessed_blocksize);

            byte[] padding_plaintext = new byte[post_pad_length];
            
            for (int i = 0; i < post_pad_length; i++)
            {
                int number_a = guessed_blocksize - 1 - (i % guessed_blocksize) + padding_short_by;
                int bytes_to_skip = (guessed_prefix_length + padding_short_by) + ((i / guessed_blocksize) * guessed_blocksize);
                byte[] a_prefix = Enumerable.Repeat((byte)'A', number_a).ToArray();
                byte[] dict_prefix = a_prefix.Take(number_a).Concat(padding_plaintext.Take(i)).ToArray();
                padding_plaintext[i] = ECBSingleByteExtract(oracle, a_prefix, dict_prefix, guessed_blocksize, bytes_to_skip);
            }
            
            return padding_plaintext;
        }

        /* the attack is described in detail in "crypto 101" page 50 */
        public static byte[] ECBByteByByte(BlackBoxEncryptor oracle)
        {
            /* initial analysis */
            AESWrapper.BlockMode oracle_mode = Analysis.ECB_CBC_Oracle(oracle);
            int max_aes_blocksize = 32;
            int blocksize = GuessECBBlocksize(oracle, max_aes_blocksize);
            int padding_length = oracle(new byte[0]).Length;
            int padding_blocks = padding_length / padding_length;

            /* start doing byte-by-byte for each block of the padding */
            byte[] padding_plaintext = new byte[padding_length];

            for(int i = 0; i < padding_length; i++)
            {
                int NumberA = blocksize - 1 - (i % blocksize);
                int bytes_to_skip = (i + NumberA) / blocksize * blocksize;
                byte[] a_prefix = Enumerable.Repeat((byte)'A', NumberA).ToArray();
                byte[] dict_prefix = a_prefix.Take(NumberA).Concat(padding_plaintext.Take(i)).ToArray();
                padding_plaintext[i] = ECBSingleByteExtract(oracle, a_prefix, dict_prefix, blocksize, bytes_to_skip);

            }

            return padding_plaintext;
        }


        /// <summary>
        /// Give known plaintext P to oracle. We get C = P XOR K back. 
        /// Construct X = P XOR ";admin=true;AAAA". Then calculate X XOR C = P XOR P XOR K XOR ";admin=true;AAAA"
        /// which equals ";admin=true;AAAA" XOR K, which is simply an ecryption of ";admin=true;AAAA", so decrypted and parsed
        /// will return true for admin. Really simple - even easier than CBC since its all inside the same block, since all
        /// blocks are independent. For CBC it was the adjacent block. 
        /// 
        /// We kinda take for granted that we know what block we hit in the ciphertext, but you could easily
        /// send in a lot of text, see length of what you get back, and thus infer what blocks you control
        /// </summary>
        /// <param name="oracle"></param>
        /// <returns></returns>
        public static byte[] CTRBitFlipAttack(BlackBoxPaddingEncryptor oracle)
        {
            int blocksize = 16;

            string inject = "YELLOW SUBMARINE";
            byte[] injectRaw = Encoding.ASCII.GetBytes(inject);

            string payload = ";admin=true;AAAA";
            byte[] payloadRaw = Encoding.ASCII.GetBytes(payload);

            byte[] oracleCipher = oracle(inject);

            var ourBlock = oracleCipher.Skip(blocksize * 2).Take(blocksize).ToArray();
            var hammer = Operations.FixedBitwiseXOR(payloadRaw, injectRaw);
            var result = Operations.FixedBitwiseXOR(ourBlock, hammer);

            var lastpart = oracleCipher.Skip(blocksize * 2).Take(oracleCipher.Length - blocksize * 2).ToArray();

            var preparedCipher = oracleCipher.Take(blocksize * 2).Concat(result).Concat(lastpart).ToArray();

            return preparedCipher;
        }

        public static byte[] CBCBitFlipAttack(BlackBoxPaddingEncryptor oracle)
        {
            int blocksize = 16;            
            string first_block = "YELLOW SUBMARINE";
            byte[] first_block_raw = Encoding.ASCII.GetBytes(first_block);
            
            string second_block = ";admin=true;AAAA";
            byte[] second_block_raw = Encoding.ASCII.GetBytes(second_block);

            byte[] xor_unto_cipher = Operations.FixedBitwiseXOR(first_block_raw, second_block_raw);
            string xor_target = Encoding.ASCII.GetString(xor_unto_cipher);

            byte[] oracled = oracle(first_block);

            byte[] flipped = oracled.Skip(blocksize).Take(blocksize).ToArray();
            flipped = Operations.FixedBitwiseXOR(flipped, xor_unto_cipher);

            byte[] last_part = oracled.Skip(blocksize * 2).Take(oracled.Length-blocksize*2).ToArray();

            byte[] prepared_cipher = oracled.Take(blocksize).Concat(flipped).Concat(last_part).ToArray();

            return prepared_cipher;
        }

        public static byte[] CBCKeyAsIVAttack(BlackBoxPaddingEncryptor oracle)
        {
            /* Make high ascii payload to provoke error message */
            var blocksize = 16;
            var P1 = Enumerable.Repeat((byte)200, blocksize).ToArray();

            /* we dont want to do encoding prior to giving it, since if we ascii encode, we will remove the high ascii stuff i think */
            /* I.e. it will be made to ? / 63, and we cant to that prior to the url parsing and ascii checking, then we wont get the error */
            var payload = P1;
            var chars = new char[payload.Length / sizeof(char)];
            System.Buffer.BlockCopy(payload, 0, chars, 0, payload.Length);
            var payloadString = new string(chars);

            var response = oracle(payloadString);

            /* we dont need to target what we send in */
            /* Its only there to spark an error. and the three blocks is because each block affects the next, so we need the 
             * two adjacent ones to be sure we get high ascii. And we need the 0's in the middle for the trick to work */

            /* Construct C1..0..C1. the trick is that key and Iv were the same in the first block (c1). */
            var C1 = response.Take(blocksize).ToArray();
            var altered = C1.Concat(Enumerable.Repeat((byte)0, blocksize)).ToArray();
            altered = altered.Concat(C1).ToArray();

            var key = new byte[blocksize];

            try
            {
                var result = AESWrapper.CBC.LookForAdminCheckAscii(altered);
            }
            catch(LeakingException e) /* it should give an error due to high ascii */
            {
                /* P1 = Decr(C1) XOR KEY (IV). P3 = DECR(C1) XOR C2, where C2 = 0's. 
                 * Thus P1 XOR P3 = Decr(c1) XOR KEY XOR DECR(c1) XOR 0's = KEY */
                var bytes = e.NonAsciiPlaintext;
                var _P1 = bytes.Take(blocksize).ToArray();
                var _P3 = bytes.Skip(blocksize * 2).Take(blocksize).ToArray();

                key = Operations.RepeatingKeyXOR(_P1, _P3);
            }

            return key;
        }

        private static string ParseAsciiError(string errorMessage)
        {
            var split = errorMessage.Split(':');
            var skipFirst = split.Skip(1).ToArray();
            var res = string.Empty;

            foreach(var s in skipFirst)
            {
                res = res + s;
            }

            return res;
        }

        private static byte ECBSingleByteExtract(BlackBoxEncryptor oracle, byte[] a_prefix, byte[] dict_prefix, int blocksize, int bytes_to_skip)
        {
            /* cipher should be bytes_to_skip bytes too short (and only 'A's), then afterwards we check it against data */
            byte[] cipher = oracle(a_prefix);
            byte[] current_block = cipher.Skip(bytes_to_skip).Take(blocksize).ToArray();
            List<byte[]> dictionary = CreateSinglyPaddedDictionary(oracle, bytes_to_skip, blocksize, dict_prefix);

            int matching_char = -1;
            for (int i = 0; i < dictionary.Count; i++)
            {
                byte[] temp = dictionary[i];
                int distance = Operations.EditDistance(current_block, temp);
                if (distance == 0) { matching_char = i;}
            }

            char result = (char)matching_char;
            return (byte)result;
        }

        /* prefix is as is, and as long as it needs to be. Give it to oracle with || {0,..,255}. Then take the block we need and input to dict */
        /* prefix is between 0 and blocksize-1 'A's, and then concatted with the recovered padding so far */
        private static List<byte[]> CreateSinglyPaddedDictionary(BlackBoxEncryptor oracle, int bytes_to_skip, int blocksize, byte[] prefix)
        {
            List<byte[]> dictionary = new List<byte[]>();
            int ascii_low = 0;
            int ascii_high = 255;

            //only need to save the block of interest, but we need to send in the entire prefix, not just the corresponding block! Else we just get 'R' again 
            for (int i = ascii_low; i <= ascii_high; i++ )
            {
                byte[] appendex = prefix.Concat(new byte[1]).ToArray();
                appendex[appendex.Length-1] = (byte)i;
                byte[] result = oracle(appendex).Skip(bytes_to_skip).Take(blocksize).ToArray();
                dictionary.Add(result);
            }

            return dictionary;
        }


        private static int GuessECBPrefixPaddingBlocksize(BlackBoxEncryptor ecb_oracle)
        {
            int blocksize = 0;

            /* we assume it's some block cipher in this range (even though AES has max 24 byte block size) */
            /* if we can find two blocks in a row that are the same, we know the blocksize. Put we need to try for each blocksize */
            /* a prefix of size 0... blocksize-1 since we dont know size o the padding. If the distance of any of the blocks are then zero, we are in bussiness */
            int[] blocksizes = new int[] { 16, 24, 32 };

            for (int i = 0; i < blocksizes.Length; i++ )
            {

                for(int b = 0; b < blocksizes[i]-1; b++)
                {
                    byte[] data = Enumerable.Repeat((byte)'A', b + (2 * blocksizes[i])).ToArray();
                    byte[] cipher = ecb_oracle(data);

                    int blocks = cipher.Length / blocksizes[i];

                    for(int block = 0; block < blocks - 1; block++)
                    {
                        byte[] first_block = cipher.Skip(block * blocksizes[i]).Take(blocksizes[i]).ToArray();
                        byte[] second_block = cipher.Skip((block + 1) * blocksizes[i]).Take(blocksizes[i]).ToArray();
                        int distance = Operations.EditDistance(first_block, second_block);
                        if (distance == 0) { blocksize = blocksizes[i]; break; }
                    }

                    if (blocksize != 0) break;
                }

                if (blocksize != 0) break;

            }
                return blocksize;
        }

        /* we give the oracle between two blocks and almost three blocks of A's. We look at the cipher for the first index of duplicating blocks. When we find it we brealk */
        /* we know how many A's we gave it. We know the position of the first block of the two blocks that are the same due to determinism of ECB */
        /* then the rest is simply math, since we know we used 32 bytes for the two blocks, we know how many A's in total, and we know the pos. of the first equal block of A's */
        private static int GuessECBPrefixPaddedSize(BlackBoxEncryptor ecb_oracle, int blocksize)
        {
            int low = 32;
            int max = 32 + 15;

            int index_of_first_block_equal = -1;
            int amount_of_a = -1;

            for (int i = low; i <= max; i++)
            {
                byte[] data = Enumerable.Repeat((byte)'A', i).ToArray();
                byte[] cipher = ecb_oracle(data);

                int blocks = cipher.Length / blocksize;
                
                for(int b = 0; b < blocks-1; b++)
                {
                    byte[] first_block = cipher.Skip(b * blocksize).Take(blocksize).ToArray();
                    byte[] second_block = cipher.Skip((b + 1) * blocksize).Take(blocksize).ToArray();
                    int distance = Operations.EditDistance(first_block, second_block);
                    if (distance == 0)
                    {
                        index_of_first_block_equal = b;
                        amount_of_a = i;
                        break;
                    }
                }

                if (index_of_first_block_equal != -1) break;
            }

            int A_before_us = amount_of_a - 2 * blocksize;
            int guessed_prefix_padding_length = index_of_first_block_equal * blocksize - A_before_us;

            return guessed_prefix_padding_length;
        }

        private static int GuessECBBlocksize(BlackBoxEncryptor ecb_oracle, int max_blocksize)
        {
            int guessed_blocksize = 0;

            byte[] first_data = Encoding.ASCII.GetBytes(Enumerable.Repeat("A", 1).ToString());
            byte[] last_cipher =ecb_oracle(first_data);

            /* compare to previous guessed blocksize bits of previous resulting cipher. If it matches the current ciphers first "previous guessed blocksize" bits, */
            /* then we hit the tipping point of the block being deterministic (i.e always the same, we know the key used is fixed and ECB!) */
            for (int i = 2; i <= max_blocksize; i++)
            {
                int old_blocksize_guess = i - 1;
                byte[] data = Enumerable.Repeat((byte)'A', i).ToArray();
                byte[] cipher = ecb_oracle(data);
                byte[] temp = new byte[old_blocksize_guess];
                byte[] old_cipher_temp = new byte[old_blocksize_guess];

                for (int j = 0; j < old_blocksize_guess; j++)
                {
                    old_cipher_temp[j] = last_cipher[j];
                    temp[j] = cipher[j];
                }
                /* doesnt need to be float actually.. */
                float distance = ((float)Operations.EditDistance(temp, old_cipher_temp) / (float)i);
                if (distance == 0.0f) guessed_blocksize = old_blocksize_guess;
                last_cipher = cipher;
            }

            return guessed_blocksize;
        }


        /*
         *  We try for BLOCKS instead of just two - it seems the higher the blocks, the better the result (i.e for 20 we get 29 (correct) for just two 29 is #16 :/
         *  Or, we could do as in stinson p 35, i.e "Kasiski Testing". 
         */
        private static EditResult[] GuessKeylength(byte[] inputRaw, int keysize_low, int keysize_high, int blocks)
        {
            EditResult[] results = new EditResult[keysize_high - keysize_low + 1];
            float best_size = float.MaxValue;
            int best_key = 0;            

            if (inputRaw.Length < blocks * keysize_high * 2) throw new ArgumentOutOfRangeException("The highest keysize * blocks is larger than the file");

            for (int i = keysize_low; i <= keysize_high; i++)
            {
                byte[] first = new byte[i * blocks];
                byte[] second = new byte[i * blocks];
                for (int j = 0; j < i * blocks; j++)
                {
                    first[j] = inputRaw[j];
                    second[j] = inputRaw[(i*blocks) + j];
                }

                int edit_distance = Operations.EditDistance(first, second);
                float edit_distance_normalized = (float)edit_distance / ((float)(i*blocks));
                results[i - keysize_low] = new EditResult(edit_distance_normalized, edit_distance, i);

                if (edit_distance_normalized < best_size)
                {
                    best_size = edit_distance_normalized;
                    best_key = i;
                }
            }

            return results;
        }

        private static byte[] TryKeyLength(byte[] inputRaw, int keylength)
        {
            byte[] inputBytes = inputRaw;
            int buffer_blocks = (int)Math.Ceiling((float)inputBytes.Length / (float)keylength);
            byte[,] buffer = Converter.From1Dto2DbyteArray(buffer_blocks, keylength, inputBytes);

            /*Transpose the blocks such that they allign according to having been xord with the same key block*/
            byte[,] transposed_buffer = Converter.Transpose2DByteArray(buffer_blocks, keylength, buffer);
            byte[] key = new byte[keylength];
            string alphabet = "abcdefghijklmnopqrstuvwxyz ETAOIN'";


            /* For each block, try all possible key values, xor it onto, and take the block that yields the most characters from
             'alphabet', because that is probably the right key block
             */
            byte[] temp = new byte[buffer_blocks];
            for (int trans_blocks = 0; trans_blocks < keylength; trans_blocks++)
            {
                for (int i = 0; i < buffer_blocks; i++)
                {
                    temp[i] = transposed_buffer[trans_blocks, i];
                }
                FreqResult fr = Analysis.SingleByteKeyAnalysis(temp, alphabet);
                key[trans_blocks] = (byte)fr.SingleKeyCharacter;
            }

            return key;
        }

        private static byte[] TryKeyLength(byte[] input, int keylength, string alphabet)
        {
            byte[] inputBytes = input;
            int buffer_blocks = (int)Math.Ceiling((float)inputBytes.Length / (float)keylength);
            byte[,] buffer = Converter.From1Dto2DbyteArray(buffer_blocks, keylength, inputBytes);

            /*Transpose the blocks such that they allign according to having been xord with the same key block*/
            byte[,] transposed_buffer = Converter.Transpose2DByteArray(buffer_blocks, keylength, buffer);
            byte[] key = new byte[keylength];


            /* For each block, try all possible key values, xor it onto, and take the block that yields the most characters from
             'alphabet', because that is probably the right key block
             */
            byte[] temp = new byte[buffer_blocks];
            for (int trans_blocks = 0; trans_blocks < keylength; trans_blocks++)
            {
                for (int i = 0; i < buffer_blocks; i++)
                {
                    temp[i] = transposed_buffer[trans_blocks, i];
                }
                FreqResult fr = Analysis.SingleByteKeyAnalysis(temp, alphabet);
                key[trans_blocks] = (byte)fr.SingleKeyCharacter;
            }

            return key;
        }
    }


}
