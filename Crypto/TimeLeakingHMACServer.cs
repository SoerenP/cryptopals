﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Crypto
{
    public delegate bool HMACValidator(string message, byte[] mac);

    public class TimeLeakPredator
    {
        private class Timer
        {
            long _start;
            long _stop;

            public void Start()
            {
                _start = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            }

            public void Stop()
            {
                _stop = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            }

            public long GetTimePassedInMiliseconds()
            {
                return _stop - _start;
            }
        }

        /// <summary>
        /// Works, but quite slow. Time spent is sum_i=1^20 { sum_j=1^256 {50 * i} } = 256000ms approx 5 min. Actually takes 30 min in total. 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="validator"></param>
        /// <param name="macSize"></param>
        /// <param name="milisecondThreshold"></param>
        /// <returns></returns>
        public static byte[] ForgeHMACForFilename(string message, HMACValidator validator, int macSize, int milisecondThreshold)
        {
            if (macSize < 1)
                throw new ArgumentException("Macsize must be positive");

            var forgedMac = new byte[macSize];

            /* Start somewhere. lets guess all zeroes */
            var currentMac = Enumerable.Repeat((byte)0x00, macSize).ToArray();

            var timer = new Timer();

            for(int b = 0; b < macSize; b++)
            {
                /* try all possible byte values (0 to 256) */
                for(short i = 0; i < 256; i++)
                {
                    var currentByte = (byte)i;

                    currentMac[b] = currentByte;

                    timer.Start();

                    var isValid = validator(message, currentMac);

                    timer.Stop();

                    var timePassed = timer.GetTimePassedInMiliseconds();

                    /* our current byte is probably correct */
                    if(timePassed >= milisecondThreshold * (b+1))
                    {
                        forgedMac[b] = currentByte;
                        break;
                    }
                    
                }
            }

            return forgedMac;
        }

        public static byte[] ForgeHMACForFilenameParallel(string message, HMACValidator validator, int macSize, int milisecondThreshold)
        {
            if (macSize < 1)
                throw new ArgumentException("Macsize must be positive");
            
            /* Start somewhere. lets guess all zeroes */
            var currentMac = Enumerable.Repeat((byte)0x00, macSize).ToArray();

            var timer = new Timer();

            ushort min = 0;
            ushort max = 256;

            for (int b = 0; b < macSize; b++)
            {
                var byteFound = false;

                Parallel.For(min, max, (i, state) =>
                {
                    var _mac = new byte[macSize];
                    Array.Copy(currentMac, _mac, macSize);

                    var _timer = new Timer();

                    var currentByte = (byte)i;
                    _mac[b] = currentByte;

                    _timer.Start();

                    var isValid = validator(message, _mac);

                    _timer.Stop();

                    var timePassed = _timer.GetTimePassedInMiliseconds();

                    if(timePassed >= milisecondThreshold * (b+1))
                    {
                        if(!byteFound)
                        {
                            currentMac[b] = currentByte;
                            byteFound = true;
                            i = max;
                            state.Stop();
                        }                                                
                    }
                });
            }

            return currentMac;
        }

        /// <summary>
        /// Finds the threshold dynamically
        /// </summary>
        /// <param name="message"></param>
        /// <param name="validator"></param>
        /// <param name="macSize"></param>
        /// <returns></returns>
        public static byte[] ForgeHMACForFilenameParallelBetter(string message, HMACValidator validator, int macSize)
        {
            if (macSize < 1)
                throw new ArgumentException("Macsize must be positive");

            /* Start somewhere. lets guess all zeroes */
            var currentMac = Enumerable.Repeat((byte)0x00, macSize).ToArray();

            var timer = new Timer();

            ushort min = 0;
            ushort max = 256;
            var averageResponseTime = GetAverageResponseTime(validator, macSize, 10);

            for (int b = 0; b < macSize; b++)
            {
                var byteFound = false;

                Parallel.For(min, max, (i, state) =>
                {
                    var _mac = new byte[macSize];
                    Array.Copy(currentMac, _mac, macSize);

                    var _timer = new Timer();

                    var currentByte = (byte)i;
                    _mac[b] = currentByte;

                    _timer.Start();

                    var isValid = validator(message, _mac);

                    _timer.Stop();

                    var timePassed = _timer.GetTimePassedInMiliseconds();

                    if (timePassed >= averageResponseTime * (b + 1))
                    {
                        if (!byteFound)
                        {
                            currentMac[b] = currentByte;
                            byteFound = true;
                            i = max;
                            state.Stop();
                        }
                    }
                });
            }

            return currentMac;
        }

        struct ResponseTimed
        {
            public Int64 responsetime;
            public int index;

            public ResponseTimed(Int64 time, int index)
            {
                responsetime = time;
                this.index = index;
            }
        }

        private static Int64 GetAverageResponseTime(HMACValidator validator, int macSize, int sampleSize)
        {
            var mac = Enumerable.Repeat((byte)0x00, macSize).ToArray();
            var message = "Doesnt matter";

            var responseTimes = new List<ResponseTimed>();
            var times = new List<Int64>();
            var timer = new Timer();

            /* find the correct first byte, i.e. the one that has the largest respons time */
            for(int i = 0; i < 256; i++)
            {
                mac[0] = (byte)i;
                timer.Start();

                var valid = validator(message, mac);

                timer.Stop();

                responseTimes.Add(new ResponseTimed(timer.GetTimePassedInMiliseconds(), i));
            }

            var correctIndex = responseTimes.OrderByDescending(t => t.responsetime).First().index;

            mac[0] = (byte)correctIndex;

            for (int i = 0; i <= sampleSize + 1; i++)
            {
                timer.Start();

                var valid = validator(message, mac);

                timer.Stop();

                times.Add(timer.GetTimePassedInMiliseconds());
            }

            //We might have had one were the first byte was equal, so remove the max.
            var max = times.Max();

            return (times.Sum() - max) / (times.Count - 1);
        }
    }



    public class TimeLeakingHMACServer
    {
        private readonly int _timeLeakMiliseconds;
        private readonly byte[] _key;
        private readonly int keysize = HMAC.BLOCKSIZE_BYTES;

        public TimeLeakingHMACServer(int timeLeakMiliseconds)
        {
            _timeLeakMiliseconds = timeLeakMiliseconds;

            _key = AESWrapper.RandomKey(keysize);
        }

        public TimeLeakingHMACServer(int timeLeakMiliseconds, byte[] key)
        {
            _timeLeakMiliseconds = timeLeakMiliseconds;

            _key = key;
        }

        public bool Validate(string file, byte[] signature)
        {
            var mac = HMAC.SHA1(_key, Encoding.ASCII.GetBytes(file));

            var isValid = InsecureCompare(mac, signature);

            return isValid;
        }

        /* only for debugging purposes of the attack */
        public byte[] GetKey()
        {
            return _key;
        }

        private bool InsecureCompare(byte[] left, byte[] right)
        {
            if (left == null || right == null)
                throw new ArgumentNullException();

            if (left.Length != right.Length)
                return false;

            for (int i = 0; i < left.Length; i++)
            {
                if (left[i] != right[i])
                {
                    return false;
                }

                Thread.Sleep(_timeLeakMiliseconds);
            }

            return true;
        }


    }
}
