﻿using Crypto.Numerical;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;

namespace Crypto.DigitalSignatureAlgorithm
{
    public class DSAParameters
    {
        public static string qDefaultHex = "f4f47f05794b256174bba6e9b396a7707e563c5b";

        public static string pDefaultHex = "800000000000000089e1855218a0e7dac38136ffafa72eda7" + 
                                            "859f2171e25e65eac698c1702578b07dc2a1076da241c76c6" +
                                            "2d374d8389ea5aeffd3226a0530cc565f3bf6b50929139ebe" +
                                            "ac04f48c3c84afb796d61e5a4f9a8fda812ab59494232c7d2" + 
                                            "b4deb50aa18ee9e132bfa85ac4374d7f9091abc3d015efc87" + 
                                            "1a584471bb1";

        public static string gDefaultHex = "5958c9d3898b224b12672c0b98e06c60df923cb8bc999d119" +
                                            "458fef538b8fa4046c8db53039db620c094c9fa077ef389b5" +
                                            "322a559946a71903f990f1f7e0e025e2d7f7cf494aff1a047" +
                                            "0f5b64c36b625a097f1651fe775323556fe00b3608c887892" +
                                            "878480e99041be601a62166ca6894bdd41a7054ec89f756ba" +
                                            "9fc95302291";

        public BigInteger q { get; set; }
        public BigInteger p { get; set; }
        public BigInteger g { get; set; }

        public BigInteger x { get; set; }
        public BigInteger y { get; set; }

        public DSAParameters()
        {
            q = BigIntegerUtility.FromHexStringToPositive(qDefaultHex);
            p = BigIntegerUtility.FromHexStringToPositive(pDefaultHex);
            g = BigIntegerUtility.FromHexStringToPositive(gDefaultHex);


            x = DSA.GenerateRandomBelow(q);
            y = BigInteger.ModPow(g, x, p);
        }

        public DSAParameters(BigInteger q, BigInteger p, BigInteger g, BigInteger x, BigInteger y)
        {
            this.q = q;
            this.p = p;
            this.g = g;
            this.x = x;
            this.y = y;
        }

        public DSAParameters(BigInteger q, BigInteger p, BigInteger g, BigInteger x)
        {

            this.q = q;
            this.p = p;
            this.g = g;
            this.x = x;
            this.y = BigInteger.ModPow(g, x, p);
        }       
    }


    public class DSA
    {
        // Ugly hack so we can test our function to calculate x from K
        public static BigInteger LastUsedK = BigInteger.One;

        public struct Signature
        {
            public BigInteger r { get; private set; }
            public BigInteger s { get; private set; }

            public Signature(BigInteger r, BigInteger s)
            {
                this.r = r;
                this.s = s;
            }
        }

        public static BigInteger GenerateRandomBelow(BigInteger ceiling)
        {
            var rng = new RNGCryptoServiceProvider();

            var bytes = new byte[ceiling.ToByteArray().Length];

            rng.GetBytes(bytes);
            bytes[bytes.Length - 1] = 0x00; // Force positive. System.Numerics.BigInteger is little endian!

            var integer = new BigInteger(bytes);

            return integer % ceiling;
        }


        public static Signature Sign(DSAParameters parameters, byte[] message, HashAlgorithm hashAlg, BigInteger k)
        {
            var r = BigInteger.ModPow(parameters.g, k, parameters.p) % parameters.q;

            /* Here we should check that r != 0, but we are a faulty implementation */
            /* if (r.Equals(BigInteger.Zero)) throw new System.Exception("Got r = 0"); */

            var hashedMessageAsInteger = HashMessageThenConvertToBigInteger(message, hashAlg);

            var intermediate = hashedMessageAsInteger + parameters.x * r % parameters.q;
            var s = BigIntegerUtility.ModInverse(k, parameters.q) * intermediate % parameters.q;

            /* here we should check that s != 0 but we are a faulty implementation */
            /* if (s == BigInteger.Zero) throw new System.Exception("Got s = 0"); */

            LastUsedK = k;

            return new Signature(r, s);
        }

        public static Signature Sign(DSAParameters parameters, byte[] message, HashAlgorithm hashAlg)
        {
            var k = GenerateRandomBelow(parameters.q);
            return Sign(parameters, message, hashAlg, k);
        }

        public static BigInteger GetPrivateKeyFromK(BigInteger q, BigInteger k, byte[] message, HashAlgorithm hashAlg, Signature signature)
        {

            /*
             *          (s * k) - H(msg)
                    x = ----------------  mod q
                                r 
             * 
             */
            var hashedInt = HashMessageThenConvertToBigInteger(message, hashAlg);
            var upper = BigIntegerUtility.mod(signature.s * k - hashedInt, q);
            var rInverse = BigIntegerUtility.ModInverse(signature.r, q);

            return upper * rInverse % q;
        }

        public static BigInteger GetKFromRepeatedUsage(DSASignatureDto left, DSASignatureDto right, HashAlgorithm hashAlg, BigInteger q)
        {

            /*
             * 
                        (m1 - m2)
                 k =    --------- mod q
                        (s1 - s2) 
              
             
             * 
             */

            var m1 = HashMessageThenConvertToBigInteger(Encoding.Default.GetBytes(left.Message), hashAlg);
            var m2 = HashMessageThenConvertToBigInteger(Encoding.Default.GetBytes(right.Message), hashAlg);
            var upper = (m1 - m2) % q;

            // C# has % behave as remainder when it comes to negative values. 
            var lower = BigIntegerUtility.mod(left.s - right.s,q);

            var lowerInverse = BigIntegerUtility.ModInverse(lower, q);
            var k = (upper * lowerInverse) % q;

            return k;
        }

        public static bool Verify(DSAParameters parameters, byte[] message, HashAlgorithm hashAlg, Signature signature)
        {
            /* You should check the below, but we do not, since we wanna showcase why in challenge 45 */
            //var rIsValid = 0 < signature.r && signature.r < parameters.q;
            //var sIsValid = 0 < signature.s && signature.s < parameters.q;

            //if (!rIsValid || !sIsValid) return false;
            
            var w = BigIntegerUtility.ModInverse(signature.s, parameters.q);
            var hashedMessageAsInteger = HashMessageThenConvertToBigInteger(message, hashAlg);
            var u1 = hashedMessageAsInteger * w % parameters.q;
            var u2 = signature.r * w % parameters.q;
            var v = (BigInteger.ModPow(parameters.g, u1, parameters.p) * BigInteger.ModPow(parameters.y, u2, parameters.p) % parameters.p) % parameters.q;

            return v == signature.r;
        }

        public static BigInteger HashMessageThenConvertToBigInteger(byte[] message, HashAlgorithm hashAlg)
        {
            var hashedMessage = hashAlg.ComputeHash(message);

            // TODO: do we need to reverse it? BigInteger expects little endian, so i think we should reverse the hashed above, before we concat the 0 to force positive. 
            // Otherwise we will not get same int as in the challenge. 
            hashedMessage = hashedMessage.Reverse().Concat(new byte[1] { 0x00 }).ToArray(); // Force positive. System.Numerics is little endian. 
            return new BigInteger(hashedMessage);
        }
    }

    public class DSASignatureDto
    {
        public static List<DSASignatureDto> Challenge44Signatures = new List<DSASignatureDto>()
        {
            new DSASignatureDto()
            {
                Message = "Listen for me, you better listen for me now. ",
                s = BigInteger.Parse("1267396447369736888040262262183731677867615804316"),
                r = BigInteger.Parse("1105520928110492191417703162650245113664610474875"),
                m = "a4db3de27e2db3e5ef085ced2bced91b82e0df19"
            },

            new DSASignatureDto()
            {
                Message = "Listen for me, you better listen for me now. ",
                s = BigInteger.Parse("29097472083055673620219739525237952924429516683"),
                r = BigInteger.Parse("51241962016175933742870323080382366896234169532"),
                m = "a4db3de27e2db3e5ef085ced2bced91b82e0df19"
            },

            new DSASignatureDto()
            {
                Message = "When me rockin' the microphone me rock on steady, ",
                s = BigInteger.Parse("277954141006005142760672187124679727147013405915"),
                r = BigInteger.Parse("228998983350752111397582948403934722619745721541"),
                m = "21194f72fe39a80c9c20689b8cf6ce9b0e7e52d4"
            },

            new DSASignatureDto()
            {
                Message = "Yes a Daddy me Snow me are de article dan. ",
                s = BigInteger.Parse("1013310051748123261520038320957902085950122277350"),
                r = BigInteger.Parse("1099349585689717635654222811555852075108857446485"),
                m = "1d7aaaa05d2dee2f7dabdc6fa70b6ddab9c051c5"
            },

            new DSASignatureDto()
            {
                Message = " But in a in an' a out de dance em ",
                s = BigInteger.Parse("203941148183364719753516612269608665183595279549"),
                r = BigInteger.Parse("425320991325990345751346113277224109611205133736"),
                m = "6bc188db6e9e6c7d796f7fdd7fa411776d7a9ff"
            },

            new DSASignatureDto()
            {
                Message = "Aye say where you come from a, ",
                s = BigInteger.Parse("502033987625712840101435170279955665681605114553"),
                r = BigInteger.Parse("486260321619055468276539425880393574698069264007"),
                m = "5ff4d4e8be2f8aae8a5bfaabf7408bd7628f43c9"
            },

            new DSASignatureDto()
            {
                Message = "People em say ya come from Jamaica, ",
                s = BigInteger.Parse("1133410958677785175751131958546453870649059955513"),
                r = BigInteger.Parse("537050122560927032962561247064393639163940220795"),
                m = "7d9abd18bbecdaa93650ecc4da1b9fcae911412"
            },

            new DSASignatureDto()
            {
                Message = "But me born an' raised in the ghetto that I want yas to know, ",
                s = BigInteger.Parse("559339368782867010304266546527989050544914568162"),
                r = BigInteger.Parse("826843595826780327326695197394862356805575316699"),
                m = "88b9e184393408b133efef59fcef85576d69e249"
            },

            new DSASignatureDto()
            {
                Message = "Pure black people mon is all I mon know. ",
                s = BigInteger.Parse("1021643638653719618255840562522049391608552714967"),
                r = BigInteger.Parse("1105520928110492191417703162650245113664610474875"),
                m = "d22804c4899b522b23eda34d2137cd8cc22b9ce8"
            },

            new DSASignatureDto()
            {
                Message = "Yeah me shoes a an tear up an' now me toes is a show a ",
                s = BigInteger.Parse("506591325247687166499867321330657300306462367256"),
                r = BigInteger.Parse("51241962016175933742870323080382366896234169532"),
                m = "bc7ec371d951977cba10381da08fe934dea80314"
            },

            new DSASignatureDto()
            {
                Message = "Where me a born in are de one Toronto, so ",
                s = BigInteger.Parse("458429062067186207052865988429747640462282138703"),
                r = BigInteger.Parse("228998983350752111397582948403934722619745721541"),
                m = "d6340bfcda59b6b75b59ca634813d572de800e8f"
            },


        };

        public BigInteger r { get; set; }
        public BigInteger s { get; set; }
        public string Message { get; set; }
        public string m { get; set; }
    }

    
}
