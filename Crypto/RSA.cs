﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Numerics;
using System.Text;

using Crypto.Numerical;

namespace Crypto
{
    public class UnpaddedMessageRecoveryOracle
    {
        public class Message
        {
            public uint UnixTimeStamp { get; }
            public byte[] Bytes { get; set; }

            public Message(byte[] bytes)
            {
                Bytes = bytes;
                UnixTimeStamp = Operations.ToUnixTimeStamp(DateTime.Now);
            }

            public byte[] GetHash()
            {
                var bytes = BitConverter.GetBytes(UnixTimeStamp).Concat(Bytes).ToArray();

                var sha256 = new SHA256CryptoServiceProvider();

                using (sha256)
                {
                    return sha256.TransformFinalBlock(bytes, 0, bytes.Length);
                }
            }

        }        
                
        public RSA.PublicKey PublicKey { get; }

        private RSA.PrivateKey _privateKey;
        private readonly List<byte[]> _hashes;


        public UnpaddedMessageRecoveryOracle()
        {
            _hashes = new List<byte[]>();

            var keypair = RSA.ProbabilisticKeyGen(new BigInteger(3), 256);

            this.PublicKey = keypair.PublicKey;
            this._privateKey = keypair.PrivateKey;

        }

        public byte[] Decrypt(Message message)
        {
            if(_hashes.Exists(x => x.SequenceEqual(message.GetHash())))
            {
                throw new CryptographicException("Message already received once");
            }

            _hashes.Add(message.GetHash());

            return RSA.Decrypt(this._privateKey, message.Bytes);
        }        
    }

    public class PKCS1Signature
    {
        static Dictionary<string, byte[]> HASH_ASN1 = new Dictionary<string, byte[]>()
        {
            { "MD5",  new byte[] { 0x30, 0x20, 0x30, 0x0c, 0x06, 0x08, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x02, 0x05, 0x05, 0x00, 0x04, 0x10} },
            { "SHA-1", new byte[] { 0x30, 0x21, 0x30, 0x09, 0x06, 0x05, 0x2b, 0x0e, 0x03, 0x02, 0x1a, 0x05, 0x00, 0x04, 0x14 } },
            { "SHA-256", new byte[] { 0x30, 0x31, 0x30, 0x0d, 0x06, 0x09, 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 0x01, 0x05, 0x00, 0x04, 0x20} },
            { "SHA-384", new byte[] { 0x30, 0x41, 0x30, 0x0d, 0x06, 0x09, 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 0x02, 0x05, 0x00, 0x04, 0x30 } },
            { "SHA-512", new byte[] { 0x30, 0x51, 0x30, 0x0d, 0x06, 0x09, 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 0x03, 0x05, 0x00, 0x04, 0x40 } }
        };

        private delegate HashAlgorithm HashAlgConstructor();

        static Dictionary<string, HashAlgConstructor> HashAlgFunctionTable = new Dictionary<string, HashAlgConstructor>()
        {
            { "MD5", () => new MD5CryptoServiceProvider() },
            { "SHA-1", () => new SHA1CryptoServiceProvider() },
            { "SHA-256", () => new SHA256CryptoServiceProvider() },
            { "SHA-384", () => new SHA384CryptoServiceProvider() },
            { "SHA-512", () => new SHA512CryptoServiceProvider() }
        };

        static Dictionary<string, int> HashSizeTable = new Dictionary<string, int>()
        {
            { "MD5",  128 / 8 },
            { "SHA-1",  160 / 8 },
            { "SHA-256",  256 / 8},
            { "SHA-384", 384 / 8 },
            { "SHA-512", 512 / 8 }
        };

        private static byte[] Hash(string hashAlg, byte[] message)
        {
            var hash = HashAlgFunctionTable[hashAlg]();

            hash.TransformFinalBlock(message, 0, message.Length);

            return hash.Hash;            
        }

        /* we just assume key is large enough to fit hash */
        public static byte[] Sign(byte[] message, RSA.PrivateKey key)
        {
            var blocksize = key.N.ToByteArray().Length - 1; /* TODO: fix hack. the -1 is a hack to try and get below N in value */

            var encoding = new byte[blocksize];

            var hash = Hash("SHA-1", message);
            var ans1 = HASH_ASN1["SHA-1"];

            /* this should be emLen (intended message length? so.. less than public key size to be sure we dont go beyound modulus?) */
            /* emLen - tlength (ans 1 length) - 3 octets */
            var paddingLength = blocksize - hash.Length - 2 - 1 - ans1.Length; 
            var padding = Enumerable.Repeat((byte)0xff, paddingLength).ToArray();

            encoding[0] = 0x00;
            encoding[1] = 0x01;

            Array.Copy(padding, 0, encoding, 2, paddingLength);

            encoding[paddingLength + 2] = 0x00; // Seperator

            Array.Copy(ans1, 0, encoding, paddingLength + 3, ans1.Length);
            Array.Copy(hash, 0, encoding, paddingLength + 3 + ans1.Length, hash.Length);

            /* TODO: this becomes negative (not really a problem but weird) and LARGER than N in terms of bits. So when we encrypt it, we cant go back with decrypt */
            /* since it is bigger than N - so the deterministic aspect is lost. We need a way to turn it into a big int that is posivite and less than N. */
            /* Maybe we pad too much? or maybe we just make the encoding way too large */
            var encodingInt = new BigInteger(encoding); 

            var signature = RSA.Decrypt(key, encodingInt);


            return signature.ToByteArray();
        }

        /*
         * The propper way to implement this kind of verification is to NOT parse the input. Parsing is error prone.
         * Instead, build the encoded data yourself as you expect it to be, then compare it to what you received. 
         * E.g. hash the message, prepare it up until the step where the signer would apply the RSA operation with private key.
         * Then, do the RSA operation with public key on the signature you received, and compare it to what you constructed yourself.  
         */
        public static bool Verify(byte[] message, byte[] signature, RSA.PublicKey key, bool toBigEndiand)
        {
            var blocksize = key.N.ToByteArray().Length - 1; /* TODO: FIX HACK? the -1 is a hack to try and get below N in value */
            var encrypted = new BigInteger(signature);
            var decrypted = RSA.Decrypt(key.e, key.N, encrypted);
            var rawSignature = decrypted.ToByteArray();

            if (toBigEndiand)
            {
                /* TODO: This is dirty. we are missing the first 0, since it gets lost when we convert in the forging, since the most significant will then be some zero bytes */
                rawSignature = rawSignature.Concat(new byte[1] { 0x00 }).Reverse().ToArray();
            }

            /* if we cant find the signature marker verification fails */
            if (rawSignature[0] != 0x00 || rawSignature[1] != 0x01) return false;

            /* find the 00 seperator between the padding and the payload */
            /* this is the mistake, we do not count the amount of padding and check if it is appropriate with regards to keysize and hash output size */
            var indexOfSeperator = rawSignature.Skip(2).ToList().IndexOf(0x00) + 2;
            if (indexOfSeperator <= 0) return false;

            /* when we know the hash alg, hash the message, compare it to the last bytes after ans1 identifier, if they are equal signature was valid */
            /* but dont take from the right, just take the expected amout of bytes after ans1, that is the vulnerability / what allows the attack */
            /* e.g. we dont look behind hash to discover if there is garbage */
            var payload = new byte[rawSignature.Length - indexOfSeperator - 1];
            Array.Copy(rawSignature, indexOfSeperator + 1, payload, 0, payload.Length);
            var findHashAlgResult = FindHashAlgorithm(payload);

            /* If we just assumed below that the hash was the remainder (e.g. dont to the last Take part of the hash length, just taking everything) */
            /* then this code would not be vulnerable. Since then we would get the wrong length */
            var payloadHash = payload.Skip(findHashAlgResult.Ans1Sequence.Length).Take(findHashAlgResult.HashLength).ToArray();

            var calculatedHash = Hash(findHashAlgResult.HashAlg, message);

            if (payloadHash.SequenceEqual(calculatedHash)) return true;

            return false;
        }

        private class FindHashAlgResult
        {
            public string HashAlg { get; private set; }
            public int HashLength { get; private set; }
            public byte[] Ans1Sequence { get; private set; }

            public FindHashAlgResult(string hashAlg, int hashLength, byte[] ans1Sequence)
            {
                HashAlg = hashAlg;
                HashLength = hashLength;
                Ans1Sequence = ans1Sequence;
            }
        }

        private static FindHashAlgResult FindHashAlgorithm(byte[] sequence)
        {
            foreach(var ans1hash in HASH_ASN1)
            {
                var bytes = ans1hash.Value;

                var candiate = sequence.ToList().GetRange(0, bytes.Count()).ToArray();

                if (bytes.SequenceEqual(candiate))
                {
                    return new FindHashAlgResult(ans1hash.Key, HashSizeTable[ans1hash.Key], candiate);
                }
            }

            throw new ArgumentException("Sequence not a valid Ans1Hash");
        }

        public static byte[] ForgeSignatureEQuals3(byte[] message, string hashAlg, RSA.PublicKey key)
        {
            var blocksize = key.N.ToByteArray().Length - 1;
            var hash = Hash(hashAlg, message);

            var forged = new byte[blocksize];
            forged[0] = 0x00;
            forged[1] = 0x01;
            forged[2] = 0xFF;
            forged[3] = 0x00;

            var ans1 = HASH_ASN1[hashAlg];
            Array.Copy(ans1, 0, forged, 4, ans1.Length);
            Array.Copy(hash, 0, forged, 4 + ans1.Length, hash.Length);

            /* experimenting */
            for (int i = 4 + ans1.Length + hash.Length; i < blocksize; i++)
            {
                forged[i] = 0x02; /* something non zero. TODO: do we need this if we do big endian? */
            }

            /* System.Numerics.Math.BigInteger is little endian.. So we have to reverse it like so. And the other end has to also know this */
            /* the real way to fix this would be to use bouncy castle big integer, it is big endian. */
            /* TODO: this almost works, but we loose the leading 0x00!! since it will now be most significant, and then we dont need most significant all zero */
            /* so the validation fails due to missing 0x00 */
            var bigEndian = forged.Reverse().ToArray();

            var forgedInt = new BigInteger(bigEndian);
            /* make sure we do not wrap? E.g. take modulus and compare it to itself again, should not change. */

            var didNotWrap = (forgedInt == forgedInt % key.N);
            if (!didNotWrap) throw new Exception("Forged signature wrapped modulus, abort");

            /* we must make it a perfect cube? So we do not loose information when we do the 3rd root below.  */
            /* We cant really. But as long as it is close enough, it will be fine. The stuff that is off and will be destroyed / misssing should be the trailing garbage / zeroes */
            /* UPDATE: it would appear that the biginteger stuff is little endian (https://docs.microsoft.com/en-us/dotnet/api/system.numerics.biginteger.-ctor?view=netframework-4.7.2) */
            /* . This means the forged stuff above is the least significant bits, and are thus */
            /* destroyed in the below approximate root stuff. The garbage at the end is what survives the imperfect cubing. The exact opposite of what we wanted. */
            /* How the hell do we fix that? Endian-ness only works at the byte level. */


            /* take qube root */
            var quberoot = BigIntegerUtility.FloorRoot(forgedInt, 3); /* TODO: we somehow destroy too much info here. Maybe we must forge the above to be a perfect cube, so cube root does not loose information? How do we do that.. */
            quberoot += 1;

            var back = quberoot * quberoot * quberoot;
            var backModN = back % key.N;
            /* This is only 40 bytes.. should be 125ish? where does it all go? is it little endian so our trailing 0's are not needed? */
            var backModNBytes = backModN.ToByteArray();

            return quberoot.ToByteArray();
        }
    }


    public class RSA
    {
        public class PublicKey
        {
            public BigInteger e { get; set; }
            public BigInteger N { get; set; }

            public PublicKey()
            {

            }
        }

        public class PrivateKey
        {
            public BigInteger d { get; set; }
            public BigInteger N { get; set; }

            public PrivateKey()
            {

            }
        }

        public class KeyGenResult
        {
            public PublicKey PublicKey { get; set; }
            public PrivateKey PrivateKey { get; set; }

            public KeyGenResult()
            {

            }
        }


        public static KeyGenResult ProbabilisticKeyGen(BigInteger e, int bits)
        {
            BigInteger p = GeneratePrime(bits);
            BigInteger q = GeneratePrime(bits);

            BigInteger N = p * q;
            BigInteger totient = (p - 1) * (q - 1);

            BigInteger u;
            BigInteger v;
            BigInteger gcd = BigIntegerUtility.ExtendedEuclidGcd(e, totient, out u, out v);

            /* loop until gcd = 1 */
            while (!gcd.Equals(BigInteger.One))
            {
                p = GeneratePrime(bits);
                q = GeneratePrime(bits);

                N = p * q;
                totient = (p - 1) * (q - 1);

                gcd = BigIntegerUtility.ExtendedEuclidGcd(e, totient, out u, out v);
            }

            BigInteger d = BigIntegerUtility.ModInverse(e, totient);

            return new KeyGenResult()
            {
                PrivateKey = new PrivateKey()
                {
                    d = d,
                    N = N
                },
                PublicKey = new PublicKey()
                {
                    e = e,
                    N = N
                }
            };
        }

        public static KeyGenResult KeyGen(BigInteger p, BigInteger q, BigInteger e)
        {
            BigInteger N = p * q;
            BigInteger totient = (p - 1) * (q - 1); /* this should be lcm of the two values*/
            BigInteger d = BigIntegerUtility.ModInverse(e, totient);

            return new KeyGenResult()
            {
                PrivateKey = new PrivateKey()
                {
                    d = d,
                    N = N
                },
                PublicKey = new PublicKey()
                {
                    e = e,
                    N = N
                }
            };
        }

        /// <summary>
        /// BE WARY: we do not account for if the message is larger in bits than the public key - if so, then the result is garbage and you cannot decrypt!! 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static BigInteger CheesyStringToBigInt(string message)
        {
            var hex = Converter.FromStringToHex(message);
            return CheesyHexStringToBigInt(hex);
        }

        public static BigInteger CheesyByteArrayToBigInt(byte[] bytes)
        {
            var hex = Converter.FromBytesToHex(bytes);
            return CheesyHexStringToBigInt(hex);
        }

        private static BigInteger CheesyHexStringToBigInt(string hex)
        {
            var bytes = Encoding.Default.GetBytes(hex);
            var prefix = new byte[] { 0x00 };
            bytes = prefix.Concat(bytes).ToArray();
            return new BigInteger(bytes);
        }

        /// <summary>
        /// BE WARY: we do not account for if the message is larger in bits than the public key - if so, then the result is garbage and you cannot decrypt!! 
        /// </summary>
        /// <param name="integer"></param>
        /// <returns></returns>
        public static string CheesyBigIntToString(BigInteger integer)
        {
            var bytes = integer.ToByteArray().Skip(1).ToArray();

            /* this is the actual hex string of the original message, so not quite the final result */
            var hex = Encoding.Default.GetString(bytes);

            /* do it again */
            return Encoding.Default.GetString(Converter.FromHexToBytes(hex));
        }

        public static BigInteger Encrypt(PublicKey pk, BigInteger message)
        {
            return BigInteger.ModPow(message, pk.e, pk.N);
        }

        public static BigInteger Decrypt(PrivateKey pk, BigInteger ciphertext)
        {
            return BigInteger.ModPow(ciphertext, pk.d, pk.N);
        }

        public static BigInteger Decrypt(BigInteger d, BigInteger n, BigInteger ciphertext)
        {
            return BigInteger.ModPow(ciphertext, d, n);
        }

        public static BigInteger Encrypt(BigInteger e, BigInteger n, BigInteger message)
        {
            return BigInteger.ModPow(message, e, n);
        }

        public static byte[] Encrypt(PublicKey pk, byte[] message)
        {
            return Encrypt(pk, new BigInteger(message)).ToByteArray();
        }

        public static byte[] Encrypt(BigInteger e, BigInteger n, byte[] message)
        {
            return Encrypt(e, n, new BigInteger(message).ToByteArray());
        }

        public static byte[] Decrypt(PrivateKey pk, byte[] ciphertext)
        {
            return Decrypt(pk, new BigInteger(ciphertext)).ToByteArray();
        }

        public static byte[] Decrypt(BigInteger d, BigInteger n, byte[] ciphertext)
        {
            return Decrypt(d, n, new BigInteger(ciphertext).ToByteArray());
        }

        /// <summary>
        /// Only works if M is prime.
        /// </summary>
        /// <param name="b"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static BigInteger ModInverse2(BigInteger b, BigInteger m)
        {
            return BigInteger.ModPow(b, m - 2, m);
        }

        public static BigInteger ModInverseSimple(BigInteger b, BigInteger m)
        {
            BigInteger i = m, v = 0, d = 1;
            while (b > 0)
            {
                BigInteger t = i / b, x = b;
                b = i % x;
                i = x;
                x = d;
                d = v - t * x;
                v = x;
            }
            v %= m;
            if (v < 0) v = (v + m) % m;
            return v;
        }

        static public BigInteger GeneratePrime(int bits)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

            var bytes = new byte[bits / 8];

            rng.GetBytes(bytes);

            // Make sign bit positive? gives bias... we should roll the dice again realistically.
            bytes[0] = 0x00;

            Org.BouncyCastle.Math.BigInteger c = new Org.BouncyCastle.Math.BigInteger(bytes);

            for (;;)
            {
                /* we want this neat function here, not part of .NET sadly */
                if (c.IsProbablePrime(1) == true) break;

                c = c.Subtract(Org.BouncyCastle.Math.BigInteger.One);
            }

            // dirty hack, the two implementations do not have same toByteArray conversion.
            var stringRepresentation = c.ToString();

            BigInteger result;

            BigInteger.TryParse(stringRepresentation, out result);

            return result;
        }

        public static bool IsPlaintextEven(PrivateKey priv, BigInteger ciphertext)
        {
            var message = Decrypt(priv, ciphertext);
            return message.IsEven;
        }

        public static BigInteger ParityDecryption(PrivateKey priv, BigInteger ciphertext, PublicKey pub)
        {
            bool done = false;
            
            /* [0, n]. We work with the interval as inclusive below. */
            var interval = new Interval(BigInteger.Zero, priv.N);
            var factor = new BigInteger(2);

            var iteration = 1;

            /* Handle first round explicitly? We need to handle explicitly until we dont have zero as lower though */

            while(!done)
            {
                /* you need to multiply the plaintext with two, which is done by multiplying the ciphertext with encr(2), e.g. 2^e ! */
                var multiplyingFactor = BigInteger.Pow(2, iteration);
                var currentCiphertext = Encrypt(pub, multiplyingFactor) * ciphertext;
                var isEven = IsPlaintextEven(priv, currentCiphertext);

                if (isEven)
                {
                    /* doubling did not wrap the modulus. plaintext <= half the modulus */
                    interval.Upper = BigInteger.Divide(interval.Lower + interval.Upper, 2);
                }
                else
                {
                    /* doubling wrapped the modulus, since the mod operation of the uneven modulus number leaves an uneven number behind */
                    /* plaintext is > half the modulus */
                    interval.Lower = BigInteger.Divide(interval.Lower + interval.Upper, 2) + 1; /* We add one because this interval is <, e.g. strictly larger. and we want interval to be inclusive */
                }

                iteration++;

                done = IsParityDecryptionDone(interval);
            }

            return interval.Lower;
        }

        private static bool IsParityDecryptionDone(Interval interval)
        {
            return interval.Lower == interval.Upper;
        }

        /* These are ment to be inclusive */
        public struct Interval
        {
            public BigInteger Upper { get; set; }
            public BigInteger Lower { get; set; }

            public Interval(BigInteger lower, BigInteger upper)
            {
                Upper = upper;
                Lower = lower;
            }
        }


    }
}
