﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto
{
    public static class Extensions
    {
        public static bool IsEven(this byte b)
        {
            /* if last bit is set, then its odd (1) so negate it */
            return !IsBitSet(b, 0);
        }

        public static bool IsBitSet(byte b, int pos)
        {
            return (b & (1 << pos)) != 0;
        }

    }
}
