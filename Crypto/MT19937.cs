﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Crypto
{
    /* Naive reset tokens that are "Random" but not unpredictable! */
    public static class PasswordResetToken
    {
        public static uint GetNext()
        {
            var seed = Operations.ToUnixTimeStamp(DateTime.Now);
            var rng = new MT19937(seed);

            return rng.ExtractNumber();
        } 
    }

    public struct PastSeedingResult
    {
        public readonly bool IsSeededFromPast;
        public readonly uint Seed;

        public PastSeedingResult(bool isSeededFromPast, uint seed)
        {
            this.IsSeededFromPast = isSeededFromPast;
            this.Seed = seed;
        }
    }

    public class MT19937TokenDetector
    {
        /* Try the current time, or subtract some seconds to see if you hit the right seed */
        public PastSeedingResult IsTokenSeededFromPast(uint token, int secondsBackInTime)
        {
            var starttime = DateTime.Now;

            for(var seconds = 0; seconds < secondsBackInTime; seconds++)
            {
                var time = Operations.ToUnixTimeStamp(starttime.AddSeconds(-seconds));
                var rng = new MT19937(time);
                var ownToken = rng.ExtractNumber();

                if (ownToken == token) return new PastSeedingResult(true, time);
            }

            return new PastSeedingResult(false, 0);
        }

        public bool IsTokenSeededFromNow(uint token)
        {
            /* Seed with current time, see if we get the same result */
            var seed = Operations.ToUnixTimeStamp(DateTime.Now);
            var rng = new MT19937(seed);
            var ownToken = rng.ExtractNumber();

            if (ownToken == token) return true;
            return false;
        }
    }


    public class RNGBreaker
    {
        private int lowWait = 40;
        private int highWait = 1000;

        public uint WaitAndBleed()
        {
            Random _random = new Random();
            
            /* Wait between 40 and 1000 seconds */
            int milisecondsToWait = _random.Next(lowWait, highWait);
            Thread.Sleep(milisecondsToWait * 1000);

            /* seed the rng */
            var seed = Operations.ToUnixTimeStamp(DateTime.Now);
            var rng = new MT19937(seed);

            /* wait again */
            milisecondsToWait = _random.Next(lowWait, highWait);
            Thread.Sleep(milisecondsToWait * 1000);

            return rng.ExtractNumber();            
        }

    }

    public class MT19937StreamCipher
    {
        public struct BruteForceResult
        {
            public UInt16 Key;
            public double Score;
            public string Plaintext;

            public BruteForceResult(UInt16 key, double score, string plaintext)
            {
                Key = key;
                Score = score;
                Plaintext = plaintext;
            }
        }


        /* The challenge says its a known plaintext, so we can simply try until we find it*/
        /* No reason to do "analysis" on the plaintext, after all, no human readable plaintext */
        /* has a lot of A's in a row, so stat. analysis will do us no good */
        public static ushort BruteForceKey(byte[] ciphertext, string knownPlaintext)
        {
            /* The keyspace is only 2^16, so simply bruteforce it. */
            ushort key = 0;

            ushort min = 0;
            ushort max = 65535;

            bool keyfound = false;


            /* Lets speed things up, each try is independant after all */
            Parallel.For(min, max + 1, i =>
            {
                  /* Create new streamcipher */
                  var streamCipher = new MT19937StreamCipher((ushort)i);
                  var cipher = streamCipher.Decrypt(ciphertext);
                  var plaintext = Encoding.ASCII.GetString(cipher);
                  if (plaintext.Contains(knownPlaintext))
                  {
                      key = streamCipher._seed;
                      keyfound = true;
                  }

                  if (keyfound)
                  {
                      i = max;
                  }
             });

            if (!keyfound)
            {
                throw new Exception("Known plaintext was never found for all possible keys");
            }

            return key;
        }

        public UInt16 _seed;

        public MT19937StreamCipher(UInt16 seed)
        {
            _seed = seed;
        }

        public byte[] Encrypt(byte[] data)
        {
            var length = data.Length;
            var keystreamlength = length / 4;

            /* Generate keystream */
            var rng = new MT19937(_seed);
            byte[] keystream = new byte[length];

            for(int i = 0; i < keystreamlength; i++)
            {
                var temp = BitConverter.GetBytes(rng.ExtractNumber());
                keystream[i * 4] = temp[0];
                keystream[(i * 4) + 1] = temp[1];
                keystream[(i * 4) + 2] = temp[2];
                keystream[(i *4) + 3] = temp[3];
            }

            /* Handle last part explicitly */
            var remainder = length % 4;
            var bytes = BitConverter.GetBytes(rng.ExtractNumber());
            for(int i = 0; i < remainder; i++)
            {
                keystream[(keystreamlength * 4) + i] = bytes[i];
            }
            
            /* Do the encryption */
            byte[] cipher = Operations.RepeatingKeyXOR(data, keystream);

            return cipher;
        }

        public byte[] Decrypt(byte[] data)
        {
            return Encrypt(data);
        }
    }

    public class MT19937
    {
        public const int w = 32;
        public const uint n = 624;
        public const uint m = 397;
        public const uint r = 31;
        public const uint a = 0x9908B0DF;
        public const int u = 11;
        public const uint d = 0xFFFFFFFF;
        public const int s = 7;
        public const uint b = 0x9D2C5680;
        public const int t = 15;
        public const uint c = 0xEFC60000;
        public const int l = 18;
        public const uint f = 1812433253;

        public const uint lower_mask = 0x7FFFFFFF;
        public const uint upper_mask = ~lower_mask;

        private uint[] MT = new uint[n];
        private uint index = n + 1;


        public MT19937(uint seed)
        {
            MT[0] = seed;

            for(uint i = 1; i < n; i++)
            {
                var temp = MT[i - 1] ^ (MT[i - 1] >> (w - 2));
                MT[i] = f * temp + i;
            }

            index = n;
        }

        public MT19937(uint[] state)
        {
            MT = state.ToArray();

            //We are splicing, so we dont want to twist it - it was done as soon as we called the first rng.
            //So we should not twist it here. Since what we untemper is after the call to twist since it normally starts
            //with index = 0; So we skip twist by setting it to zero below.
            index = 0;
        }

        public uint[] GetState()
        {
            return MT;
        }

        public uint ExtractNumber()
        {
            if(index >= n)
            {
                if(index > n)
                {
                    throw new Exception("Not seeded");
                }

                Twist();
            }

            uint y = MT[index];
            y = y ^ ((y >> u) & d); //d is 0xFFFFFFFF so it does nothing.
            y = y ^ ((y << s) & b);
            y = y ^ ((y << t) & c);
            y = y ^ (y >> l);

            ++index;

            return y;
        }

        private void Twist()
        {
            for (uint i = 0; i < n; ++i)
            {
                uint x = (MT[i] & upper_mask) + (MT[(i + 1) % n] & lower_mask);
                uint xA = x >> 1;

                if (x % 2 != 0)
                {
                    xA = xA ^ a;
                }

                MT[i] = MT[(i + m) % n] ^ xA;
            }

            index = 0;
        }

        public static uint Untemper(uint random)
        {
            var y = random;
            y = UndoTemperShiftL(y);
            y = UndoTemperShiftLeft(y, 15, c);
            y = UndoTemperShiftLeft(y, 7, b);
            // The and with 0xFFFFFFFF does nothing. Its all ones. So we can reuse the NoMask Function
            y = UndoTemperShiftRightNoMask(y, 11);

            return y;
        }

        private static uint UndoTemperShiftLeft(uint value, int shift, uint mask)
        {
            int chunk = 0;
            uint result = 0;
            while(chunk * shift < 32)
            {
                // create mask or this part
                uint partMask = (0xFFFFFFFF >> (32 - shift)) << (shift * chunk);

                // Obtain the part
                uint part = value & partMask;

                // Unapply XOR, remember the mask
                value ^= (part << shift) & mask;

                // add the part to the result
                result |= part;
                chunk++;
            }

            return result;
        }

        private static uint UndoTemperShiftRightNoMask(uint value, int shift)
        {
            int chunk = 0;
            uint result = 0;
            while(chunk * shift < 32)
            {
                // create mask. Allign all 1's to the right, ontop of the spot we are currently at (chunk * 11).
                uint partMask = (0xFFFFFFFF << (32 - shift)) >> (shift * chunk);

                // obtain that part by and'ing, since the rest of the mask is zero.
                uint part = value & partMask;

                // Unapply the xor from the next part of the integer using our newly obtained part.
                // This means that we discover more bits of the original, so we can do the next iteration.
                value ^= part >> shift;

                //Add the part to the result. The part is the original of the untempered value to the right, so we can simply add it.
                // The unapply above is simply to make the value ready for the next round.
                result += part;
                chunk++;
            }

            return result;
        }

        /* works because 18 > 32/2 so we can do it in one go */
        private static uint UndoTemperShiftL(uint value)
        {
            //After shift, the 18 foremost are the same as before. The last 14 get XORd with what is 
            //The leftmost 14 of the original value. But those survive the xor! So we just need to get them and xor them again
            uint last14 = value >> 18;
            return value ^ last14;
        }
    }
}
