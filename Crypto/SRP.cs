﻿using System;
using System.Linq;
using System.Text;
using System.Numerics;
using System.Security.Cryptography;

namespace Crypto.SRP
{
    public class SrpClient
    {
        public BigInteger A;

        private readonly SrpParameters _parameters;
        private BigInteger a;
        private BigInteger salt;
        private BigInteger B;
        private BigInteger u;
        private byte[] K;

        public SrpClient(SrpParameters parameters)
        {
            _parameters = parameters;

            a = DiffieHellman.RandomIntegerBelow(_parameters.N);
            A = SrpProtocol.ModExp(_parameters.g, a, _parameters.N);
        }

        public void ProtocolStep2_SendSaltAndB(BigInteger salt, BigInteger B)
        {
            this.salt = salt;
            this.B = B;

            // Generate U
            this.u = SrpProtocol.HashIntoNewInteger(A, B);

            /*
             * Generate string xH=SHA256(salt|password)
                Convert xH to integer x somehow (put 0x on hexdigest)
                Generate S = (B - k * g**x)**(a + u * x) % N
                Generate K = SHA256(S)
             */

            var rawPassword = Encoding.ASCII.GetBytes(this._parameters.Password);
            var x = SrpProtocol.FromStringToBigInt(rawPassword, this.salt);

            var baseValue = BigInteger.Remainder(B - this._parameters.k * SrpProtocol.ModExp(this._parameters.g, x, this._parameters.N), this._parameters.N);
            var exponent = this.a + this.u * x; // TODO: can we reduce mod here for efficiency? if the exponent is a multiple somehow, then removing those factors sohulndt change anything right?
            var S = SrpProtocol.ModExp(baseValue, exponent, _parameters.N);

            K = SrpProtocol.BigIntToByteArray(S);
        }

        public byte[] ComputeMac()
        {
            using (var hmac = new HMACSHA256(K))
            {
                var rawSalt = SrpProtocol.BigIntToByteArray(this.salt);
                return hmac.ComputeHash(rawSalt, 0, rawSalt.Length);
            }
        }

    }

    public class SrpServer
    {
        public BigInteger salt;
        public BigInteger B;

        private readonly SrpParameters _parameters;
        private readonly BigInteger v;
        private readonly BigInteger b;
        private BigInteger A;
        private BigInteger u;
        private byte[] K;

        public SrpServer(SrpParameters parameters)
        {
            _parameters = parameters;

            salt = DiffieHellman.RandomIntegerBelow(_parameters.N);
            var x = SrpProtocol.FromStringToBigInt(Encoding.ASCII.GetBytes(this._parameters.Password), salt);
            this.v = SrpProtocol.ModExp(_parameters.g, x, _parameters.N);
            this.b = DiffieHellman.RandomIntegerBelow(_parameters.N);

            // B = kv + g^b mod N, first get g^b mod N
            var g_to_the_b = SrpProtocol.ModExp(_parameters.g, this.b, _parameters.N);

            // Dont reduce mod N just yet. Since we later do B - k*g^x, which can never be negative, but it CAN happen if we reduce mod B here,
            // and not AFTER subtraction. It should never be negative since B = k*g^x + g^b > k*g^x
            this.B = _parameters.k * this.v + g_to_the_b;
        }

        public void ProtocolStep1_SendIandA(string I, BigInteger A)
        {
            if (I != _parameters.Email)
                throw new ArgumentException("Nope");

            this.A = A;

            // Generate U
            this.u = SrpProtocol.HashIntoNewInteger(A, B);

            /* Generate S = (A * v**u) ** b % N
                Generate K = SHA256(S) */

            var baseValue = BigInteger.Remainder(A * SrpProtocol.ModExp(this.v, u, _parameters.N), _parameters.N);
            var S = SrpProtocol.ModExp(baseValue, this.b, _parameters.N);

            K = SrpProtocol.BigIntToByteArray(S);
        }

        public bool ValidateMac(byte[] mac)
        {
            using (var hmac = new HMACSHA256(K))
            {
                var rawSalt = SrpProtocol.BigIntToByteArray(this.salt);
                var computedMac = hmac.ComputeHash(rawSalt, 0, rawSalt.Length);

                bool correct = true;

                // Go through all entries to not leak anything with timing. Doesnt matter since this is toybox example but still..
                for (int i = 0; i < mac.Length; i++)
                {
                    if (mac[i] != computedMac[i])
                        correct = false;
                }

                return correct;
            }            
        }
    }

    public static class SrpProtocol
    {
        public delegate BigInteger ModularExp(BigInteger value, BigInteger exponent, BigInteger modules);

        public static ModularExp ModExp = DiffieHellman.ModExp;

        public static BigInteger FromStringToBigInt(byte[] Password, BigInteger salt)
        {
            using (var sha256 = new SHA256CryptoServiceProvider())
            {
                var hashInput = salt.ToByteArray().Concat(Password).ToArray();
                var hash = sha256.ComputeHash(hashInput, 0, hashInput.Length);
                hash[hash.Length - 1] &= 0x7F; //Force to be positive integer? Little endian!
                return new BigInteger(hash);
            }
        }

        public static BigInteger FromStringToBigInt(byte[] str)
        {
            using (var sha256 = new SHA256CryptoServiceProvider())
            {
                var hashInput = str;
                var hash = sha256.ComputeHash(hashInput, 0, hashInput.Length);
                hash[hash.Length - 1] &= 0x7F; //Force to be positive integer? Little endian!
                return new BigInteger(hash);
            }
        }

        public static BigInteger HashIntoNewInteger(BigInteger A, BigInteger B)
        {
            using (var sha256 = new SHA256CryptoServiceProvider())
            {
                var Araw = A.ToByteArray();
                var Braw = B.ToByteArray();

                var message = Araw.Concat(Braw).ToArray();

                var uH = sha256.ComputeHash(message, 0, message.Length);
                uH[uH.Length - 1] &= 0x7F; //Force to be positive integer? Little endian!

                return new BigInteger(uH);
            }
        }

        public static byte[] BigIntToByteArray(BigInteger S)
        {
            using (var sha256 = new SHA256CryptoServiceProvider())
            {
                var rawS = S.ToByteArray();
                return sha256.ComputeHash(rawS, 0, rawS.Length);
            }
        }

        public static byte[] ComputeMac(byte[] key, BigInteger salt)
        {
            using (var hmac = new HMACSHA256(key))
            {
                var rawSalt = SrpProtocol.BigIntToByteArray(salt);
                return hmac.ComputeHash(rawSalt, 0, rawSalt.Length);
            }
        }
    }

    public class SrpParameters
    {
        public BigInteger N { get; set; }
        public BigInteger g { get; set; }
        public BigInteger k { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public SrpParameters()
        {

        }
        
    }

    public class SimplifiedSrpClient
    {
        SrpParameters _parameters;

        private BigInteger a;
        private BigInteger s;

        public SimplifiedSrpClient(SrpParameters parameters)
        {
            _parameters = parameters;
        }

        public BigInteger ComputeA()
        {
            if(a == null)
            {
                a = DiffieHellman.RandomIntegerBelow(_parameters.N);
            }

            return SrpProtocol.ModExp(_parameters.g, a, _parameters.N);
        }

        public void ComputeSecret(BigInteger B, BigInteger salt, BigInteger u)
        {
            var x = SrpProtocol.FromStringToBigInt(Encoding.ASCII.GetBytes(_parameters.Password), salt);
            var exponent = this.a + (u * x);
            s = SrpProtocol.ModExp(B, exponent, _parameters.N);
        }

        public byte[] ComputeMac(BigInteger salt)
        {
            using (var sha256 = new SHA256CryptoServiceProvider())
            {
                var key = sha256.ComputeHash(SrpProtocol.BigIntToByteArray(s));

                using (var hmac = new HMACSHA256(key))
                {
                    var rawSalt = SrpProtocol.BigIntToByteArray(salt);
                    return hmac.ComputeHash(rawSalt, 0, rawSalt.Length);
                }
            }
        }
    }

    public class SimplifiedSrpServer
    {
        SrpParameters _parameters;

        private BigInteger b;
        private BigInteger s;
        public BigInteger salt;
        public BigInteger u;

        public SimplifiedSrpServer(SrpParameters parameters)
        {
            _parameters = parameters;
            salt = DiffieHellman.RandomIntegerBelow(_parameters.N);
            u = SrpProtocol.FromStringToBigInt(AESWrapper.RandomKey());
        }

        public BigInteger ComputeB()
        {
            if(b == null)
            {
                b = DiffieHellman.RandomIntegerBelow(_parameters.N);
            }

            return SrpProtocol.ModExp(_parameters.g, b, _parameters.N);
        }

        public void ComputeSecret(BigInteger A)
        {
            var x = SrpProtocol.FromStringToBigInt(Encoding.ASCII.GetBytes(_parameters.Password), salt);
            var v = SrpProtocol.ModExp(_parameters.g, x, _parameters.N);
            s = SrpProtocol.ModExp(A * SrpProtocol.ModExp(v, u, _parameters.N), b, _parameters.N);
        }

        public bool ValidateMac(byte[] mac)
        {
            using (var sha256 = new SHA256CryptoServiceProvider())
            {
                var key = sha256.ComputeHash(SrpProtocol.BigIntToByteArray(this.s));
                var computedMac = SrpProtocol.ComputeMac(key, salt);

                bool equal = true;

                for(int i = 0; i < computedMac.Length; i++)
                {
                    if (computedMac[i] != mac[i]) equal = false;
                }

                return equal;
            }
        }        
    }
}
