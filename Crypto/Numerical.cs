﻿using System;
using System.Linq;
using System.Numerics;

namespace Crypto.Numerical
{
    public static class BigIntegerUtility
    {
        /* https://stackoverflow.com/questions/1082917/mod-of-negative-number-is-melting-my-brain */
        public static BigInteger mod(BigInteger x, BigInteger m)
        {
            return (x % m + m) % m;
        }

        public static BigInteger FromHexStringToPositive(string hexstring)
        {
            // We preprend zero to force positive. See https://stackoverflow.com/questions/30119174/converting-a-hex-string-to-its-biginteger-equivalent-negates-the-value
            var fixedString = "0" + hexstring;

            return BigInteger.Parse(fixedString, System.Globalization.NumberStyles.HexNumber);
        }

        public static BigInteger FloorRoot(BigInteger number, int rootDegree)
        {
            var bits = number.ToByteArray().Length * 8;
            int p = (bits + rootDegree - 1) / rootDegree; /* This is integer division ceiled. source: http://www.cs.nott.ac.uk/~psarb2/G51MPC/slides/NumberLogic.pdf */
            var x = Math.Pow(2, p);
            var BigX = new BigInteger(x);
            BigInteger y;

            while(x > 1)
            {
                y = (((rootDegree - 1) * BigX) + (number / (BigInteger.Pow(BigX, rootDegree - 1)))) / rootDegree;

                if (y >= BigX)
                {
                    return BigX;
                }

                BigX = y;
            }

            return 1;        
        }

        public static BigInteger ModInverse(BigInteger b,
            BigInteger m)
        {
            if (m.Sign < 1)
                throw new ArithmeticException("Modulus must be positive");

            BigInteger d = BigInteger.Remainder(b, m);
            BigInteger u;
            BigInteger v;
            BigInteger gcd = ExtendedEuclidGcd(d, m, out u, out v);

            if (!gcd.Equals(BigInteger.One))
                throw new ArithmeticException("Numbers not relatively prime.");

            if (u.Sign < 0)
            {
                u = u + m;
            }

            return u;
        }

        /// <summary>
        /// Extended Euclidian Gcd algorithm, returning the 
        /// greatest common divisor of two BigIntegers,
        /// while also providing u and v, where: a*u + b*v = gcd(a,b).
        /// </summary>
        /// <param name="u">Output BigInteger parameter, where a*u + b*v = gcd(a,b)
        /// </param>
        /// <param name="v">Output BigInteger parameter, where a*u + b*v = gcd(a,b)
        /// </param>
        /// <returns>The greatest common divisor of the two given BigIntegers</returns>
        /// <exception cref="BigIntegerException">
        /// Cannot compute the Gcd of negative BigIntegers exception</exception>
        public static BigInteger ExtendedEuclidGcd(BigInteger a, BigInteger b,
                                                   out BigInteger u, out BigInteger v)
        {
            if ((a.Sign < 0) || (b.Sign < 0))
                throw new ArgumentException
              ("Cannot compute the Gcd of negative BigIntegers.", "");

            BigInteger u1 = new BigInteger();
            BigInteger u2 = new BigInteger(1);
            BigInteger v1 = new BigInteger(1);
            BigInteger v2 = new BigInteger();
            BigInteger q, r;

            u = new BigInteger();
            v = new BigInteger();

            while (b > BigInteger.Zero)
            {
                q = a / b;
                r = a - q * b;
                u = u2 - q * u1;
                v = v2 - q * v1;

                a = new BigInteger(b.ToByteArray());
                b = new BigInteger(r.ToByteArray());
                u2 = new BigInteger(u1.ToByteArray());
                u1 = new BigInteger(u.ToByteArray());
                v2 = new BigInteger(v1.ToByteArray());
                v1 = new BigInteger(v.ToByteArray());
                u = new BigInteger(u2.ToByteArray());
                v = new BigInteger(v2.ToByteArray());
            }

            return a;
        }
    }
}
