﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using Mpir.NET;
using System.Security.Cryptography;

namespace Crypto
{
    public delegate BigInteger MODEXP(BigInteger value, BigInteger exponent, BigInteger modulus);

    public struct SemanticSecureMessage
    {
        public byte[] Data { get; }
        public byte[] Iv { get; }

        public SemanticSecureMessage(byte[] data, byte[] iv)
        {
            Data = data;
            Iv = iv;
        }
    }

    /// <summary>
    /// The math: A = g ^ a mod p. B = g ^ b mod p. Since Discrete Log is assumed hard in certain groups, seeing A or B, even knowing g and p, it is infeasible to calculate the random, secret a and b, since DL is hard.
    /// This means A and B can freely be exchanged between alice and bob aftter they do the math locally.
    /// 
    /// In fact A and B are public Keys for DL based Public Key crypto. 
    /// 
    /// The cool thing is that Alice and Bob can construct the same secret S after exchanging public keys, since they each have the secrets a and b locally respectively.
    /// 
    /// Observe that S = B^a mod p = (g^b)^a mod p = g^ba mod p = g^ab mod p = (g^a)^b mod p = A^b mod P. They can each calcualte the same S (which can be used as a session key for symmetric crypto in the following session).
    /// /// </summary>
    public class DiffieHellman
    {
        public byte[] GetKeyFromSecret(BigInteger secret)
        {
            return SHA1.Hash(secret.ToByteArray()).Take(16).ToArray();
        }

        public class Alice
        {
            private BigInteger a;
            private BigInteger p;
            private BigInteger g;
            private BigInteger s;

            private MODEXP _modexp;

            public BigInteger A { get; }

            public Alice(BigInteger p, BigInteger g)
            {
                this.p = p;
                this.g = g;

                this._modexp = DiffieHellman.ModExp;

                this.a = RandomIntegerBelow(p);
                this.A = _modexp(g, a, p);
            }

            public Alice(BigInteger p, BigInteger g, MODEXP modexp)
            {
                this.p = p;
                this.g = g;

                this._modexp = modexp;

                this.a = RandomIntegerBelow(p);
                this.A = _modexp(g, a, p);
            }

            public BigInteger GetSecret(BigInteger B)
            {
                if (s != null) return s;

                s = _modexp(B, a, p);

                return s;
            }

            public void GenerateSecret(BigInteger B)
            {
                s = _modexp(B, a, p);
            }

            public SemanticSecureMessage Encrypt(string message)
            {
                var messageBytes = Encoding.ASCII.GetBytes(message);

                if (s == null) throw new Exception("Secret not generated");

                var key = SHA1.Hash(this.s.ToByteArray()).Take(16).ToArray();
                var iv = AESWrapper.RandomKey(16);

                var encrypted = AESWrapper.CBC.Encrypt(messageBytes, key, iv);

                return new SemanticSecureMessage(encrypted, iv);
            }

            public string Decrypt(SemanticSecureMessage message)
            {
                var key = SHA1.Hash(s.ToByteArray()).Take(16).ToArray();

                return Encoding.ASCII.GetString(AESWrapper.CBC.Decrypt(message.Data, key, message.Iv));
            }
        }

        public class Bob
        {
            private BigInteger b;
            private BigInteger p;
            private BigInteger g;
            private BigInteger s;

            private MODEXP _modexp;

            public BigInteger B { get; }

            public Bob(BigInteger p, BigInteger g)
            {
                this.p = p;
                this.g = g;

                this._modexp = DiffieHellman.ModExp;

                this.b = RandomIntegerBelow(p);
                this.B = _modexp(g, b, p);
            }

            public Bob(BigInteger p, BigInteger g, MODEXP modexp)
            {
                this.p = p;
                this.g = g;

                this._modexp = modexp;

                this.b = RandomIntegerBelow(p);
                this.B = _modexp(g, b, p);
            }

            public BigInteger GetSecret(BigInteger A)
            {
                if (s != null) return s;

                s = _modexp(A, b, p);

                return s;
            }

            public void GenerateSecret(BigInteger A)
            {
                s = _modexp(A, b, p);
            }

            public SemanticSecureMessage Echo(SemanticSecureMessage encrypted)
            {
                if (s == null) throw new Exception("Must generate Secret first");

                var key = SHA1.Hash(s.ToByteArray()).Take(16).ToArray();

                var decrypted = AESWrapper.CBC.Decrypt(encrypted.Data, key, encrypted.Iv);

                var iv = AESWrapper.RandomKey(16);

                var encryptedAgain = AESWrapper.CBC.Encrypt(decrypted, key, iv);

                return new SemanticSecureMessage(encryptedAgain, iv);
            }
        }

        private BigInteger p;
        private BigInteger g;
        private MODEXP _modexp;

        public Alice alice { get; private set; }
        public Bob bob { get; private set; }

        public bool IsValid { get { return (alice.GetSecret(bob.B) == bob.GetSecret(alice.A)); } }

        public static BigInteger RandomIntegerBelow(BigInteger N)
        {
            byte[] bytes = N.ToByteArray();
            BigInteger R;
            RNGCryptoServiceProvider secure_getrandom = new RNGCryptoServiceProvider();

            do
            {
                secure_getrandom.GetBytes(bytes);
                bytes[bytes.Length - 1] &= (byte)0x7F; //force sign bit to positive
                R = new BigInteger(bytes);
            }
            while (R >= N);

            return R;
        }

        public DiffieHellman(BigInteger p, BigInteger g)
        {
            this.p = p;
            this.g = g;

            Initialize();
        }

        /// <summary>
        /// Parses P from hex into a bigint.
        /// </summary>
        /// <param name="p">hex representation of big integer</param>
        /// <param name="g"></param>
        public DiffieHellman(string p, BigInteger g)
        {
            this.p = BigInteger.Parse(p, System.Globalization.NumberStyles.HexNumber);

            this.g = g;

            Initialize();
        }

        public DiffieHellman(BigInteger p, BigInteger g, MODEXP modexp)
        {
            this.p = p;
            this.g = g;

            this._modexp = modexp;

            Initialize();
        }

        private void Initialize()
        {
            if (this._modexp == null)
                _modexp = ModExp;

            alice = new Alice(this.p, this.g, _modexp);
            bob = new Bob(this.p, this.g, _modexp);
        }

        /// <summary>
        /// Based on Bruce Schneiers pseudocode in applied cryptography.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="exponent"></param>
        /// <param name="modulus"></param>
        /// <returns></returns>
        public static BigInteger ModExp(BigInteger value, BigInteger exponent, BigInteger modulus)
        {
            if (exponent.IsZero) return BigInteger.One;
            if (exponent.IsOne) return value;
            if (0 > exponent.Sign) throw new ArgumentException("Exponent negative");

            var result = BigInteger.One;
            var baze = value % modulus;
            do
            {
                if (!exponent.IsEven)
                {
                    result = (result * baze) % modulus;
                }

                exponent = BigInteger.Divide(exponent, 2);
                baze = BigInteger.ModPow(baze, 2, modulus);
            }
            while (!exponent.IsZero);

            return result;
        }

        /// <summary>
        /// Wrapper for the MRP GNU Arbitrary precision libraries. 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="exponent"></param>
        /// <param name="modulus"></param>
        /// <returns></returns>
        public static BigInteger mpir_ModExp(BigInteger value, BigInteger exponent, BigInteger modulus)
        {
            mpz_t _value = new mpz_t(value);
            mpz_t _exponent = new mpz_t(exponent);
            mpz_t _modulus = new mpz_t(modulus);

            var result = _value.PowerMod(_exponent, _modulus);

            return result.ToBigInteger();
        }
    }
}
