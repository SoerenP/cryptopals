﻿using Crypto.Numerical;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Crypto
{
    public static class NumberTheory
    {
        /// <summary>
        /// Taken from Handbook of Applied Cryptography, algorithm 2.121. 
        /// 
        /// The solution x to the simultanious congruende in the chinese remainder theorem may be
        /// computed as x = sum_i=1^k ai*Ni*Mi mod n, where Ni = n/ni, Mi = Ni^-1 mod ni, and n = n1..nk
        /// </summary>
        /// <param name="ais">Ordered list of ai's</param>
        /// <param name="moduli">Ordered list of ni's</param>
        /// <returns></returns>
        public static BigInteger GaussAlgorithm(List<BigInteger> ais, List<BigInteger> moduli)
        {
            BigInteger N = moduli.Aggregate((result, item) => result * item);
            int k = moduli.Count();

            BigInteger x = BigInteger.Zero;
            BigInteger N_i = BigInteger.Zero;
            BigInteger M_i = BigInteger.Zero;

            for(int i = 0; i < k; i++)
            {
                N_i = N / moduli[i];
                M_i = BigIntegerUtility.ModInverse(N_i, moduli[i]);
                x += ais[i] * N_i * M_i;
            }

            return x % N;
        }
    }
}
