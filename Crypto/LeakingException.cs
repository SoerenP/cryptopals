﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto
{
    class LeakingException : Exception
    {
        public readonly byte[] NonAsciiPlaintext;
        public readonly string ErrorMessage;

        public LeakingException(string errorMessage, byte[] nonAsciiPlaintext)
        {
            this.NonAsciiPlaintext = nonAsciiPlaintext;
            this.ErrorMessage = errorMessage;
        }
       
    }
}
