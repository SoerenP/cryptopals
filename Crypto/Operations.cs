﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto
{
    public static class Operations
    {
        private static readonly int[] LookupTable =
        Enumerable.Range(0, 256).Select(CountBits).ToArray();

        private static int CountBits(int value)
        {
            int count = 0;
            for(int i = 0; i < 8; i++)
            {
                count += (value >> i) & 1;
            }
            return count;
        }

        public static string FixedBase64XOR(string buffer1_base64string, string buffer2_base64string)
        {
            if (buffer1_base64string.Length != buffer2_base64string.Length)
                throw new ArgumentException("Buffers should have equal size");

            byte[] buffer1_byte = Convert.FromBase64String(buffer1_base64string);
            byte[] buffer2_byte = Convert.FromBase64String(buffer2_base64string);
            byte[] XORd = new byte[buffer1_byte.Length];

            for (int i = 0; i < XORd.Length; i++)
            {
                XORd[i] = (byte)(buffer1_byte[i] ^ buffer2_byte[i]);
            }
            string result = Convert.ToBase64String(XORd);
            return result;
        }

        public static byte[] FixedBitwiseXOR(byte[] buffer, byte[] key)
        {
            if (buffer.Length != key.Length) throw new ArgumentException("Buffer and key must have same length");
            byte[] XORd = new byte[buffer.Length];

            for (int i = 0; i < XORd.Length; i++)
            {
                XORd[i] = (byte)(buffer[i] ^ key[i]);
            }
            return XORd;
        }

        public static byte[] FixedBitwiseXOR(byte[] buffer, byte key)
        {
            byte[] XORd = new byte[buffer.Length];
            for (int i = 0; i < XORd.Length; i++)
            { 
                XORd[i] = (byte)(buffer[i] ^ key);
            }
            return XORd;
        }

        public static byte[] RepeatingKeyXOR(string ascii_buffer, string ascii_key)
        {
            string hex_buffer = Converter.FromStringToHex(ascii_buffer);
            byte[] raw_buffer = Converter.FromHexToBase64Byte(hex_buffer);
            byte[] XORd = new byte[raw_buffer.Length];

            for (int i = 0; i < XORd.Length; i++)
            {
                char roundkey = ascii_key[i % ascii_key.Length];
                XORd[i] = (byte)(raw_buffer[i] ^ Convert.ToByte(roundkey));
            }

            return XORd;
        }

        public static byte[] RepeatingKeyXOR(byte[] buffer, byte[] repeating_key)
        {
            byte[] XORd = new byte[buffer.Length];
            for (int i = 0; i < buffer.Length; i++)
            {
                XORd[i] = (byte)(buffer[i] ^ repeating_key[i % repeating_key.Length]);
            }
            return XORd;
        }

        public static int EditDistance(string left_buffer, string right_buffer)
        {
            if (left_buffer.Length != right_buffer.Length) throw new ArgumentException("buffers must have equal length");

            int distance = 0;
            byte[] raw_left = Converter.FromHexToBase64Byte(Converter.FromStringToHex(left_buffer));
            byte[] raw_right = Converter.FromHexToBase64Byte(Converter.FromStringToHex(right_buffer));
            int length = raw_left.Length;
            for (int i = 0; i < length; i++)
            {
                byte temp = (byte)(raw_left[i] ^ raw_right[i]);
                distance += LookupTable[temp];
            }
            return distance;
        }

        public static int EditDistance(byte[] left_buffer, byte[] right_buffer)
        {
            if (left_buffer.Length != right_buffer.Length) throw new ArgumentException("buffers must have equal length");

            int distance = 0;
            int length = left_buffer.Length;
            for(int i = 0; i < length; i++)
            {
                byte temp = (byte)(left_buffer[i] ^ right_buffer[i]);
                distance += LookupTable[temp];
            }
            return distance;
        }

        public static int AverageEditDistance(byte[] buffer, int blocksize)
        {
            if (2 * buffer.Length < blocksize) throw new ArgumentException("buffer should be at least twice as big as blocksize");
            int distance = 0;
            int distances_compared = 0;
            int blocks = (int)Math.Ceiling((float)buffer.Length / (float)blocksize);

            List<byte[]> result = new List<byte[]>();
            for (int i = 0; i < blocks; i++)
            {
                byte[] temp = new byte[blocksize];
                for (int j = 0; j < blocksize; j++)
                {
                    temp[j] = buffer[i * blocksize + j];
                }
                result.Add(temp);
            }

            for(int i = 0; i < blocks-1; i++)
            {
                byte[] front = result[i];
                for(int j = i+1; j < blocks; j++)
                {
                    byte[] temp = result[j];
                    string temp_hex = BitConverter.ToString(temp);
                    distance += EditDistance(front,temp);
                    distances_compared++;
                }
            }

            return distance / distances_compared;
        }


        /* pads the data buffer so it has blocksize in bytes, according to the PKCS#7 padding scheme */
        /* TODO brug delegate til encoding af string til at returnere? */
        public static string PKCSHashtag7Padding(string ascii, int blocksize)
        {
            string data_hex = Converter.FromStringToHex(ascii).Replace("-", string.Empty);
            byte[] data_raw = Converter.FromHexToBase64Byte(data_hex);
            byte[] result = PKCSHashtag7Padding(data_raw, blocksize);
            return Encoding.ASCII.GetString(result);
        }

        public static byte[] PKCSHashtag7Padding(byte[] data, int blocksize_bytes)
        {
            int max_blocksize = 256;
            if (blocksize_bytes > max_blocksize) throw new ArgumentException("blocksize must be less than 256");

            int bytes_to_pad = blocksize_bytes - (data.Length % blocksize_bytes);
            Console.WriteLine("bytes to pad {0}", bytes_to_pad);
            int padded_length = data.Length + bytes_to_pad;
            byte[] padded = new byte[padded_length];

            for (int i = 0; i < data.Length; i++)
            {
                padded[i] = data[i];
            }

            for (int i = data.Length; i < padded_length; i++)
            {
                padded[i] = (byte)bytes_to_pad;
            }

            return padded;
        }

        public static string PKCSHashtag7Validation(string data, int blocksize)
        {
            return Encoding.ASCII.GetString(PKCSHashtag7Validation(Encoding.ASCII.GetBytes(data), blocksize));
        }

        /* the min padding will be 1 */
        /* the last byte should denote how many bytes we should strip off. strip them, and check that they are all equal */
        public static byte[] PKCSHashtag7Validation(byte[] data, int blocksize)
        {
            int max = blocksize;
            int min = 1;
            
            int padding_amount = (int)data[data.Length-1];

            bool is_number = (padding_amount <= max) && (padding_amount >= min);
            if (!is_number) throw new ArgumentException("Invalid Padding");

            byte[] padding = data.Skip(data.Length - padding_amount).Take(padding_amount).ToArray();
            byte first = padding[0];
            for (int i = 1; i < padding.Length; i++)
            {
                byte next = padding[i];
                if (!first.Equals(next)) throw new ArgumentException("Invalid Padding");
            }

            byte[] stripped = data.Take(data.Length - padding_amount).ToArray();

            return stripped;
        }

        public static uint ToUnixTimeStamp(DateTime timestamp)
        {
            return (uint)(timestamp.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }
    }
}
