﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using PROFILE = System.Collections.Generic.Dictionary<string, string>;

namespace Crypto
{
    public static class ProfileFactory
    {
        private static string meta_eq = "=";
        private static string meta_next_pair = "&";
        private static char meta_next_pair_char = '&';
        private static char meta_eq_char = '=';

        private static int userId;
        private static byte[] AES_KEY;

        static ProfileFactory()
        {
            AES_KEY = AESWrapper.RandomKey();
            userId = 0;
        }

        private static int NextUserId()
        {
            userId++;
            return userId;
        }

        public static string UserToCookie(PROFILE user)
        {
            string cookie = string.Empty;
            string[] keys = user.Keys.ToArray();

            for (int i = 0; i < keys.Length; i++)
            {
                string key = keys[i];
                string value = user[key];
                cookie += key + meta_eq + value;
                if (i != keys.Length - 1) cookie += meta_next_pair;
            }

            return cookie;
        }

        public static PROFILE KEqualsVCookieParser(string cookie)
        {
            string[] pairs = cookie.Split(meta_next_pair_char);

            Dictionary<string, string> user = new Dictionary<string, string>();

            for (int i = 0; i < pairs.Length; i++)
            {
                string current = pairs[i];
                int eq_index = current.IndexOf(meta_eq);
                if (eq_index > 0)
                {
                    string key = current.Substring(0, eq_index);
                    string value = "'" + current.Substring(eq_index + 1).TrimEnd(new char[] { '\0' }) + "'"; //quote all values and remove potential NUL from encryption/decryption
                    try
                    {
                        user.Add(key, value);
                    }
                    catch (ArgumentException e) { Console.WriteLine("tried to write key " + key.ToString()); }
                }
                else
                {
                    string key = "error";
                    string value = "no value to key: " + current;
                    user.Add(key, value);
                }
            }

            return user;
        }

        public static void PrintUser(PROFILE user)
        {
            string[] keys = user.Keys.ToArray();
            for (int i = 0; i < user.Count; i++)
            {
                Console.WriteLine("key: {0}, value: {1}", keys[i], user[keys[i]]);
            }
        }

        public static string ToString(PROFILE user)
        {
            string[] keys = user.Keys.ToArray();
            string result = String.Empty;
            for (int i = 0; i < user.Count; i++)
            {
                result = result + $"key: {keys[i]}, value: {user[keys[i]]}";
            }
            return result;
        }

        public static void PrintUser(StreamWriter sw, PROFILE user)
        {
            string[] keys = user.Keys.ToArray();
            for (int i = 0; i < user.Count; i++)
            {
                sw.WriteLine("key: {0}, value: {1}", keys[i], user[keys[i]]);
            }
        }

        public static byte[] ProfileForEncrypted(string email)
        {
            string encoded = ProfileForUser(email);
            byte[] raw = Encoding.ASCII.GetBytes(encoded);
            return AESWrapper.ECB.Encrypt(raw, AES_KEY, Enumerable.Repeat((byte)0, 16).ToArray());
        }

        public static PROFILE DecryptAndParseUserCookie(byte[] user_cookie_encrypted)
        {
            string decrypted = Encoding.ASCII.GetString(AESWrapper.ECB.Decrypt(user_cookie_encrypted, AES_KEY, Enumerable.Repeat((byte)0, 16).ToArray()));
            return KEqualsVCookieParser(decrypted);
        }

        public static string ProfileForUser(string email)
        {
            /* remove all metacharacters from email */
            string fixed_email = string.Empty;
            char[] chunked_email = email.ToArray<char>();
            for(int i = 0; i < email.Length; i++)
            {
                if (IsCharLegal(chunked_email[i]))
                {
                    fixed_email += chunked_email[i].ToString();
                }
            }

            /* add user id field and role field */
            PROFILE user = new PROFILE();
            user.Add("email", fixed_email);
            user.Add("uid", NextUserId().ToString());
            user.Add("role", "user");

            return UserToCookie(user);
        }

        private static bool IsCharLegal(char c)
        {
            return !((c == meta_next_pair_char) || (c == meta_eq_char));
        }
    }
}
