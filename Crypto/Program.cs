﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto
{
    class Program
    {
        static void Main(string[] args)
        {
            var rng = new MT19937(1488817778);

            var output = rng.ExtractNumber();

            Console.WriteLine(output);

            Console.ReadLine();
        }
    }
}
