﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Diagnostics;

namespace Crypto
{
    public static class Converter
    {
        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                .ToArray();
        }

        public static string FromBase64StringToASCII(string base64)
        {
            byte[] data = Convert.FromBase64String(base64);
            return Encoding.ASCII.GetString(data);
        }

        public static byte[] FromHexToBase64Byte(string hex)
        {
            hex = hex.Replace("-", "");
            byte[] result = new byte[hex.Length / 2];
            for(int i = 0; i < result.Length; i++)
            {
                result[i] = Convert.ToByte(hex.Substring(i * 2,2),16);
            }
            return result;
        }
    
        public static string FromHexToBase64String(string hex)
        {
            hex = hex.Replace("-", "");
            byte[] result = new byte[hex.Length / 2];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            }
            return Convert.ToBase64String(result);
        }

        public static string FromStringToHex(string asciiString)
        {
            string hex = "";
            foreach (char c in asciiString)
            {
                int tmp = c;
                hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
            }
            return hex;
        }

        public static string FromBytesToHex(byte[] bytes)
        {
            return BitConverter.ToString(bytes).Replace("-", "");
        }

        public static byte[] FromHexToBytes(string hex)
        {
            hex = hex.Replace("-", "");
            byte[] bytes = new byte[hex.Length / 2];
            for(int i = 0; i < bytes.Length; i++)
            {
                bytes[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            }
            return bytes;
        }

        public static byte[,] From1Dto2DbyteArray(int height, int width, byte[] source)
        {
            byte[,] res = new byte[height, width];

            for (int i = 0; i < height - 1; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    res[i, j] = source[i * width + j];
                }
            }

            /* handle the last block explicitly, it might not be alligned properly */
            /* maybe whatever is "left" in the last few entries are perturbing things? Could insert == which is ignored in base64`*/
            int lastblock = height - 1;
            for (int j = 0; j < source.Length % width; j++)
            {
                res[lastblock, j] = source[lastblock * width + j];
            }

            return res;
        }

        public static byte[,] Transpose2DByteArray(int height, int width, byte[,] source)
        {
            byte[,] res = new byte[width, height];
            for (int trans_blocks = 0; trans_blocks < width; trans_blocks++)
            {
                for (int blocks = 0; blocks < height; blocks++)
                {
                    res[trans_blocks, blocks] = source[blocks, trans_blocks];
                }
            }
            return res;
        }

        public static void Print2DByteArray(byte[,] input, int rows, int columns, System.IO.StreamWriter log)
        {
            log.WriteLine("Printing 2D array of {0} x {1} dim",rows,columns);
            byte[] temp = new byte[columns];
            for(int i = 0; i < rows; i++)
            {
                for(int j = 0; j < columns; j++)
                {
                    temp[j] = input[i, j];
                }
                var str = Convert.ToBase64String(temp);
                log.WriteLine("row {0}: " + str.ToString(), i);
            }
        }

        public static List<byte[]> Base64FileToListOfByteArrays(string filename)
        {
            string line;

            var result = new List<byte[]>();

            StreamReader sr = new StreamReader(filename);
            try
            {
                while ((line = sr.ReadLine()) != null)
                {
                    result.Add(Convert.FromBase64String(line));
                }
            }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                    ((IDisposable)sr).Dispose();
                }

            }

            return result;
        }

        public static byte[] Base64FileToByteArray(string filename)
        {
            string input = string.Empty;
            string line;
            StreamReader sr = new StreamReader(filename);
            try
            {
                while ((line = sr.ReadLine()) != null)
                {
                    input += line;
                }
            }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                    ((IDisposable)sr).Dispose();
                }
                    
            }

            return Convert.FromBase64String(input);
        }

        public static List<byte[]> TruncateCiphers(List<byte[]> ciphers)
        {
            int shortest = int.MaxValue;

            foreach(var ciph in ciphers)
            {
                int temp = ciph.Length;
                if(temp < shortest)
                {
                    shortest = temp;
                }
            }

            var result = new List<byte[]>();

            foreach(var ciph in ciphers)
            {
                result.Add(ciph.Take(shortest).ToArray());
            }

            return result;
        }

        /* We assume ciphers are of equal lenght */
        public static List<byte[]> TransposeCiphers(List<byte[]> ciphers)
        {
            var result = new List<byte[]>();

            int newAmountOfRows = ciphers.First().Length;
            int newRowLength = ciphers.Count;

            byte[] temp = new byte[newRowLength];
            for(int i = 0; i < newAmountOfRows; i++)
            {
                int j = 0;
                foreach(var cipher in ciphers)
                {
                    var currentByte = cipher[i];
                    temp[j] = currentByte;
                    j++;
                }

                result.Add(temp.ToArray());
            }

            return result;
        }

    }
}
