﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using extensions;

namespace Crypto
{
    public class MD4KeyedMAC
    {
        private static Random _random = new Random();
        private static byte[] _randomKey = _random.GetBytes(20);

        public static byte[] GetMessageDigest(byte[] data)
        {
            var buffer = _randomKey.Concat(data).ToArray();
            return MD4.ComputeMD4(buffer);
        }

        public static bool IsValid(MessageWithIntegrity message)
        {
            var mac = MD4KeyedMAC.GetMessageDigest(message.Message);
            return mac.SequenceEqual(message.Mac);
        }
    }

    public static class MDKeyedMACExtensionAttacker
    {
        public static MessageWithIntegrity Forge(byte[] data, byte[] mac, byte[] extension)
        {
            var message = new MessageWithIntegrity
            {
                Mac = mac,
                Message = data
            };

            for (int i = 1; i < 30; i++)
            {
                var forged = ForgeWithExtension(extension, message, i);
                if(MD4KeyedMAC.IsValid(forged))
                {
                    return forged;
                }
            }

            return new MessageWithIntegrity();
        }

        private static MessageWithIntegrity ForgeWithExtension(byte[] extension, MessageWithIntegrity mac, int prefixBytes)
        {
            var md4 = new MD4();

            var padded = mac.Message.Concat(md4.Padding(mac.Message.Length + prefixBytes)).ToArray();

            md4.Initialize(mac.Mac, padded.Length + prefixBytes);
            md4.Hashcore(extension, 0, extension.Length);

            var forged = new MessageWithIntegrity
            {
                Mac = md4.HashFinally(),
                Message = padded.Concat(extension).ToArray()
            };

            return forged;
        }
    }

    public class MD4 : HashAlgorithm
    {
        private UInt32[] _x;

        private UInt32 _a;
        private UInt32 _b;
        private UInt32 _c;
        private UInt32 _d;

        private int _bytesProcessed;

        public static byte[] ComputeMD4(byte[] buffer)
        {
            using (HashAlgorithm hash = new MD4())
            {
                return hash.ComputeHash(buffer);
            }
        }

        public MD4()
        {
            _x = new UInt32[16];

            Initialize();
        }

        public override void Initialize()
        {
            _a = 0x67452301;
            _b = 0xefcdab89;
            _c = 0x98badcfe;
            _d = 0x10325476;

            _bytesProcessed = 0;
        }

        public void Initialize(byte[] hash, int previousBytes)
        {
            _bytesProcessed = previousBytes;

            var split = Words(hash).Take(4).ToArray();
            _a = split[0];
            _b = split[1];
            _c = split[2];
            _d = split[3];
        }

        protected override void HashCore(byte[] array, int ibStart, int cbSize)
        {
            ProcessMessage(Bytes(array, ibStart, cbSize));
        }

        public void Hashcore(byte[] array, int ibStart, int cbSize)
        {
            ProcessMessage(Bytes(array, ibStart, cbSize));
        }

        protected override byte[] HashFinal()
        {
            try
            {
                ProcessMessage(Padding(_bytesProcessed));

                return new[] { _a, _b, _c, _d }.SelectMany(word => Bytes(word)).ToArray();
            }
            finally
            {
                Initialize();
            }
        }

        public byte[] HashFinally()
        {
            try
            {
                ProcessMessage(Padding(_bytesProcessed));

                return new[] { _a, _b, _c, _d }.SelectMany(word => Bytes(word)).ToArray();
            }
            finally
            {
                Initialize();
            }
        }

        private void ProcessMessage(IEnumerable<byte> bytes)
        {
            foreach(byte b in bytes)
            {
                int c = _bytesProcessed & 63;
                int i = c >> 2;
                int s = (c & 3) << 3;

                _x[i] = (_x[i] & ~((uint)255 << s)) | ((uint)b << s);

                if(c == 63)
                {
                    Process16WordBlock();
                }

                _bytesProcessed++;
            }
        }

        private static IEnumerable<byte> Bytes(byte[] bytes, int offset, int length)
        {
            for(int i = offset; i < length; i++)
            {
                yield return bytes[i];
            }
        }

        private IEnumerable<uint> Words(byte[] hash)
        {
            yield return BitConverter.ToUInt32(hash, 0);
            yield return BitConverter.ToUInt32(hash, sizeof(uint));
            yield return BitConverter.ToUInt32(hash, sizeof(uint) * 2);
            yield return BitConverter.ToUInt32(hash, sizeof(uint) * 3);
        }

        private IEnumerable<byte> Bytes(uint word)
        {
            yield return (byte)(word & 255);
            yield return (byte)((word >> 8) & 255);
            yield return (byte)((word >> 16) & 255);
            yield return (byte)((word >> 24) & 255);
        }

        private IEnumerable<byte> Repeat(byte value, int count)
        {
            for(int i = 0; i < count; i++)
            {
                yield return value;
            }
        }

        public IEnumerable<byte> Padding(int bytesProcessed)
        {
            return Repeat(128, 1)
                .Concat(Repeat(0, ((bytesProcessed + 8) & 0x7fffffc0) + 55 - bytesProcessed))
                .Concat(Bytes((uint)bytesProcessed << 3))
                .Concat(Repeat(0, 4));
        }

        private void Process16WordBlock()
        {
            uint aa = _a;
            uint bb = _b;
            uint cc = _c;
            uint dd = _d;

            foreach (int k in new[] { 0, 4, 8, 12 })
            {
                aa = Round1Operation(aa, bb, cc, dd, _x[k], 3);
                dd = Round1Operation(dd, aa, bb, cc, _x[k + 1], 7);
                cc = Round1Operation(cc, dd, aa, bb, _x[k + 2], 11);
                bb = Round1Operation(bb, cc, dd, aa, _x[k + 3], 19);
            }

            foreach (int k in new[] { 0, 1, 2, 3 })
            {
                aa = Round2Operation(aa, bb, cc, dd, _x[k], 3);
                dd = Round2Operation(dd, aa, bb, cc, _x[k + 4], 5);
                cc = Round2Operation(cc, dd, aa, bb, _x[k + 8], 9);
                bb = Round2Operation(bb, cc, dd, aa, _x[k + 12], 13);
            }

            foreach (int k in new[] { 0, 2, 1, 3 })
            {
                aa = Round3Operation(aa, bb, cc, dd, _x[k], 3);
                dd = Round3Operation(dd, aa, bb, cc, _x[k + 8], 9);
                cc = Round3Operation(cc, dd, aa, bb, _x[k + 4], 11);
                bb = Round3Operation(bb, cc, dd, aa, _x[k + 12], 15);
            }

            unchecked
            {
                _a += aa;
                _b += bb;
                _c += cc;
                _d += dd;
            }
        }

        private static uint ROL(uint value, int numberOfBits)
        {
            return (value << numberOfBits) | (value >> (32 - numberOfBits));
        }

        private static uint Round1Operation(uint a, uint b, uint c, uint d, uint xk, int s)
        {
            unchecked
            {
                return ROL(a + ((b & c) | (~b & d)) + xk, s);
            }
        }

        private static uint Round2Operation(uint a, uint b, uint c, uint d, uint xk, int s)
        {
            unchecked
            {
                return ROL(a + ((b & c) | (b & d) | (c & d)) + xk + 0x5a827999, s);
            }
        }

        private static uint Round3Operation(uint a, uint b, uint c, uint d, uint xk, int s)
        {
            unchecked
            {
                return ROL(a + (b ^ c ^ d) + xk + 0x6ed9eba1, s);
            }
        }
    }
}
