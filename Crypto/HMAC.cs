﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crypto
{
    public class HMAC
    {
        public const int BLOCKSIZE_BYTES = 64;

        /// <summary>
        /// As described in pseudocode on wikipedia. 
        /// </summary>
        /// <param name="key">Secret key. Will be trimmed inside function to be of correct size according to the hash</param>
        /// <param name="data">data on which we generate the hmac</param>
        /// <returns></returns>
        public static byte[] SHA1(byte[] key, byte[] data)
        {
            var _key = key;

            if(_key.Length > BLOCKSIZE_BYTES)
            {
                _key = Crypto.SHA1.Hash(_key);
            }

            if(_key.Length < BLOCKSIZE_BYTES)
            {
                var difference = BLOCKSIZE_BYTES - _key.Length;
                _key = _key.Concat(Enumerable.Repeat((byte)0x00, difference).ToArray()).ToArray();
            }

            var o_pad = Enumerable.Repeat((byte)0x5c, BLOCKSIZE_BYTES).ToArray();
            var o_key_pad = Operations.FixedBitwiseXOR(o_pad, _key);

            var i_pad = Enumerable.Repeat((byte)0x36, BLOCKSIZE_BYTES).ToArray();
            var i_key_pad = Operations.FixedBitwiseXOR(i_pad, _key);

            var first = Crypto.SHA1.Hash(i_key_pad.Concat(data).ToArray());

            return Crypto.SHA1.Hash(o_key_pad.Concat(first).ToArray());
        }

    }
}
