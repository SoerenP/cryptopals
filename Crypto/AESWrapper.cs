﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace Crypto
{
    public static class AESWrapper
    {
        private static int keysize_bits = 128;
        private static int keysize_bytes = 16;
        private static int blocksize_bits = 128;
        private static int blocksize_bytes = 16;

        private static readonly Random getrandom = new Random();
        private static readonly RNGCryptoServiceProvider secure_getrandom = new RNGCryptoServiceProvider();
        private static readonly byte[] PaddingOracleFixedKey = Encoding.ASCII.GetBytes("SUCH MUCH SECRET");
        private static readonly byte[] PaddingOracleFixedNonce = Encoding.ASCII.GetBytes("FIXNONCE");
        
        public static byte[] GetPaddingOracleFixedKey
        {
            get
            {
                return PaddingOracleFixedKey;
            }
        }

        public static byte[] GetPaddingOracleFixedNonce
        {
            get
            {
                return PaddingOracleFixedNonce;
            }
        }

        private static int upperPaddingSize = 50;
        private static int lowerPaddingSize = 10;

        private static byte[] randomPrefixPadding;

        static AESWrapper()
        {
            RandomPrefixPaddingLength = getrandom.Next(lowerPaddingSize, upperPaddingSize);
            randomPrefixPadding = new byte[RandomPrefixPaddingLength];
            secure_getrandom.GetBytes(randomPrefixPadding);
        }



        public static byte[] RandomKey()
        {
            byte[] key = new byte[keysize_bytes];
            secure_getrandom.GetBytes(key);
            return key;
        }

        public static byte[] RandomKey(int size)
        {
            if (size < 1)
                throw new ArgumentException("Size must be positive");

            byte[] key = new byte[size];
            secure_getrandom.GetBytes(key);
            return key;
        }

        public enum BlockMode { ECB, CBC, CTR };
        private static BlockMode oracleMode;
        public static  BlockMode OracleMode
        {
            get
            {
                return oracleMode;
            }
            set
            {
                oracleMode = value;
            }


        }

        private static int randomPrefixPaddingLength;
        public static int RandomPrefixPaddingLength
        {
            get
            {
                return randomPrefixPaddingLength;
            }
            set
            {
                randomPrefixPaddingLength = value;
            }
        }

        #region ORACLE

        public static byte[] EncryptionOracleRandomPrefixPadding(byte[] data)
        {
            string padding = "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg" + "aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq" + "dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg" + "YnkK";
            byte[] raw_padding = Convert.FromBase64String(padding);
            byte[] padded_data = randomPrefixPadding.Concat(data).Concat(raw_padding).ToArray();
            return ECB.Encrypt(padded_data, PaddingOracleFixedKey);
        }


        public static byte[] EncryptionOraclePadding(byte[] data)
        {
            string padding = "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg" + "aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq" + "dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg" + "YnkK";
            
            byte[] raw_padding = Convert.FromBase64String(padding);

            int length = data.Length + raw_padding.Length;
            byte[] padded_data = data.Concat(raw_padding).ToArray();

            byte[] cipher = ECB.Encrypt(padded_data, PaddingOracleFixedKey);

            return cipher;
        }

        public static byte[] EncryptionOracle(byte[] data)
        {
            int bytes_to_pad = getrandom.Next(5, 11);
            int length = data.Length + 2 * bytes_to_pad;

            byte[] padded_data = new byte[length];
            byte[] pad = new byte[bytes_to_pad];
            secure_getrandom.GetBytes(pad);

            for(int i = 0; i < bytes_to_pad; i++)
            {
                padded_data[i] = pad[i];
                padded_data[data.Length + bytes_to_pad + i] = pad[i];
            }

            for(int i = bytes_to_pad; i < data.Length + bytes_to_pad; i++)
            {
                padded_data[i] = data[i - bytes_to_pad];
            }

            int cbc = getrandom.Next(0, 2);

            byte[] cipher = new byte[length];
            byte[] key = RandomKey();

            if (cbc > 0)
            {
                byte[] iv = RandomKey();
                cipher = CBC.Encrypt(padded_data, key, iv);
                AESWrapper.OracleMode = AESWrapper.BlockMode.CBC;
            }
            else
            {
                cipher = ECB.Encrypt(padded_data, key);
                AESWrapper.OracleMode = AESWrapper.BlockMode.ECB;
            }

            return cipher;
        }

        #endregion

        #region CTR

        /// <summary>
        /// For challenge 25
        /// </summary>
        public class DiscEncryption
        {
            private readonly byte[] _key;
            private readonly byte[] _nonce;
            private readonly byte[] _ciphertext;

            private readonly CTR _ctr;

            public DiscEncryption(byte[] plaintext)
            {
                _key = RandomKey();
                _nonce = RandomKey().Take(8).ToArray();

                _ctr = new CTR(_key, _nonce);

                _ciphertext = _ctr.Encrypt(plaintext);
            }

            public byte[] GetCiphertext()
            {
                return _ciphertext;
            }

            /// <summary>
            /// Offset is in bytes. Everything behind the offset should be replaced?
            /// </summary>
            /// <param name="ciphertext"></param>
            /// <param name="offset"></param>
            /// <param name="newtext"></param>
            /// <returns></returns>
            public byte[] Edit(byte[] ciphertext, int offset, byte[] newtext)
            {
                // Decrypt everything
                var decrypted = _ctr.Decrypt(ciphertext);

                //Inject everything after the offset and to the end.
                Array.Copy(newtext, 0, decrypted, offset, newtext.Length);

                //Reencrypt
                return _ctr.Encrypt(decrypted);
            }
                
        }

        public class CTR
        {
            static readonly int nonce_length = 8;
            public static readonly int blocksizeBytes = 16;
            private Int64 _counter;
            private readonly byte[] _nonce;
            private readonly byte[] _key;


            public CTR(byte[] key, byte[] nonce)
            {
                if (key.Length < keysize_bytes) throw new ArgumentException($"Key must be at least {keysize_bytes} bytes long");
                if (nonce.Length != nonce_length) throw new ArgumentException($"Nonce must be {nonce_length} bytes long");

                _key = key;
                _nonce = nonce;
                _counter = 0;
            }

            /// <summary>
            /// Needed for challenge 25
            /// </summary>
            /// <param name="counter"></param>
            public void SetCounter(Int64 counter)
            {
                _counter = counter;
            }

            public byte[] Encrypt(byte[] data)
            {               
                var plaintext = data;
                var ciphertext = new List<byte[]>();

                int data_length = data.Length;
                int rounds = data_length / blocksizeBytes;

                //The nonce concatted with the counter should be 128 bits = 16 bytes, so we need an int64 for the 8 last bytes
                //We do not check for little endian as we should - i just happen to know that MY machine is little endian. 
                //Good luck to anyone else. 
                for(int i = 0; i < rounds; i++)
                {
                    var encrypted = GetRoundKey();
                    var toXorOnto = plaintext.Skip(i * blocksizeBytes).Take(blocksizeBytes).ToArray();

                    var cipher = Operations.RepeatingKeyXOR(encrypted, toXorOnto);

                    ciphertext.Add(cipher.ToArray());
                    IncrementCounter();
                }


                //Take last bytes explicitly (if there are any)
                int missingBytes = plaintext.Length % blocksizeBytes;
                if(missingBytes != 0)
                {                    
                    var lastEncrypted = GetRoundKey();                    
                    var slicedLastEncrypted = lastEncrypted.Take(missingBytes).ToArray();
                    var slicedToXorOnto = plaintext.Skip((rounds) * blocksizeBytes).Take(missingBytes).ToArray();

                    var finalCipher = Operations.RepeatingKeyXOR(slicedLastEncrypted, slicedToXorOnto);
                    ciphertext.Add(finalCipher.ToArray());
                }
                

                //take each block from each round, copy together and return.
                var result = new byte[data.Length];

                int lenghtSoFar = 0;
                foreach(var bytes in ciphertext)
                {                    
                    bytes.CopyTo(result, lenghtSoFar);
                    lenghtSoFar += bytes.Length;
                }

                /* reset counter */
                _counter = 0;

                return result;
            }

            public byte[] Decrypt(byte[] data)
            {
                return Encrypt(data);
            }

            private byte[] GetRoundKey()
            {
                var counterbytes = BitConverter.GetBytes(_counter);

                if (!BitConverter.IsLittleEndian)
                {
                    counterbytes.Reverse();
                }

                return ECB.Encrypt(_nonce.Concat(counterbytes).ToArray(), _key);
            }

            private void IncrementCounter()
            {
                _counter++;
            }

            public static byte[] ParseThenPadd(string data)
            {
                string stripped = CBC.UrlParseAndPad(data);

                /* pad then encrypt under fixed random key */
                int blocksize = 16;
                stripped = Operations.PKCSHashtag7Padding(stripped, blocksize);
                byte[] stripped_raw = Encoding.ASCII.GetBytes(stripped);

                var ctr = new AESWrapper.CTR(PaddingOracleFixedKey, PaddingOracleFixedNonce);

                return ctr.Encrypt(stripped_raw);
            }

            public static bool LookForAdmin(byte[] data)
            {
                var ctr = new CTR(PaddingOracleFixedKey, PaddingOracleFixedNonce);
                byte[] decrypted = ctr.Decrypt(data);
                string ascii = Encoding.ASCII.GetString(decrypted);
                string looking_for = ";admin=true;";
                return ascii.Contains(looking_for);
            }
        }

        #endregion

        #region CBC

        public static class CBC
        {
            private static string[] cookies = new string[]{"MDAwMDAwTm93IHRoYXQgdGhlIHBhcnR5IGlzIGp1bXBpbmc=", "MDAwMDAxV2l0aCB0aGUgYmFzcyBraWNrZWQgaW4gYW5kIHRoZSBWZWdhJ3MgYXJlIHB1bXBpbic=",
            "MDAwMDAyUXVpY2sgdG8gdGhlIHBvaW50LCB0byB0aGUgcG9pbnQsIG5vIGZha2luZw==","MDAwMDAzQ29va2luZyBNQydzIGxpa2UgYSBwb3VuZCBvZiBiYWNvbg==","MDAwMDA0QnVybmluZyAnZW0sIGlmIHlvdSBhaW4ndCBxdWljayBhbmQgbmltYmxl",
            "MDAwMDA1SSBnbyBjcmF6eSB3aGVuIEkgaGVhciBhIGN5bWJhbA==","MDAwMDA2QW5kIGEgaGlnaCBoYXQgd2l0aCBhIHNvdXBlZCB1cCB0ZW1wbw==","MDAwMDA3SSdtIG9uIGEgcm9sbCwgaXQncyB0aW1lIHRvIGdvIHNvbG8=",
            "MDAwMDA4b2xsaW4nIGluIG15IGZpdmUgcG9pbnQgb2g=","MDAwMDA5aXRoIG15IHJhZy10b3AgZG93biBzbyBteSBoYWlyIGNhbiBibG93"};

            private static readonly byte[] FixedKey = Encoding.ASCII.GetBytes("YELLOW SUBMARINE");
            private static readonly Random getrandom = new Random();
            private static readonly RNGCryptoServiceProvider secure_getrandom = new RNGCryptoServiceProvider(); //TODO: always the same during runtime, not good for actual crypto.. 


            public static byte[] Encrypt(byte[] data, byte[] key)
            {
                return CBC.Encrypt(data, key, Enumerable.Repeat((byte)0, AESWrapper.keysize_bytes).ToArray());
            }

            public static byte[] Encrypt(byte[] data, byte[] key, byte[] iv)
            {
                if (key.Length < keysize_bytes || iv.Length < keysize_bytes) throw new ArgumentException("Keysize or IV too small, must be 16 bytes");
                if (key.Length != keysize_bytes) throw new ArgumentException("Keysize must be 16 bytes");

                int data_length = data.Length;
                int rounds = (int)Math.Ceiling((float)data_length / (float)blocksize_bytes);
                int padding_needed = data_length % blocksize_bytes;

                /* do padding if needed */
                byte[] plaintext = data;
                if(padding_needed != 0)
                {
                    plaintext = Operations.PKCSHashtag7Padding(plaintext, blocksize_bytes);
                }

                /* handle first block with IV explicitly */
                byte[] first_block = new byte[blocksize_bytes];
                for (int j = 0; j < blocksize_bytes; j++)
                {
                    first_block[j] = plaintext[j];
                }

                first_block = Operations.RepeatingKeyXOR(first_block, iv);
                byte[] cipher_block = ECB.Encrypt(first_block, key);
                byte[] ciphertext = new byte[plaintext.Length];
                for (int j = 0; j < blocksize_bytes; j++)
                {
                    ciphertext[j] = cipher_block[j];
                }

                /* take previous cipher block, next plaintext block and XOR together. */
                /* Take result and feed to AES with the key. Write the resulting block to ciphertext, and keep going */
                for (int i = 1; i < rounds; i++)
                {
                    byte[] temp = new byte[blocksize_bytes];

                    for (int j = 0; j < blocksize_bytes; j++)
                    {
                        temp[j] = plaintext[i * blocksize_bytes + j];
                    }

                    temp = Operations.RepeatingKeyXOR(temp, cipher_block);
                    cipher_block = ECB.Encrypt(temp, key);

                    for(int j = 0; j < blocksize_bytes; j++)
                    {
                        ciphertext[i * blocksize_bytes + j] = cipher_block[j];
                    }

                }

                return ciphertext;
            
            }

            public static byte[] Decrypt(byte[] data, byte[] key)
            {
                return CBC.Decrypt(data, key, Enumerable.Repeat((byte)0, AESWrapper.keysize_bytes).ToArray());
            }

            public static byte[] Decrypt(byte[] data, byte[] key, byte[] iv)
            {
                int data_length = data.Length;
                int rounds = (int)Math.Ceiling((float)data_length / (float)blocksize_bytes);

                byte[] plaintext = new byte[data.Length];
                byte[] ciphertext = data;

                /* handle first block explicitly with IV */
                byte[] temp = new byte[blocksize_bytes];
                byte[] cipher_block = new byte[blocksize_bytes];
                for(int j = 0; j < blocksize_bytes; j++)
                {
                    cipher_block[j] = ciphertext[j];
                }

                temp = ECB.Decrypt(cipher_block, key);
                temp = Operations.RepeatingKeyXOR(temp, iv);

                for(int j = 0; j < blocksize_bytes; j++)
                {
                    plaintext[j] = temp[j];
                }

                /* now iterate (could do this in parallel actually) */
                byte[] old_cipher_block = new byte[blocksize_bytes];
                for(int i = 1; i < rounds; i++)
                {
                    for(int j = 0; j < blocksize_bytes; j++)
                    {
                        old_cipher_block[j] = cipher_block[j];
                        cipher_block[j] = ciphertext[i * blocksize_bytes + j];
                    }

                    temp = ECB.Decrypt(cipher_block, key);
                    temp = Operations.RepeatingKeyXOR(temp, old_cipher_block);

                    for(int j = 0; j < blocksize_bytes; j++)
                    {
                        plaintext[i * blocksize_bytes + j] = temp[j];
                    }
                }

                return plaintext;
            }

            #endregion

            #region cookie

            public struct Cookie
            {
                public byte[] ciphertext;
                public byte[] iv;

                public Cookie(byte[] ciphertext, byte[] iv)
                {
                    this.ciphertext = new byte[ciphertext.Length];
                    Array.Copy(ciphertext, this.ciphertext, this.ciphertext.Length);

                    this.iv = new byte[iv.Length];
                    Array.Copy(iv, this.iv, this.iv.Length);
                }

                /// <summary>
                /// Makes a deep copy of the incomming cookie. 
                /// </summary>
                /// <param name="toCopy"></param>
                public Cookie(Cookie toCopy)
                {
                    this.ciphertext = new byte[toCopy.ciphertext.Length];
                    Array.Copy(toCopy.ciphertext, this.ciphertext, this.ciphertext.Length);

                    this.iv = new byte[toCopy.iv.Length];
                    Array.Copy(toCopy.iv, this.iv, this.iv.Length);
                }
            }

            public static Cookie CookieCBC()
            {
                int blocksize = 16;
                int cookie_index = getrandom.Next(0, cookies.Length);
                var cookie = Convert.FromBase64String(cookies[cookie_index]);
                Console.WriteLine("cookie {0}", Convert.ToBase64String(cookie));
                byte[] plaintext = Operations.PKCSHashtag7Padding(cookie, blocksize);
                byte[] IV = AESWrapper.RandomKey();
                byte[] ciphertext = CBC.Encrypt(plaintext, FixedKey, IV);

                return new Cookie(ciphertext, IV);
            }

            public static bool CookieValidPadding(Cookie c)
            {
                int blocksize = 16;
                byte[] plaintext = Decrypt(c.ciphertext,FixedKey,c.iv);

                bool valid = true;
                byte[] stripped;
                try
                {
                    stripped = Operations.PKCSHashtag7Validation(plaintext, blocksize);
                }
                catch (ArgumentException)
                {
                    valid = false;
                }
                //if (valid) { Console.WriteLine("valid plaintext: {0}", Encoding.ASCII.GetString(plaintext)); }
                return valid;
            }

            #endregion

            /* maybe this should be in profilefactory instead */
            public static byte[] ParseThenPadd(string data)
            {
                string stripped = UrlParseAndPad(data);

                /* pad then encrypt under fixed random key */
                int blocksize = 16;
                stripped = Operations.PKCSHashtag7Padding(stripped, blocksize);
                byte[] stripped_raw = Encoding.ASCII.GetBytes(stripped);

                return CBC.Encrypt(stripped_raw, AESWrapper.PaddingOracleFixedKey);
            }

            public static byte[] ParseThenPaddUseKeyAsIv(string data)
            {
                string stripped = UrlParseAndPad(data);

                /* pad then encrypt under fixed random key. Be dump and use the key as iv as well */
                int blocksize = 16;
                stripped = Operations.PKCSHashtag7Padding(stripped, blocksize);
                byte[] stripped_raw = Encoding.ASCII.GetBytes(stripped);

                return CBC.Encrypt(stripped_raw, AESWrapper.PaddingOracleFixedKey, AESWrapper.PaddingOracleFixedKey);
            }

            public static bool LookForAdmin(byte[] data)
            {
                byte[] decrypted = CBC.Decrypt(data, AESWrapper.PaddingOracleFixedKey);
                string ascii = Encoding.ASCII.GetString(decrypted);
                string looking_for = ";admin=true;";
                return ascii.Contains(looking_for);
            }

            public static bool LookForAdminCheckAscii(byte[] data)
            {

                byte[] decrypted = CBC.Decrypt(data, AESWrapper.PaddingOracleFixedKey, AESWrapper.PaddingOracleFixedKey);
                string ascii = Encoding.ASCII.GetString(decrypted);

                /* getstring takes any non ascii and makes it '?'. This means when we go back, those bytes are 63. So if the two byte */
                /* arrays are not equal, it means there was garbage ascii in there somewhere. */
                var backAgain = Encoding.ASCII.GetBytes(ascii);

                if (decrypted != backAgain)
                {
                    /* This would be dump in the real world. Which is the point :) */
                    throw new LeakingException("Data contained non-ascii", decrypted);
                }

                string looking_for = ";admin=true;";
                return ascii.Contains(looking_for);
            }

            public static string UrlParseAndPad(string data)
            {
                /* parse input and quote out ';' and ''' */
                string[] split = data.Split(';').ToArray();
                string stripped = string.Empty;

                for (int i = 0; i < split.Length; i++)
                {
                    stripped = stripped + split[i];
                    if (i != split.Length - 1) stripped = stripped + "';'";
                }

                split = stripped.Split('=').ToArray();
                stripped = string.Empty;

                for (int i = 0; i < split.Length; i++)
                {
                    stripped = stripped + split[i];
                    if (i != split.Length - 1) stripped = stripped + "'='";
                }

                /* append and prepend the fixed strings */
                string prefix = "comment1=cooking%20MCs;userdata=";
                string postfix = ";comment2=%20like%20a%20pound%20of%20bacon";
                stripped = prefix + stripped + postfix;

                return stripped;
            }
        }

        #region ECB

        /* Inspired by https://msdn.microsoft.com/en-us/library/system.security.cryptography.aesmanaged(v=vs.110).aspx */
        /* ECB doesn't actually use the IV but I think we have to send it regardless due to the interface implemented by AesManaged & ICryptoTransform */
        public static class ECB
        {
            public static byte[] Encrypt(byte[] data, byte[] key)
            {
                return Encrypt(data, key, Enumerable.Repeat((byte)0, AESWrapper.keysize_bytes).ToArray());
            }

            public static byte[] Encrypt(byte[] data, byte[] key, byte[] iv)
            {
                var aesAlg = new AesManaged
                {
                    KeySize = AESWrapper.keysize_bits,
                    Key = key,
                    BlockSize = AESWrapper.blocksize_bits,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.Zeros,
                    IV = iv
                };

                var encrypted = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV)
                    .TransformFinalBlock(data, 0, data.Length);
                return encrypted;

            }

            public static byte[] Decrypt(byte[] data, byte[] key)
            {
                return Decrypt(data, key, Enumerable.Repeat((byte)0, AESWrapper.keysize_bytes).ToArray());
            }

            /* look at http://stackoverflow.com/questions/2116607/rijndaelmanaged-padding-is-invalid-and-cannot-be-removed-that-only-occurs-when */
            public static byte[] Decrypt(byte[] data, byte[] key, byte[] iv)
            {
                var aesAlg = new AesManaged
                {
                    KeySize = AESWrapper.keysize_bits,
                    Key = key,
                    BlockSize = AESWrapper.blocksize_bits,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.Zeros,
                    IV = iv
                };

                var decrypted = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV)
                    .TransformFinalBlock(data, 0, data.Length);
                return decrypted;
            }
        }
    }

    #endregion
}
