﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Numerics;
using Crypto;
using System.Text;
using Crypto.SRP;
using System.Collections.Generic;
using System;
using System.Security.Cryptography;
using Crypto.Numerical;

namespace ChallengeTests
{
    [TestClass]
    public class TestSet5
    {   //1536 bit
        // OBS: you must prepend a 0 to the hex representation, lest the integer becomes negative
        // The reason is in the answer to https://stackoverflow.com/questions/30119174/converting-a-hex-string-to-its-biginteger-equivalent-negates-the-value

        public static string BiggerP = "0FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD1" +
                            "29024E088A67CC74020BBEA63B139B22514A08798E3404DD" +
                            "EF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245" +
                            "E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7ED" +
                            "EE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3D" +
                            "C2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F" +
                            "83655D23DCA3AD961C62F356208552BB9ED529077096966D" +
                            "670C354E4ABC9804F1746C08CA237327FFFFFFFFFFFFFFFF";

        public static BigInteger NistPrime = BigInteger.Parse(BiggerP, System.Globalization.NumberStyles.HexNumber);

        private TestContext testContextInstance;
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestCategory("Set5"), TestMethod]
        public void TestModPow()
        {
            var g = new BigInteger(2);
            var p = NistPrime;
            var random = DiffieHellman.RandomIntegerBelow(p);

            // 4 sec
            var result1 = BigInteger.ModPow(g, random, p);

            // 93 ms
            var result2 = DiffieHellman.ModExp(g, random, p);

            // 47 ms
            var result3 = DiffieHellman.mpir_ModExp(g, random, p);

        }

        [TestCategory("Set5"), TestMethod]
        public void TestChallenge33()
        {
            var p = new BigInteger(37);
            var g = new BigInteger(5);
            var dh = new DiffieHellman(p, g, BigInteger.ModPow);

            Assert.IsTrue(dh.IsValid);

            var bignum = NistPrime;

            var biggerDh = new DiffieHellman(bignum, 2, DiffieHellman.ModExp);

            Assert.IsTrue(biggerDh.IsValid);
        }

        [TestCategory("Set5"), TestMethod]
        public void Challenge34NormalProtocol()
        {
            var p = new BigInteger(37);
            var g = new BigInteger(5);
            var dh = new DiffieHellman(p, g, BigInteger.ModPow);

            var Alice = new DiffieHellman.Alice(p, g);
            var Bob = new DiffieHellman.Bob(p, g);

            // A -> B "send A"
            var A = Alice.A;
            Bob.GenerateSecret(A);

            // B -> A "send B"
            var B = Bob.B;
            Alice.GenerateSecret(B);

            // Both have secret, now we can send messages, fx A -- message ---> B
            var message = "YELLOW SUBMARINE";
            var encrypted = Alice.Encrypt(message);
            var response = Bob.Echo(encrypted);

            var responseAscii = Alice.Decrypt(response);

            Assert.IsTrue(responseAscii == message);
        }

        [TestCategory("Set5"), TestMethod]
        public void Challenge34_MITM()
        {
            var p = new BigInteger(37);
            var g = new BigInteger(5);
            var dh = new DiffieHellman(p, g, BigInteger.ModPow);

            // get A, but give bob P
            var Alice = dh.alice;
            var Bob = dh.bob;

            var A = dh.alice.A;
            Bob.GenerateSecret(p);

            // get B, but give alice p
            var B = Bob.B;
            Alice.GenerateSecret(p);

            //get message from alice
            var message = "YELLOW SUBMARINE";
            var encrypted = Alice.Encrypt(message);

            var response = Bob.Echo(encrypted);

            // Decrypt only from our pov. We know only p, g, B, A... hm
            // But we know Alice generated S = p^a mod p = 0 and Bob generated S = p^b mod p = 0, since we MITMd that.
            // So the key used is derived from the number 0. just do the same.
            var key = Crypto.SHA1.Hash(new BigInteger(0).ToByteArray()).Take(16).ToArray();

            var decrypted = Encoding.ASCII.GetString(AESWrapper.CBC.Decrypt(response.Data, key, response.Iv));

            Assert.IsTrue(decrypted == message);
        }

        [TestCategory("Set5"), TestMethod]
        public void Challenge35_MITM_g_equals_1()
        {
            var p = new BigInteger(37);
            var g = new BigInteger(5);
            var dh = new DiffieHellman(p, g, BigInteger.ModPow);

            // g = 1
            var Alice = dh.alice;
            var Bob = new DiffieHellman.Bob(p, new BigInteger(1));

            var A = Alice.A;
            Bob.GenerateSecret(A);

            var B = Bob.B;
            Alice.GenerateSecret(B);

            //get message from alice
            var message = "YELLOW SUBMARINE";
            var encrypted = Alice.Encrypt(message);

            // A = g^a mod p is as it should be
            // B = g^b mod p = 1, since g = 1 for Bob only.
            // Means that S_A = g^ba = 1^ba mod p = 1.
            var key = Crypto.SHA1.Hash(new BigInteger(1).ToByteArray()).Take(16).ToArray();

            var decrypted = Encoding.ASCII.GetString(AESWrapper.CBC.Decrypt(encrypted.Data, key, encrypted.Iv));

            Assert.IsTrue(decrypted == message);
        }

        [TestCategory("Set5"), TestMethod]
        public void Challenge35_MITM_g_equals_p()
        {
            var p = new BigInteger(37);
            var g = new BigInteger(5);
            var dh = new DiffieHellman(p, g, BigInteger.ModPow);

            // g = p
            var Alice = dh.alice;
            var Bob = new DiffieHellman.Bob(p, p);

            var A = Alice.A;
            Bob.GenerateSecret(A);

            var B = Bob.B;
            Alice.GenerateSecret(B);

            //get message from alice
            var message = "YELLOW SUBMARINE";
            var encrypted = Alice.Encrypt(message);

            // A = g^a mod p is as it should be
            // B = p^b mod p = p, since g = p for Bob only.
            // Means that S_A = p^ba mod p = 0.
            var key = Crypto.SHA1.Hash(new BigInteger(0).ToByteArray()).Take(16).ToArray();

            var decrypted = Encoding.ASCII.GetString(AESWrapper.CBC.Decrypt(encrypted.Data, key, encrypted.Iv));

            Assert.IsTrue(decrypted == message);
        }

        [TestCategory("Set5"), TestMethod]
        public void Challenge35_MITM_g_equals_p_minus_1()
        {
            var p = new BigInteger(37);
            var g = new BigInteger(5);
            var dh = new DiffieHellman(p, g, BigInteger.ModPow);

            // g = p - 1
            var Alice = dh.alice;
            var malicoius_g = p - new BigInteger(1);
            var Bob = new DiffieHellman.Bob(p, malicoius_g);

            var A = Alice.A;
            Bob.GenerateSecret(A);

            var B = Bob.B;
            Alice.GenerateSecret(B);

            //get message from alice
            var message = "YELLOW SUBMARINE";
            var encrypted = Alice.Encrypt(message);

            // A = g^a mod p is as it should be, however due to MITM we have
            // B = (p-1)^b mod p
            // Since P is a large prime (and in our case it happens to also be the case),
            // P-1 is (most likely) even. P-1 is thus divisible by 2. 
            // Each subgroup of Z*_P (mod p) is of the form 1, h, h^2,...h^(q-1) for some h where q is the order of h
            // q is a divisior of p-1, e.g. the size of any subgroup is a diviosr of p-1. 
            // THE CONVERSE also holds, for any divisor d of p-1, there is a single subgroup of size d.
            //
            // Now here is the crazy thing - in our case p-1 is even, so it is divisble by d = 2. 
            // Since d = 2 is a divisor of p-1 there exists a single subgroup of size d = 2! So only 2 possible values 
            // can be generated in this subgroup, and subsequently be used to derive the key. 
            // This subgroup of only d = 2 elements has exactly the elements 1 and p-1, and is ALWAYS present as a subgroup
            // in any Z*_P for any prime P, even IF you use safe primes. Thats why you should always check against p-1 explicitly in DH key exchange.
            //
            // See "cryptography and engineering design principles and practical applications" section 11.4 for the full details

            // We know key is either based on 1 or p-1 
            var key_1 = Crypto.SHA1.Hash(new BigInteger(1).ToByteArray()).Take(16).ToArray();
            var key_2 = Crypto.SHA1.Hash(malicoius_g.ToByteArray()).Take(16).ToArray();

            var decrypted_1 = Encoding.ASCII.GetString(AESWrapper.CBC.Decrypt(encrypted.Data, key_1, encrypted.Iv));
            var decrypted_2 = Encoding.ASCII.GetString(AESWrapper.CBC.Decrypt(encrypted.Data, key_2, encrypted.Iv));

            Assert.IsTrue(decrypted_1 == message || decrypted_2 == message);
        }

        [TestCategory("Set5"), TestMethod]
        public void Challenge36_SecureRemotePassword()
        {
            var email = "Johndoe@gmail.com";

            var srpParameters = new SrpParameters()
            {
                g = new BigInteger(2),
                k = new BigInteger(3),
                N = NistPrime,
                Email = email,
                Password = "This shouldnt be plaintext lol"
            };

            var client = new SrpClient(srpParameters);
            var server = new SrpServer(srpParameters);

            // C -> S, send I & A
            server.ProtocolStep1_SendIandA(email, client.A);

            //S -> C, send salt & B
            client.ProtocolStep2_SendSaltAndB(server.salt, server.B);

            var mac = client.ComputeMac();
            var valid = server.ValidateMac(mac);

            /*
             *  The math of SRP is the following: 
             *  S_client = (B - k*g^x)^(a+ux) mod N
             *  = (k*g^x + g^b - k*g^x)^(a+ux) mod N
             *  = (g^b)^(a+ux) 
             *  = g^(ba + xub) mod N
             * 
             * S_server = (A * v^u)^b mod N = (g^a * v^u)^b mod N
             * = (g^a * g^(xu))^b = g^ab * g^xub 
             * = g^(ab + xub) MOD N
             * 
             * The hash input for the key is thus the same. Neat
             */

            Assert.IsTrue(valid);
        }

        [TestCategory("Set5"), TestMethod]
        public void Challenge37_SecureRemotePassword_AfixedtoZero()
        {
            /*
             * If the client sends A = 0, then the server will compute its key 
             * from S = ( 0 * v^u)^b mod N which is always zero. The key is then always SHA256(new BigInteger(0));
             * 
             * 
             * If the client sends a multiple of N, e.g. just N as A = 0,
             * then server side S = ( N * v^u)^b mod N which is also always zero, for any multiple of N. 
             * 
             * I guess to prevent this one would have to check that A is > 0 (is this enough for hte low end?) and < N. 
             * OR always hash it, to remove the algrebraic structure straight up, THEN convert it to a bigint
             * straight after and continue with the math. Then it should never hit this case (try this actually!) 
             * 
             */
            var email = "Johndoe@gmail.com";

            var srpParameters = new SrpParameters()
            {
                g = new BigInteger(2),
                k = new BigInteger(3),
                N = NistPrime,
                Email = email,
                Password = "This shouldnt be plaintext lol"
            };

            var evilClient = new SrpClient(srpParameters);
            var server = new SrpServer(srpParameters);

            // C -> S, send I & A, but set A = 0
            server.ProtocolStep1_SendIandA(email, new BigInteger(0));

            // Skip step 2, just use server salt below instead of sending it. The point is that the client gets it, its not secret at all.
            // The point of the attack is that the client doesnt need to know the password at all. 

            //evil client doesnt know the password. But he knows the key is based on 0 biginteger! so he can jsut ocmpute the key without knowing ANY of the secret integers or anything.
            // because the math dictates that if A = 0, then the entire computation of S for the server will be 0, no matter what the other parameters are. 
            var fixedKey = SrpProtocol.BigIntToByteArray(new BigInteger(0));

            var forgedMac = SrpProtocol.ComputeMac(fixedKey, server.salt);
            var valid = server.ValidateMac(forgedMac);

            Assert.IsTrue(valid);
        }

        [TestCategory("Set5"), TestMethod]
        public void Challenge37_SecureRemotePassword_AfixedtoMultipleOfN()
        {
            /* Same as above, just another value that is 0 mod N (e.g. N itself) */
            var email = "Johndoe@gmail.com";

            var srpParameters = new SrpParameters()
            {
                g = new BigInteger(2),
                k = new BigInteger(3),
                N = NistPrime,
                Email = email,
                Password = "This shouldnt be plaintext lol"
            };

            var evilClient = new SrpClient(srpParameters);
            var server = new SrpServer(srpParameters);

            // C -> S, send I & A, but set A = N, key will be based on 0 int again. Works for any multiple of N.
            server.ProtocolStep1_SendIandA(email, srpParameters.N);
            var fixedKey = SrpProtocol.BigIntToByteArray(new BigInteger(0));

            var forgedMac = SrpProtocol.ComputeMac(fixedKey, server.salt);
            var valid = server.ValidateMac(forgedMac);

            Assert.IsTrue(valid);
        }

        [TestCategory("Set5"), TestMethod]
        public void Challenge38_SimplifiedSrp_NormalRun()
        {
            var srpParameters = new SrpParameters()
            {
                g = new BigInteger(2),
                k = new BigInteger(3),
                N = NistPrime,
                Email = "bob",
                Password = "asdfsdfsdf"
            };

            var client = new SimplifiedSrpClient(srpParameters);
            var server = new SimplifiedSrpServer(srpParameters);

            var A = client.ComputeA();
            var B = server.ComputeB();
            var salt = server.salt;
            var u = server.u;

            server.ComputeSecret(A);
            client.ComputeSecret(B, salt, u);

            var mac = client.ComputeMac(salt);
            var loggedIn = server.ValidateMac(mac);

            Assert.IsTrue(loggedIn);
        }

        [TestCategory("Set5"), TestMethod]
        public void Challenge38_SimplifiedSrp_OfflineDictionary()
        {
            List<string> passwords = new List<string>()
            {
                "passw0rd",
                "qwerty123123",
                "p455w0rd",
                "password1",
                "password1234",
                "julie1",
                "lisa1234",
                "secretPassword",
                "thenameofthecompany",
                "mothersmaidenname",
                "shortandsweet",
                "penis",
                "bigboob13s",
                "s3cr37",
                "cant remember"
            };

            var random = new Random();
            var randomPasswordUsed = passwords.ElementAt(random.Next() % passwords.Count);

            TestContext.WriteLine("Password chosen:" + randomPasswordUsed);

            var srpParameters = new SrpParameters()
            {
                g = new BigInteger(2),
                k = new BigInteger(3),
                N = NistPrime,
                Email = "bob",
                Password = randomPasswordUsed
            };

            var client = new SimplifiedSrpClient(srpParameters);

            // Choose some stuff as MITM to send to client.
            var badb = DiffieHellman.RandomIntegerBelow(srpParameters.N);
            var badB = SrpProtocol.ModExp(srpParameters.g, badb, srpParameters.N);
            var salt = DiffieHellman.RandomIntegerBelow(srpParameters.N);
            var u = SrpProtocol.FromStringToBigInt(AESWrapper.RandomKey());

            var A = client.ComputeA();
            client.ComputeSecret(badB, salt, u);

            var mac = client.ComputeMac(salt);

            /* We have as the "server", that S = (A * v^u)^b. the only unknown is the x in v = g^x. So assuming client uses easy to guess password */
            /* we can just try x, compute v, then compute everything else, since we are impersonating server and have the b and u we forwarded */
            /* would this work on normal SRP? dont see why not */
            foreach(var password in passwords)
            {
                using (var sha256 = new SHA256CryptoServiceProvider())
                {
                    var x = SrpProtocol.FromStringToBigInt(Encoding.ASCII.GetBytes(password), salt);
                    var v = SrpProtocol.ModExp(srpParameters.g, x, srpParameters.N);
                    var s = SrpProtocol.ModExp(A * SrpProtocol.ModExp(v, u, srpParameters.N), badb, srpParameters.N);

                    var key = sha256.ComputeHash(SrpProtocol.BigIntToByteArray(s));

                    var computedMac = SrpProtocol.ComputeMac(key, salt);

                    if (computedMac.SequenceEqual(mac))
                    {
                        TestContext.WriteLine("Password used:" + password);
                        Assert.IsTrue(password.SequenceEqual(randomPasswordUsed));
                    }

                }
            }

        }

        [TestCategory("Set5"), TestMethod]
        public void ModInverseTest()
        {
            //  invmod(17, 3120) is 2753
            BigInteger a = 17;
            BigInteger N = 3120;

            BigInteger result = 2753;

            BigInteger calculated = BigIntegerUtility.ModInverse(a, N);

            Assert.AreEqual(result, calculated);
        }

        [TestCategory("Set5"), TestMethod]
        public void ModInverseSimpleTest()
        {
            BigInteger a = 17;
            BigInteger N = 3120;

            BigInteger result = 2753;

            BigInteger calculated = Crypto.RSA.ModInverseSimple(a, N);

            Assert.AreEqual(result, calculated);
        }

        // you are not guarnateed that these usual hardcoded values are valid: https://www.reddit.com/r/crypto/comments/6363di/how_do_computers_choose_the_rsa_value_for_e/
        // and https://crypto.stackexchange.com/questions/12255/in-rsa-why-is-it-important-to-choose-e-so-that-it-is-coprime-to-%CF%86n
        [TestCategory("Set5"), TestMethod]
        public void Challenge39_RSA_SmallPrimes()
        {
            BigInteger p = 2357;
            BigInteger q = 2551;

            var keyGenResult = Crypto.RSA.KeyGen(p, q, 3674911);

            BigInteger message = 5234673;

            var encrypted = Crypto.RSA.Encrypt(keyGenResult.PublicKey, message);
            var decrypted = Crypto.RSA.Decrypt(keyGenResult.PrivateKey, encrypted);

            Assert.AreEqual(message, decrypted);
        }

        [TestCategory("Set5"), TestMethod]
        public void Challenge39_RSA_MediumPrimes_256bit()
        {
            BigInteger e = 3;
            int keySizeInBits = 256;

            var keyGenResult = Crypto.RSA.ProbabilisticKeyGen(e, keySizeInBits);

            BigInteger message = 5234673;

            var encrypted = Crypto.RSA.Encrypt(keyGenResult.PublicKey, message);
            var decrypted = Crypto.RSA.Decrypt(keyGenResult.PrivateKey, encrypted);

            Assert.AreEqual(message, decrypted);
        }

        [TestCategory("Set 5"), TestMethod]
        public void Challenge39_RSA_EncryptStringTheCheesyWay()
        {
            BigInteger e = 3;
            int keySizeInBits = 256;

            var keyGenResult = Crypto.RSA.ProbabilisticKeyGen(e, keySizeInBits);

            // Small string, must be below keysize when taken as bytes. 
            var message = "hi mom";
            var messageInteger = Crypto.RSA.CheesyStringToBigInt(message);

            var encrypted = Crypto.RSA.Encrypt(keyGenResult.PublicKey, messageInteger);
            var decrypted = Crypto.RSA.Decrypt(keyGenResult.PrivateKey, encrypted);

            var decryptedMessage = Crypto.RSA.CheesyBigIntToString(decrypted);

            Assert.AreEqual(message, decryptedMessage);
        }

        [TestCategory("Utility"), TestMethod]
        public void GaussAlgorithm_SmallExample()
        {
            var ais = new List<BigInteger>()
            {
                3, 7
            };

            var moduli = new List<BigInteger>()
            {
                7, 13
            };

            var expectedX = new BigInteger(59);

            var result = NumberTheory.GaussAlgorithm(ais, moduli);

            Assert.AreEqual(expectedX, result);
        }

        [TestCategory("Utility"), TestMethod]
        public void FloorRootTest()
        {
            var A = 27;

            var expectedResult = 3;
            var result = BigIntegerUtility.FloorRoot(A, 3);

            Assert.AreEqual(expectedResult, result);
        }

        [TestCategory("Set 5"), TestMethod]
        public void Challenge40()
        {
            BigInteger e = 3;
            int keySizeInBits = 256;

            // Small string, must be below keysize when taken as bytes. 
            var message = "hi mom";
            var messageInteger = Crypto.RSA.CheesyStringToBigInt(message);

            var key0 = Crypto.RSA.ProbabilisticKeyGen(e, keySizeInBits);
            var key1 = Crypto.RSA.ProbabilisticKeyGen(e, keySizeInBits);
            var key2 = Crypto.RSA.ProbabilisticKeyGen(e, keySizeInBits);

            var c0 = Crypto.RSA.Encrypt(key0.PublicKey, messageInteger);
            var c1 = Crypto.RSA.Encrypt(key1.PublicKey, messageInteger);
            var c2 = Crypto.RSA.Encrypt(key2.PublicKey, messageInteger);

            /*
             * result =
                  (c_0 * m_s_0 * invmod(m_s_0, n_0)) +
                  (c_1 * m_s_1 * invmod(m_s_1, n_1)) +
                  (c_2 * m_s_2 * invmod(m_s_2, n_2)) mod N_012
             * 
             * where 
             *  c_0, c_1, c_2 are the three respective residues mod
                 n_0, n_1, n_2

                 m_s_n (for n in 0, 1, 2) are the product of the moduli
                 EXCEPT n_n --- ie, m_s_1 is n_0 * n_2

                 N_012 is the product of all three moduli
             * 
             */

            var ms0 = key1.PublicKey.N * key2.PublicKey.N;
            var ms1 = key0.PublicKey.N * key2.PublicKey.N;
            var ms2 = key0.PublicKey.N * key1.PublicKey.N;

            /* what they outline above in the challenge is just gauss algortihm spelled out, nicer to use the actual algortihm imo */

            var gaussResult = NumberTheory.GaussAlgorithm(new List<BigInteger>() { c0, c1, c2 },
                                                           new List<BigInteger>() { key0.PublicKey.N, key1.PublicKey.N, key2.PublicKey.N });

            var thirdRootWithMod = BigIntegerUtility.FloorRoot(gaussResult, 3);

            var decrypt = Crypto.RSA.CheesyBigIntToString(thirdRootWithMod);

            Assert.AreEqual(messageInteger, thirdRootWithMod);
            Assert.IsTrue(message.SequenceEqual(decrypt));

        }
    }
}
