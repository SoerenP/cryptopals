﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Crypto;
using System.Linq;
using System.Threading;

namespace ChallengeTests
{
    /// <summary>
    /// Summary description for TestSet3
    /// </summary>
    [TestClass]
    public class TestSet3
    {
        public TestSet3()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        private static string file_prefix = "C:\\Users\\Søren\\Documents\\cryptopals\\ChallengeTests\\files\\";

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestCategory("Set3"), TestMethod]
        public void TestChallenge17()
        {
            bool testpassed = true;

            AESWrapper.CBC.Cookie c = AESWrapper.CBC.CookieCBC();

            bool valid_padding = AESWrapper.CBC.CookieValidPadding(c);
            int blocksize = 16;

            var result = Analysis.CBCPaddingOracleAttack(c, AESWrapper.CBC.CookieValidPadding, blocksize);
            var plaintext = System.Text.Encoding.UTF8.GetString(result);

            TestContext.WriteLine(plaintext);

            Assert.IsTrue(testpassed);
        }

        [TestCategory("Set3"), TestMethod]
        public void TestChallenge18()
        {
            //base64
            var challenge = Convert.FromBase64String("L77na/nrFsKvynd6HzOoG7GHTLXsTVu9qvY/2syLXzhPweyyMTJULu/6/kXX0KSvoOLSFQ==");
            var key = Encoding.ASCII.GetBytes("YELLOW SUBMARINE");

            //Prøv fra en string istf? 
            var nonce = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

            var ctr = new AESWrapper.CTR(key, nonce);

            var result = ctr.Decrypt(challenge);

            TestContext.WriteLine(BitConverter.IsLittleEndian.ToString());

            TestContext.WriteLine(Encoding.ASCII.GetString(result));

            var cryptor = new AESWrapper.CTR(key, nonce);

            var plaintext = Encoding.ASCII.GetBytes("YELLOW SUBMARINE CYKA BLYAT  CYKA BLYAT CYKA BLYAT CYKA BLYAT");
            var cipher = cryptor.Encrypt(plaintext);

            var decrypted = cryptor.Decrypt(cipher);
            var decryptedHumanReadable = Encoding.ASCII.GetString(decrypted);
            TestContext.WriteLine(decryptedHumanReadable);

            Assert.IsTrue(plaintext.SequenceEqual(decrypted));
        }

        /// <summary>
        /// I suddenly get it. We cant fint the key to be the same as what we give it. Thats not the purpose. Because the keystream is an encryption of
        /// the counter using our acutal "key". This means what we get from the XORing is not the key as we know it, its the "keystream" which is
        /// the encryption of the counter using the actual key. The goal is to get that key, and then using it to decrypt.
        /// </summary>
        [TestCategory("Set3"), TestMethod]
        public void TestChallenge20()
        {
            var plaintexts = Helper.Base64ChallengeFileToListOfByteArrays(20);

            var ciphertexts = new List<byte[]>();
            var key = AESWrapper.RandomKey();

            var nonce = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

            foreach (var plain in plaintexts)
            {
                var ctr = new AESWrapper.CTR(key, nonce);

                var cipher = ctr.Encrypt(plain);
                ciphertexts.Add(cipher);
            }

            var truncated = Converter.TruncateCiphers(ciphertexts);

            Analysis.BreakFixedNonceCTR(truncated, truncated.First().Length);
        }

        [TestCategory("Set3"), TestMethod]
        public void TestChallenge21()
        {
            var now = Operations.ToUnixTimeStamp(DateTime.Now);
            var rng = new MT19937(now);

            int roundtrip = 100;

            uint[] firstPass = new uint[roundtrip];

            for(int i = 0; i < 100; i++)
            {
                firstPass[i] = rng.ExtractNumber();
            }

            uint[] secondPass = new uint[roundtrip];

            var other = new MT19937(now);

            for(int i = 0; i < 100; i++)
            {
                secondPass[i] = other.ExtractNumber();
                Assert.AreEqual(firstPass[i], secondPass[i]);
            }

        }

        /* This is commented out because it can take 30 min (since i do indeed wait up to 1000 seconds) */
        /* So we dont want to run it when we run all tests. But it does work */
        public void TestChallenge22()
        {
            /* We got the following result after playing the waiting game. We waited s_1 \in {40, 1000} seconds, seeded, waited
             * s_2 \in {40, 1000} seconds and then extracted the below output
             *   
             */
            var breaker = new RNGBreaker();

            var output  = breaker.WaitAndBleed();

            /* We assume people use it once and throw it away. So we assume they only call it once. thus we do the same, and "step back in time" by
             * decreasing the unix timestamp */
            uint seed = Operations.ToUnixTimeStamp(DateTime.Now);
            while(true)
            {
                var rng = new MT19937(seed);
                if(rng.ExtractNumber() == output)
                {
                    Console.WriteLine("Seed: {0}, output: {1}", seed, output);
                    break;
                }
                seed--; //go back in time
            }

            Console.ReadLine();
        }

        [TestCategory("Set3"), TestMethod]
        public void TestChallenge23()
        {
            var now = Operations.ToUnixTimeStamp(DateTime.Now);
            var rng = new MT19937(now);

            var size = MT19937.n;

            uint[] origSeeds = rng.GetState();
            uint[] seeds = new uint[size];
            uint[] results = new uint[size];

            for(int i = 0; i < size; i++)
            {
                var temp = rng.ExtractNumber();
                results[i] = temp;
                seeds[i] = MT19937.Untemper(temp);
            }

            /* check the extracted seeds are correct */
            for(int i = 0; i < size; i++)
            {
                Assert.AreEqual(seeds[i], origSeeds[i]);
            }

            /* Create new rng with the extracted seeds */
            var spliced = new MT19937(seeds);
            for(int i = 0; i < size; i++)
            {
                var temp = spliced.ExtractNumber();

                Assert.AreEqual(results[i], temp);
            }

            /* How to prevent such reversing? A cryptographic hash somewhere would prevent it */
            /* since such hashes (sha256 fx) have a compression function that actually throws data away */
            /* Such that reverse engineering as we do here is simply impossible - the data is lost */
            /* But here, the data can be recovered due to the xoring and whatnot. */
        }

        [TestCategory("Set3"), TestMethod]
        public void TestChallenge24_EncryptionDecryption_ShortPlaintext()
        {
            /* Uneven amount of bytes (not divisible by 8 or 32) */
            UInt16 seed = UInt16.MaxValue;
            var streamCipher = new MT19937StreamCipher(seed);

            var plaintext = "YELLOW SUBMARINE!";
            var rawPlaintext = Encoding.ASCII.GetBytes(plaintext);

            var ciphertext = streamCipher.Encrypt(rawPlaintext);
            var decrypted = streamCipher.Decrypt(ciphertext);

            var decryptedASCII = Encoding.ASCII.GetString(decrypted);

            Assert.AreEqual(plaintext, decryptedASCII);
        }

        [TestCategory("Set3"), TestMethod]
        public void TestChallenge24_EncryptionDecryption_LongerPlaintext()
        {
            UInt16 seed = UInt16.MaxValue;
            var streamCipher = new MT19937StreamCipher(seed);

            var plaintext = "YELLOW SUBMARINE IS SUPREME MY MACHINE LOLOLOL!";
            var rawPlaintext = Encoding.ASCII.GetBytes(plaintext);

            var ciphertext = streamCipher.Encrypt(rawPlaintext);

            var ciphertextASCII = Encoding.ASCII.GetString(ciphertext);

            var decrypted = streamCipher.Decrypt(ciphertext);

            var decryptedASCII = Encoding.ASCII.GetString(decrypted);

            Assert.AreEqual(plaintext, decryptedASCII);
        }

        [TestCategory("Set3"), TestMethod]
        public void TestChallenge24_RecoverSeed()
        {
            /* Prepare ciphertext */
            var knownPlaintextAscii = "AAAAAAAAAAAAAA";
            var knownPlaintextRaw = Encoding.ASCII.
                                    GetBytes(knownPlaintextAscii);
            var random = new Random();
            var randomPlaintextLength = random.Next(10, 50);
            var randomPlaintextRaw = new byte[randomPlaintextLength];
            random.NextBytes(randomPlaintextRaw);

            var finalPlaintext = knownPlaintextRaw;

            finalPlaintext.Concat(randomPlaintextRaw);

            var secretKey = (ushort)random.Next();

            var streamCipher = new MT19937StreamCipher(secretKey);
            var cipherText = streamCipher.Encrypt(finalPlaintext);

            var plaintext = streamCipher.Decrypt(cipherText);

            var contains = Encoding.ASCII.GetString(plaintext).Contains(knownPlaintextAscii);

            Assert.IsTrue(contains);

            /* Recover Key. But how? */
            /* The keyspace for each key is only 2^16. */
            /* Could we just bruteforce it? We only have to check 65k keys. */
            var key = MT19937StreamCipher.BruteForceKey(cipherText, knownPlaintextAscii);

            Assert.AreEqual(key, secretKey);
        }

        [TestCategory("Set3"), TestMethod]
        public void TestChallenge24_DetectPasswordResetTokenFromCurrentTimestamp()
        {
            var token = PasswordResetToken.GetNext();

            var detector = new MT19937TokenDetector();
            var isNow = detector.IsTokenSeededFromNow(token);

            Assert.IsTrue(isNow);

            Thread.Sleep(2000);

            var wasInPast = detector.IsTokenSeededFromPast(token, 10);

            Assert.IsTrue(wasInPast.IsSeededFromPast);
        }
    }
}
