﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Crypto;
using static Crypto.UnpaddedMessageRecoveryOracle;
using System.Security.Cryptography;
using System.Numerics;
using System.Text;
using Crypto.DigitalSignatureAlgorithm;
using Crypto.Numerical;
using System.Threading.Tasks;

namespace ChallengeTests
{
    [TestClass]
    public class TestSet6
    {
        [TestCategory("Set6"), TestMethod]
        public void Challenge41()
        {
            var oracle = new UnpaddedMessageRecoveryOracle();

            var plaintext = "Secret";
            var plaintextBigInt = Crypto.RSA.CheesyStringToBigInt(plaintext);
            var encrypted = Crypto.RSA.Encrypt(oracle.PublicKey, plaintextBigInt);
            var message = new Message(encrypted.ToByteArray());

            var decrypt = oracle.Decrypt(message);

            try
            {
                var decryptAgain = oracle.Decrypt(message);
            }
            catch(Exception e)
            {
                Assert.AreEqual(typeof(CryptographicException), e.GetType());
            }

            /* what if we alter ciphertext a bit? */
            /* RSA has the multiplicative property (homomorphism) so if you do not padd (e.g. remove the direct relation between number and bytes), */
            /* you can do all sorts of stuff since the algebra is still in there! See below, we multiply to get around hash, then we can invmod afterwards to recover plaintext. */

            BigInteger C = new BigInteger(message.Bytes);
            BigInteger S = Crypto.RSA.GeneratePrime(256);

            var forgedC = (BigInteger.ModPow(S, oracle.PublicKey.e, oracle.PublicKey.N) * C) % oracle.PublicKey.N;

            var forgedMessage = new Message(forgedC.ToByteArray());

            /* message is different due to mult, gets through hash check */ 
            var decryptForged = oracle.Decrypt(forgedMessage);

            var forgedP = new BigInteger(decryptForged);

            /* now: P = P' / S mod N. Get inverse and multiply (thats how you divide in a cyclic group!) */
            var inverseS = BigIntegerUtility.ModInverse(S, oracle.PublicKey.N);

            var P = forgedP * inverseS % oracle.PublicKey.N;

            var attackResult = Crypto.RSA.CheesyBigIntToString(P);

            Assert.AreEqual(plaintext, attackResult);
        }

        [TestCategory("Set6"), TestMethod]
        public void Challenge42()
        {
            var message = "hi mom";
            var rawMessage = Encoding.ASCII.GetBytes(message);
            var keyGenResult = Crypto.RSA.ProbabilisticKeyGen(3, 512);
            var privateKey = keyGenResult.PrivateKey;

            var signature = PKCS1Signature.Sign(rawMessage, privateKey);
            var valid = PKCS1Signature.Verify(rawMessage, signature, keyGenResult.PublicKey, false);

            Assert.IsTrue(valid);

            var forged = PKCS1Signature.ForgeSignatureEQuals3(rawMessage, "SHA-1", keyGenResult.PublicKey);

            valid = PKCS1Signature.Verify(rawMessage, forged, keyGenResult.PublicKey, true);

            Assert.IsTrue(valid);
        }

        [TestCategory("Set6"), TestMethod]
        public void Challenge43_TestPrerequisites()
        {
            /* Lets just test we can actually sign and validate stuff */
            var parameters = new Crypto.DigitalSignatureAlgorithm.DSAParameters();


            var message = Encoding.Default.GetBytes("Hello there");
            var hashAlg = new SHA256Managed();

            var signature = Crypto.DigitalSignatureAlgorithm.DSA.Sign(parameters, message, hashAlg);

            var valid = Crypto.DigitalSignatureAlgorithm.DSA.Verify(parameters, message, hashAlg, signature);

            Assert.IsTrue(valid);

            var k = Crypto.DigitalSignatureAlgorithm.DSA.LastUsedK;
            var calculatedX = Crypto.DigitalSignatureAlgorithm.DSA.GetPrivateKeyFromK(parameters.q, k, message, hashAlg, signature);

            Assert.AreEqual(parameters.x, calculatedX);
        }            

        [TestMethod]
        public void Challenge43()
        {
            var message = Encoding.Default.GetBytes("For those that envy a MC it can be hazardous to your health\nSo be friendly, a matter of life and death, just like a etch-a-sketch\n");
            var hashAlg = new SHA1Managed();

            var hashedMessage = hashAlg.ComputeHash(message);
            var hexstring = BitConverter.ToString(hashedMessage).Replace("-", "");

            /* First we just try to guess our own random k to see if logic works? */
            var q = BigIntegerUtility.FromHexStringToPositive(Crypto.DigitalSignatureAlgorithm.DSAParameters.qDefaultHex);
            var p = BigIntegerUtility.FromHexStringToPositive(Crypto.DigitalSignatureAlgorithm.DSAParameters.pDefaultHex);
            var g = BigIntegerUtility.FromHexStringToPositive(Crypto.DigitalSignatureAlgorithm.DSAParameters.gDefaultHex);

            /* not needed, just do it for the constructor */
            var x = Crypto.DigitalSignatureAlgorithm.DSA.GenerateRandomBelow(q);
            var y = BigInteger.ModPow(g, x, p);

            var parameters = new Crypto.DigitalSignatureAlgorithm.DSAParameters(q, p, g, x, y);
            
            int lower = 1;
            int upper = 65536;

            BigInteger foundK = new BigInteger(-1);

            // We can guess our own k, so the logic is sound. But the given parameters, i cant quite get to work. .NET biginteger is weird with endianness and sign.. 
            var random = new Random();
            var ownK = new BigInteger(random.Next(1, 65536));

            var signature = Crypto.DigitalSignatureAlgorithm.DSA.Sign(parameters, message, hashAlg, ownK);

            Parallel.For(lower, upper + 1, (i, state) => 
            {
                var lowerBigInt = new BigInteger(i);
                var calculatedX = Crypto.DigitalSignatureAlgorithm.DSA.GetPrivateKeyFromK(q, lowerBigInt, message, hashAlg, signature);

                if (calculatedX > 0)
                {
                    var calculatedY = BigInteger.ModPow(parameters.g, calculatedX, parameters.p);

                    if(parameters.y.Equals(calculatedY))
                    {
                        foundK = lowerBigInt;
                        state.Stop();
                    }
                }

            });

            Assert.AreNotEqual(new BigInteger(-1), foundK);

            // Can we guess the k from cryptopals? Also between 0 and 2^16. 
            // Set to challenge y. 

            // OBS: These are litteral integers as strings, its not hex values. TODO: check if other solutions treat these as ints or hex? 
            var challengeSignature = new Crypto.DigitalSignatureAlgorithm.DSA.Signature(BigInteger.Parse("548099063082341131477253921760299949438196259240"),
                                                                                        BigInteger.Parse("857042759984254168557880549501802188789837994940"));
            
            var challengeY = BigIntegerUtility.FromHexStringToPositive("84ad4719d044495496a3201c8ff484feb45b962e7302e56a392aee4" +
                                                                      "abab3e4bdebf2955b4736012f21a08084056b19bcd7fee56048e004" +
                                                                      "e44984e2f411788efdc837a0d2e5abb7b555039fd243ac01f0fb2ed" +
                                                                      "1dec568280ce678e931868d23eb095fde9d3779191b8c0299d6e07b" +
                                                                      "bb283e6633451e535c45513b2d33c99ea17");

            foundK = new BigInteger(-1);
            parameters.y = challengeY;

            Parallel.For(lower, upper + 1, (i, state) => 
            {
                var lowerBigInt = new BigInteger(i);
                var calculatedX = Crypto.DigitalSignatureAlgorithm.DSA.GetPrivateKeyFromK(q, lowerBigInt, message, hashAlg, challengeSignature);

                if (calculatedX > 0)
                {
                    var calculatedY = BigInteger.ModPow(parameters.g, calculatedX, parameters.p);

                    if(parameters.y.Equals(calculatedY))
                    {
                        foundK = lowerBigInt;
                        state.Stop();
                    }
                }
            });

            // TODO: Maybe check the actual hash and whatnot, just to be nicer. 
            Assert.AreNotEqual(new BigInteger(-1), foundK);
        }

        [TestMethod]
        public void Challenge44()
        {
            var y = BigIntegerUtility.FromHexStringToPositive("2d026f4bf30195ede3a088da85e398ef869611d0f68f07" +
                                                                "13d51c9c1a3a26c95105d915e2d8cdf26d056b86b8a7b8" +
                                                                "5519b1c23cc3ecdc6062650462e3063bd179c2a6581519" +
                                                                "f674a61f1d89a1fff27171ebc1b93d4dc57bceb7ae2430" +
                                                                "f98a6a4d83d8279ee65d71c1203d2c96d65ebbf7cce9d3" +
                                                                "2971c3de5084cce04a2e147821");


            var q = BigIntegerUtility.FromHexStringToPositive(Crypto.DigitalSignatureAlgorithm.DSAParameters.qDefaultHex);
            var p = BigIntegerUtility.FromHexStringToPositive(Crypto.DigitalSignatureAlgorithm.DSAParameters.pDefaultHex);
            var g = BigIntegerUtility.FromHexStringToPositive(Crypto.DigitalSignatureAlgorithm.DSAParameters.gDefaultHex);

            /* not needed, just do it for the constructor */
            var x = Crypto.DigitalSignatureAlgorithm.DSA.GenerateRandomBelow(q);


            var parameters = new Crypto.DigitalSignatureAlgorithm.DSAParameters(q, p, g, x, y);

            /* For each pair of messages, try to see if we can get a k, that then gives us an x, that gives us the public key above */
            var messages = DSASignatureDto.Challenge44Signatures;
            var hashAlg = new SHA1Managed();

            var foundK = new BigInteger(-1);

            for(int i = 0; i < messages.Count - 1; i++)
            {
                var first = messages[i];

                var signature = new Crypto.DigitalSignatureAlgorithm.DSA.Signature(first.r, first.s);

                /* Go through all the others */
                for (int j = i + 1; j < messages.Count; j++)
                {
                    var second = messages[j];

                    var candidateK = Crypto.DigitalSignatureAlgorithm.DSA.GetKFromRepeatedUsage(first, second, hashAlg, q);

                    var calculatedX = Crypto.DigitalSignatureAlgorithm.DSA.GetPrivateKeyFromK(q, candidateK, Encoding.Default.GetBytes(first.Message), hashAlg, signature);

                    if (calculatedX > 0)
                    {
                        var calculatedY = BigInteger.ModPow(parameters.g, calculatedX, parameters.p);

                        if (calculatedY.Equals(y))
                        {
                            foundK = candidateK;
                            break;
                        }
                    }
                }
            }

            // TODO: Maybe check the actual hash and whatnot, just to be nicer. 
            Assert.AreNotEqual(new BigInteger(-1), foundK);
        }

        [TestMethod]
        public void Challenge45_gEqualsZero()
        {
            /* g = 0 */
            /* if we sign with g = 0, then r = (g^k mod p) moq q = 0 */
            /* and then v = g^u1 y^u2 mod p) moq q = 0 as well. So r = 0 s = whatever is a valid signature for everything */
            var g = new BigInteger(0);
            var q = BigIntegerUtility.FromHexStringToPositive(Crypto.DigitalSignatureAlgorithm.DSAParameters.qDefaultHex);
            var p = BigIntegerUtility.FromHexStringToPositive(Crypto.DigitalSignatureAlgorithm.DSAParameters.pDefaultHex);
            
            var x = Crypto.DigitalSignatureAlgorithm.DSA.GenerateRandomBelow(q);
            var k = Crypto.DigitalSignatureAlgorithm.DSA.GenerateRandomBelow(q);

            var parameters = new Crypto.DigitalSignatureAlgorithm.DSAParameters(q, p, g, x);
            
            var message = Encoding.ASCII.GetBytes("Hello There");

            var hashAlg = new SHA1Managed();

            var signature = Crypto.DigitalSignatureAlgorithm.DSA.Sign(parameters, message, hashAlg, k);

            /* r will always be zero, so will v, so you can pass anything as the signed message, just see below */
            var totallyOtherMessage = Encoding.ASCII.GetBytes("I wanna sell my stocks");

            var valid = Crypto.DigitalSignatureAlgorithm.DSA.Verify(parameters, totallyOtherMessage, hashAlg, signature);

            Assert.IsTrue(valid);
        }

        [TestMethod]
        public void Challenge45_gEqualsPplus1()
        {
            /* g = (p+1) */
            /* ((p+1) ^ anything mod p = 1, so R = 1 always, and so will v. */
            var q = BigIntegerUtility.FromHexStringToPositive(Crypto.DigitalSignatureAlgorithm.DSAParameters.qDefaultHex);
            var p = BigIntegerUtility.FromHexStringToPositive(Crypto.DigitalSignatureAlgorithm.DSAParameters.pDefaultHex);
            var g = p + 1;

            var x = Crypto.DigitalSignatureAlgorithm.DSA.GenerateRandomBelow(q);
            var k = Crypto.DigitalSignatureAlgorithm.DSA.GenerateRandomBelow(q);

            var parameters = new Crypto.DigitalSignatureAlgorithm.DSAParameters(q, p, g, x);

            var hashAlg = new SHA1Managed();

            var message = Encoding.ASCII.GetBytes("Hello, world");
            var z = Crypto.DigitalSignatureAlgorithm.DSA.HashMessageThenConvertToBigInteger(message, hashAlg);

            /* we just know the public parameters, the public key, and the message, and we can forge easily like below due to p+1 */
            var r = BigIntegerUtility.mod(BigInteger.ModPow(parameters.y, z, p), q);
            var zInverse = BigIntegerUtility.ModInverse(z, q);
            var s = BigIntegerUtility.mod(r * zInverse, q);

            var forgedSignature = new Crypto.DigitalSignatureAlgorithm.DSA.Signature(r, s);

            var valid = Crypto.DigitalSignatureAlgorithm.DSA.Verify(parameters, message, hashAlg, forgedSignature);

            Assert.IsTrue(valid);
        }

        [TestMethod]
        public void Challenge46()
        {
            var e = new BigInteger(3);
            /* We dont do the full 1024, since the challenge message fits in the below, and then its goes faster. */
            var keygen = Crypto.RSA.ProbabilisticKeyGen(e, 768);

            /* Test the even stuff */
            var anEvenInteger = new BigInteger(2);
            var encryptedEven = Crypto.RSA.Encrypt(keygen.PublicKey, anEvenInteger);

            var isEven = Crypto.RSA.IsPlaintextEven(keygen.PrivateKey, encryptedEven);

            Assert.IsTrue(isEven);

            var anUnevenInteger = new BigInteger(3);
            var encryptedUneven = Crypto.RSA.Encrypt(keygen.PublicKey, anUnevenInteger);

            isEven = Crypto.RSA.IsPlaintextEven(keygen.PrivateKey, encryptedUneven);

            Assert.IsFalse(isEven);

            /* Do the challenge */
            var plaintextB64 = "VGhhdCdzIHdoeSBJIGZvdW5kIHlvdSBkb24ndCBwbGF5IGFyb3VuZCB3aXRoIHRoZSBGdW5reSBDb2xkIE1lZGluYQ==";
            var plaintextBytes = Convert.FromBase64String(plaintextB64);
            var plaintext = Encoding.ASCII.GetString(plaintextBytes);
            var plaintextInt = Crypto.RSA.CheesyStringToBigInt(plaintext);
            
            var ciphertext = Crypto.RSA.Encrypt(keygen.PublicKey, plaintextInt);

            var parityDecrypted = Crypto.RSA.ParityDecryption(keygen.PrivateKey, ciphertext, keygen.PublicKey);

            /* The last few digits are not the same (e.g. four or so) so the decrypted text is not always the same for the few last characters. */
            var decryptedString = Crypto.RSA.CheesyBigIntToString(parityDecrypted);

            Assert.AreEqual(plaintext, decryptedString);
        }
    }


}
