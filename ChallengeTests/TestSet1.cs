﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Crypto;
using System.Text;

namespace ChallengeTests
{
    [TestClass]
    public class TestSet1
    {
        private static string file_prefix = "C:\\Users\\Søren\\Documents\\cryptopals\\ChallengeTests\\files\\";
        private TestContext testContextInstance;
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestCategory("Set1"), TestMethod]
        public void TestChallenge1()
        {
            string test = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
            string check = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t";
            byte[] res_raw = Converter.FromHexToBase64Byte(test);
            string res_raw_string = Convert.ToBase64String(res_raw);
            string res = Encoding.ASCII.GetString(res_raw);
            bool testpassed = (String.Compare(res_raw_string, check) == 0) ? true : false;

            TestContext.WriteLine("\n ----- Testing ConvertHexToBase64 (chal 1) -----\n");
            TestContext.WriteLine("Input: \n" + test + "\n");
            TestContext.WriteLine("Expected Output: \n" + check + "\n");
            TestContext.WriteLine("Actual Output: \n" + res_raw_string + "\n");
            TestContext.WriteLine("Ascii Output: \n " + res + "\n");
            TestContext.WriteLine("Test passed: " + testpassed.ToString() + "\n");

            Assert.IsTrue(testpassed);
        }

        [TestCategory("Set1"), TestMethod]
        public void TestChallenge2()
        {
            string test1_chal2 = "1c0111001f010100061a024b53535009181c";
            byte[] test1_chal2_raw = Converter.FromHexToBase64Byte(test1_chal2);
            string test2_chal2 = "686974207468652062756c6c277320657965";
            byte[] test2_chal2_raw = Converter.FromHexToBase64Byte(test2_chal2);

            string expected = "746865206b696420646f6e277420706c6179";
            byte[] expected_bytes = Converter.FromHexToBase64Byte(expected);

            TestContext.WriteLine("expected bytes: " + Encoding.ASCII.GetString(expected_bytes));

            byte[] bytes = Operations.FixedBitwiseXOR(test1_chal2_raw, test2_chal2_raw);
            TestContext.WriteLine("bytes: " + Encoding.ASCII.GetString(bytes));

            /* bitconverter returns hex of the form 06-C6... so we have to remove - and lower? :/ */
            string res = BitConverter.ToString(bytes);
            res = res.Replace("-", "").ToLower();
            bool testpassed = (String.Compare(res, expected) == 0) ? true : false;
            string res_ascii = Encoding.ASCII.GetString(bytes);

            TestContext.WriteLine("\n ----- Testing FixedXOR (chal 2) ----- \n");
            TestContext.WriteLine("Input: \n" + test1_chal2 + "\n");
            TestContext.WriteLine("Expected Output: \n" + expected + "\n");
            TestContext.WriteLine("Actual Output: \n" + res + "\n");
            TestContext.WriteLine("Ascii Output: \n" + res_ascii + "\n");
            TestContext.WriteLine("stringtohex?: \n" + Converter.FromStringToHex(res_ascii) + "\n");
            TestContext.WriteLine("Test passed: " + testpassed.ToString() + "\n");

            Assert.IsTrue(testpassed);
        }

        [TestCategory("Set1"), TestMethod]
        public void TestChallenge3()
        {
            TestContext.WriteLine("\n ------ Testing Single-Byte XOR cipher (chal 3) ------ \n");

            /* prepare challenge */
            string test3_challenge_hex = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736";
            byte[] test3_raw = Converter.FromHexToBase64Byte(test3_challenge_hex);
            string test3_challenge_ascii = Encoding.ASCII.GetString(test3_raw);
            TestContext.WriteLine("The following string has been XORd against a single character: \n\n" + test3_challenge_hex + "\n");
            TestContext.WriteLine("Trying every possible single byte key in ASCII 0..255\n");

            string alphabet = "abcdefghijklmnopqrstuvwxyz "; //notice the added space! spaces are good, sentences have spaces
            byte[] raw = Converter.FromHexToBase64Byte(test3_challenge_hex);
            FreqResult fr = Analysis.SingleByteKeyAnalysis(raw, alphabet);
            char expected_winner = 'X';
            TestContext.WriteLine("Expected key: " + expected_winner + "\n");
            TestContext.WriteLine("Suggested key from frequency analysis (score " + fr.score + "): " + fr.SingleKeyCharacter + "\n");
            bool test_passed = (expected_winner == fr.SingleKeyCharacter) ? true : false;
            TestContext.WriteLine("Decrypted Ciphertext under key " + fr.SingleKeyCharacter + "\n");
            TestContext.WriteLine(fr.plaintext + "\n");
            TestContext.WriteLine("Test Passed: " + test_passed.ToString() + "\n");

            Assert.IsTrue(test_passed);
        }

        [TestCategory("Set1"), TestMethod]
        public void TestChallenge4()
        {
            TestContext.WriteLine("\n ------ Testing Single-Char detection (chal 4) ------- \n");

            /* run what we did in test3 on each possible line of the file */
            /* the one with the highest score should be the correct one */
            /* solution = "Now that the party is jumping */
            int counter = 0;
            string line;
            FreqResult best = new FreqResult(0, 'a', "dummy");

            /* alphabet we score against. spaces does the trick! small letters are awarded, since sentences are spelled primarily in lower */
            /* whereas "random garbage" should have an even distribution of upper and lower. Thus we do not reward upper case. */
            //string alphabet = "abcdefghijklmnopqrstuvwxyz ";
            string alphabet = "etaoin shrdlu";
            /* open ciphertexts */
            string challenge_filename = Helper.GetPathToTestfile(4);
            System.IO.StreamReader file = new System.IO.StreamReader(challenge_filename);
            while ((line = file.ReadLine()) != null)
            {
                counter++;
                byte[] raw = Converter.FromHexToBase64Byte(line);
                FreqResult fr = Analysis.SingleByteKeyAnalysis(raw, alphabet);
                if (fr.score > best.score)
                {
                    best = fr;
                }
            }
            TestContext.WriteLine("winner? score: " + best.score.ToString() + ", key: " + best.SingleKeyCharacter + ", plaintext: " + best.plaintext.ToString());
            string expected_answer = "Now that the party is jumping\n";
            TestContext.WriteLine("expected plaintext:" + expected_answer);
            bool test_passed = (String.Compare(expected_answer, best.plaintext) == 0) ? true : false;
            TestContext.WriteLine("Test Passed: " + test_passed.ToString());

            Assert.IsTrue(test_passed);
        }

        [TestCategory("Set1"), TestMethod]
        public void TestChallenge5()
        {
            TestContext.WriteLine("\n ------ Testing Repeating Key XOR (chal 5) ------- \n");
            string plaintext = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal";
            string key = "ICE";

            string expected_hex = "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f";

            string first_hex = Converter.FromStringToHex(plaintext);
            string key_hex = Converter.FromStringToHex(key);

            byte[] first_res_raw = Operations.RepeatingKeyXOR(plaintext, key);

            string first_res_hex = BitConverter.ToString(first_res_raw).Replace("-", string.Empty).ToLower();

            bool test_passed = (String.Compare(expected_hex, first_res_hex) == 0);

            TestContext.WriteLine("expected first: \t" + expected_hex + "\n");
            TestContext.WriteLine("result first: \t\t" + first_res_hex + "\n");

            byte[] key_raw = Converter.FromHexToBase64Byte(Converter.FromStringToHex(key));
            byte[] decrypt_raw = Operations.RepeatingKeyXOR(first_res_raw, key_raw);
            string decrypt_ascii = Encoding.ASCII.GetString(decrypt_raw);
            TestContext.WriteLine("decrypted: " + decrypt_ascii + "\n");

            TestContext.WriteLine("Test Passed: " + test_passed + "\n");

            Assert.IsTrue(test_passed);
        }

        [TestCategory("Set1"), TestMethod]
        public void TestChallenge6()
        {
            string real_key = "Terminator X: Bring the noise";
            string real_key_hex = Converter.FromStringToHex(real_key).Replace("-", string.Empty).ToLower();
            byte[] real_key_raw = Converter.FromHexToBase64Byte(real_key_hex);

            TestContext.WriteLine("\n ------ Testing Edit distance (chal 5.5) ------\n");
            int expected_distance = 37;
            string buffer1 = "this is a test";
            string buffer2 = "wokka wokka!!!";
            int distance = Operations.EditDistance(buffer1, buffer2);
            TestContext.WriteLine("expected: {0}, got {1}\n", expected_distance, distance);

            TestContext.WriteLine("\n ------ Breaking Repeating Key XOR (chal 6.0) ------\n");
            
            byte[] cipher_raw = Helper.Base64ChallengeFileToByteArray(6);
            byte[] key = Analysis.BreakRepeatingKeyXOR(cipher_raw);


            bool testpassed = (String.Compare(real_key, Encoding.ASCII.GetString(key)) == 0);

            TestContext.WriteLine("Expected key: {0}", real_key);
            TestContext.WriteLine("Extractd key: {0}", Encoding.ASCII.GetString(key));

            byte[] raw_res = Operations.RepeatingKeyXOR(cipher_raw, key);
            string plaintext = Encoding.ASCII.GetString(raw_res);
            TestContext.WriteLine("resulting plaintext: \n" + plaintext);

            Assert.IsTrue(testpassed);
        }

        [TestCategory("Set1"), TestMethod]
        public void TestChallenge7()
        {
            string key = "YELLOW SUBMARINE";
            byte[] key_raw = Encoding.ASCII.GetBytes(key);
            byte[] cipher_raw = Helper.Base64ChallengeFileToByteArray(7);
            byte[] plaintext_raw = AESWrapper.ECB.Decrypt(cipher_raw, key_raw, new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });

            string plaintext_ascii = Encoding.ASCII.GetString(plaintext_raw);

            TestContext.WriteLine("\n ------ Testing ECB encryption/decryption ------ \n");
            string test = "YELLOW SUBMARINE IS SUPREMEEEEEE";
            string test_hex = Converter.FromStringToHex(test).Replace("-", string.Empty);
            byte[] test_raw = Encoding.ASCII.GetBytes(test);
            byte[] cipher = AESWrapper.ECB.Encrypt(test_raw, key_raw, new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
            byte[] decrypted = AESWrapper.ECB.Decrypt(cipher, key_raw, new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
            string result = Encoding.ASCII.GetString(decrypted);

            TestContext.WriteLine("plaintext: {0}", test);
            TestContext.WriteLine("cipher {0}", Encoding.ASCII.GetString(cipher), cipher.Length);
            TestContext.WriteLine("result: {0}", result, result.Length);

            TestContext.WriteLine("\n ------ Calling AES-128-ECB (chal 7) ------ \n");

            TestContext.WriteLine("Resulting plaintext: " + plaintext_ascii);

            bool testpassed = true;
            Assert.IsTrue(testpassed);
        }

        [TestCategory("Set1"), TestMethod]
        public void Challenge8()
        {
            string filename = Helper.GetPathToTestfile(8);

            /* get each line. Convert to byte. Count in blocks of 16. Take avrg of hamming distance of all, the more similar, the less distance */
            /* and ECB should have many duplicatiosn => small overall distance due to its deterministic nature */
            /* so, for each line, for all combinations of blocks, we do the hamming distance, the smallest overall should have many dupli -> its the ECB */

            StreamReader input = new StreamReader(filename);
            string line;
            string best_line;
            int lowest_line = 0;
            int line_count = 0;
            int lowest_so_far = Int32.MaxValue;
            int blocksize = 16;
            while ((line = input.ReadLine()) != null)
            {
                byte[] raw_line = Converter.FromHexToBase64Byte(line);
                int distance = Operations.AverageEditDistance(raw_line, blocksize);
                if (distance < lowest_so_far)
                {
                    lowest_so_far = distance;
                    lowest_line = line_count;
                    best_line = line;
                }
                line_count++;
            }

            int expected_answer = 132; //based on what others have deduced. Kinda hard to know since i cant decrypt it.. 8D
            bool testpassed = false;
            if (lowest_line == expected_answer) testpassed = true;

            TestContext.WriteLine("Best line: {0}", lowest_line);
            TestContext.WriteLine("Expected:  {0}", expected_answer);
            TestContext.WriteLine("Test passed: {0}", testpassed.ToString());

            Assert.IsTrue(testpassed);
        }
    }
}
