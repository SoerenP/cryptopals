﻿using Crypto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeTests
{
    public static class Helper
    {
        public static byte[] Base64ChallengeFileToByteArray(int challenge)
        {
            return Converter.Base64FileToByteArray(GetPathToTestfile(challenge));
        }

        public static byte[] Base64ChallengeFileToByteArray(string filename)
        {
            return Converter.Base64FileToByteArray(GetPathToTestfile(filename));
        }

        public static List<byte[]> Base64ChallengeFileToListOfByteArrays(int challenge)
        {
            return Converter.Base64FileToListOfByteArrays(GetPathToTestfile(challenge));
        }

        public static string GetPathToTestfile(int challenge)
        {
            var challengeFile = GetFileEnding(challenge.ToString());
            return Path.Combine(Environment.CurrentDirectory, challengeFile);
        }

        public static string GetPathToTestfile(string filename)
        {
            return Path.Combine(Environment.CurrentDirectory, filename);
        }

        private static string GetFileEnding(string prefix)
        {
            return prefix + ".txt";
        }
    }
}
