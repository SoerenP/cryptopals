﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Crypto;
using System.Numerics;

namespace ChallengeTests
{
    [TestClass]
    public class TestSet4
    {
        private readonly string filePathPrefix = "C:\\Users\\Søren\\Documents\\cryptopals\\ChallengeTests\\files\\";

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestCategory("Set4"), TestMethod]
        public void TestChallenge25()
        {
            //Use plaintext from 7.txt
            var key = Encoding.ASCII.GetBytes("YELLOW SUBMARINE");
            var plaintext = AESWrapper.ECB.Decrypt(Helper.Base64ChallengeFileToByteArray(7), key);

            var discencryptor = new AESWrapper.DiscEncryption(plaintext);

            /* Give a known plaintext P'. Then we get C' = P' XOR K'. Then calculate K' = C' XOR P' = P' XOR P' XOR K' */
            var ciphertext = discencryptor.GetCiphertext();

            var knownPlaintext = Enumerable.Repeat((byte)'A', ciphertext.Length).ToArray();
            var CPRime = discencryptor.Edit(ciphertext, 0, knownPlaintext);
            var keystream = Operations.FixedBitwiseXOR(knownPlaintext, CPRime);

            /* We know keystream. CTR jsut xors keystream with cipher/plaintext. So we can use it directly */
            var recoveredPlaintext = Operations.FixedBitwiseXOR(ciphertext, keystream);

            Assert.IsTrue(recoveredPlaintext.SequenceEqual(plaintext));
        }

        [TestCategory("Set4"), TestMethod]
        public void TestChallenge26()
        {
            string to_encrypt = ";admin=true;";
            byte[] encrypted = AESWrapper.CTR.ParseThenPadd(to_encrypt);
            bool found = AESWrapper.CTR.LookForAdmin(encrypted);
            Assert.IsFalse(found);

            byte[] forged = Analysis.CTRBitFlipAttack(AESWrapper.CTR.ParseThenPadd);

            var ctr = new AESWrapper.CTR(AESWrapper.GetPaddingOracleFixedKey, AESWrapper.GetPaddingOracleFixedNonce);

            string decryped = Encoding.ASCII.GetString(ctr.Decrypt(forged));

            TestContext.WriteLine(decryped);

            Assert.IsTrue(AESWrapper.CTR.LookForAdmin(forged));
        }

        [TestCategory("Set4"), TestMethod]
        public void TestChallenge27()
        {
            var key = Analysis.CBCKeyAsIVAttack(AESWrapper.CBC.ParseThenPaddUseKeyAsIv);

            var usedKey = AESWrapper.GetPaddingOracleFixedKey;

            TestContext.WriteLine("Found key: " + Encoding.ASCII.GetString(key));
            TestContext.WriteLine("Used key: " + Encoding.ASCII.GetString(usedKey));

            Assert.IsTrue(key.SequenceEqual(usedKey));
        }

        [TestCategory("Set4"), TestMethod]
        public void SHA1Test()
        {
            var message = Encoding.ASCII.GetBytes("The quick brown fox jumps over the lazy dog");

            var expected = "2F-D4-E1-C6-7A-2D-28-FC-ED-84-9E-E1-BB-76-E7-39-1B-93-EB-12";

            var hash = SHA1.Hash(message);

            var result = BitConverter.ToString(hash);

            Assert.IsTrue(result.SequenceEqual(expected));
        }

        [TestCategory("Set4"), TestMethod]
        public void TestChallenge28()
        {
            var messageAscii = "SELL ALL THE STOCKS SIGNED MR MONEY";
            var message = Encoding.ASCII.GetBytes(messageAscii);

            var mac = SHA1KeyedMAC.GetMessageDigest(message);

            Assert.IsTrue(mac.SequenceEqual(SHA1KeyedMAC.GetMessageDigest(message)));

            var tamperedMessageAscii = "SELL NON THE STOCKS SIGNED MR MONEY";
            var tamperedMessage = Encoding.ASCII.GetBytes(tamperedMessageAscii);

            var forged = SHA1KeyedMAC.GetMessageDigest(tamperedMessage);

            Assert.IsFalse(mac.SequenceEqual(forged));
        }

        [TestCategory("Set4"), TestMethod]
        public void TestChallenge29()
        {
            var data = Encoding.ASCII.GetBytes("comment1=cooking%20MCs;userdata=foo;comment2=%20like%20a%20pound%20of%20bacon");
            var extension = Encoding.ASCII.GetBytes(";admin=true");
            var mac = SHA1KeyedMAC.GetMessageDigest(data);

            var forged = SHA1KeyedMACExtensionAttacker.Forge(data, mac, extension);

            Assert.IsTrue(SHA1KeyedMAC.IsValid(forged));
        }

        [TestCategory("Set4"), TestMethod]
        public void MD4Test()
        {
            /* Hashing */
            var message = Encoding.ASCII.GetBytes("a");

            var expexted = "BD-E5-2C-B3-1D-E3-3E-46-24-5E-05-FB-DB-D6-FB-24";

            var hash = MD4.ComputeMD4(message);

            var result = BitConverter.ToString(hash);

            Assert.IsTrue(expexted.SequenceEqual(result));
        }

        [TestCategory("Set4"), TestMethod]
        public void MD4KeyedTest()
        {
            var messageAscii = "SELL ALL THE STOCKS SIGNED MR MONEY";
            var message = Encoding.ASCII.GetBytes(messageAscii);

            var mac = MD4KeyedMAC.GetMessageDigest(message);

            Assert.IsTrue(mac.SequenceEqual(MD4KeyedMAC.GetMessageDigest(message)));

            var tamperedMessageAscii = "SELL NON THE STOCKS SIGNED MR MONEY";
            var tamperedMessage = Encoding.ASCII.GetBytes(tamperedMessageAscii);

            var forged = MD4KeyedMAC.GetMessageDigest(tamperedMessage);

            Assert.IsFalse(mac.SequenceEqual(forged));
        }

        [TestCategory("Set4"), TestMethod]
        public void TestChallenge30()
        {
            var data = Encoding.ASCII.GetBytes("comment1=cooking%20MCs;userdata=foo;comment2=%20like%20a%20pound%20of%20bacon");
            var extension = Encoding.ASCII.GetBytes(";admin=true");
            var mac = MD4KeyedMAC.GetMessageDigest(data);

            var forged = MDKeyedMACExtensionAttacker.Forge(data, mac, extension);

            Assert.IsTrue(MD4KeyedMAC.IsValid(forged));
        }

        [TestCategory("Set4"), TestMethod]
        public void TestHMACHA1()
        {
            var message = Encoding.ASCII.GetBytes("The quick brown fox jumps over the lazy dog");
            var key = Encoding.ASCII.GetBytes("key");
            var expected = "de7c9b85b8b78aa6bc8a7a36f70a90701c9db4d9";

            var result = HMAC.SHA1(key, message);
            var parsedResult = BitConverter.ToString(result).ToLower().Replace("-","");

            TestContext.WriteLine("expected: " + expected);
            TestContext.WriteLine("result: " + parsedResult);

            Assert.IsTrue(parsedResult.SequenceEqual(expected));
        }

        [TestCategory("Set4"), TestMethod]
        public void TestTimeLeakingHMAC()
        {
            var filename = "passwords.txt";

            var message = Encoding.ASCII.GetBytes(filename);

            var key = Encoding.ASCII.GetBytes("scienceathome1234");

            var mac = HMAC.SHA1(key, message);

            var server = new TimeLeakingHMACServer(1, key);

            var isValid = server.Validate(filename, mac);

            Assert.IsTrue(isValid);

            var isFalse = server.Validate(filename + "LOL", mac);

            Assert.IsFalse(isFalse);
        }

        /* Works, but takes approx 30 min or more. */
        /* starts falling around 35 */
        //[TestCategory("Set4"), TestMethod]
        public void TestChallenge31()
        {
            var filename = "passwords.txt";

            var macsize = 20;

            var thresholdMiliseconds = 50;
            
            var server = new TimeLeakingHMACServer(thresholdMiliseconds);

            var actualMac = HMAC.SHA1(server.GetKey(), Encoding.ASCII.GetBytes(filename));

            var forgedMac = TimeLeakPredator.ForgeHMACForFilename(filename, 
                                                                    server.Validate, 
                                                                    macsize, 
                                                                    thresholdMiliseconds);

            var isValid = server.Validate(filename, forgedMac);

            TestContext.WriteLine("forgedMac: " + BitConverter.ToString(forgedMac));

            TestContext.WriteLine("actualMac" + BitConverter.ToString(actualMac));

            Assert.IsTrue(isValid);
        }

        /* Much faster, takes 1 min */
        /* still fails around 40-35 as the single threaded version */
        /* Not super consistent either. Raceconditions? The state.stop might not be enough, maybe we need
           a mutex lock on the byte array. 
             */
        [TestCategory("Set4"), TestMethod]
        public void TestChallenge31Parallel()
        {
            var filename = "passwords.txt";

            var macsize = 20;

            var thresholdMiliseconds = 40;

            var server = new TimeLeakingHMACServer(thresholdMiliseconds);

            var actualMac = HMAC.SHA1(server.GetKey(), Encoding.ASCII.GetBytes(filename));

            var forgedMac = TimeLeakPredator.ForgeHMACForFilenameParallel(filename,
                                                                    server.Validate,
                                                                    macsize,
                                                                    thresholdMiliseconds);

            var isValid = server.Validate(filename, forgedMac);

            TestContext.WriteLine("forgedMac: " + BitConverter.ToString(forgedMac));

            TestContext.WriteLine("actualMac" + BitConverter.ToString(actualMac));

            Assert.IsTrue(isValid);
        }

        [TestCategory("Set4"), TestMethod]
        public void TestChallenge32()
        {
            /* 

             works for 15 ms
             
             */

            var filename = "passwords.txt";

            var macsize = 20;

            var thresholdMiliseconds = 20;

            var server = new TimeLeakingHMACServer(thresholdMiliseconds);

            var actualMac = HMAC.SHA1(server.GetKey(), Encoding.ASCII.GetBytes(filename));

            var forgedMac = TimeLeakPredator.ForgeHMACForFilenameParallelBetter(filename,
                                                                    server.Validate,
                                                                    macsize);

            var isValid = server.Validate(filename, forgedMac);

            TestContext.WriteLine("forgedMac: " + BitConverter.ToString(forgedMac));

            TestContext.WriteLine("actualMac" + BitConverter.ToString(actualMac));

            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void TestModPow()
        {
            var g = new BigInteger(2);

            //1536 bit
            // OBS: you must prepend a 0 to the hex representation, lest the integer becomes negative
            // The reason is in the answer to https://stackoverflow.com/questions/30119174/converting-a-hex-string-to-its-biginteger-equivalent-negates-the-value
            var biggerP = "0FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD1" +
                            "29024E088A67CC74020BBEA63B139B22514A08798E3404DD" +
                            "EF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245" + 
                            "E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7ED" + 
                            "EE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3D" +
                            "C2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F" +
                            "83655D23DCA3AD961C62F356208552BB9ED529077096966D" +
                            "670C354E4ABC9804F1746C08CA237327FFFFFFFFFFFFFFFF";
            var p = BigInteger.Parse(biggerP, System.Globalization.NumberStyles.HexNumber);
            var random = DiffieHellman.RandomIntegerBelow(p);

            // 90 ms
            var result1 = BigInteger.ModPow(g, random, p);

            //// 93 ms
            //var result2 = DiffieHellman.ModExp(g, random, p);

            //// 47 ms
            //var result3 = DiffieHellman.mpir_ModExp(g, random, p);

        }

        [TestMethod]
        public void TestChallenge33()
        {
            var p = new BigInteger(37);
            var g = new BigInteger(5);
            var dh = new DiffieHellman(p, g, BigInteger.ModPow);

            Assert.IsTrue(dh.IsValid);

            // OBS: you must prepend a 0 to the hex representation, lest the integer becomes negative
            // The reason is in the answer to https://stackoverflow.com/questions/30119174/converting-a-hex-string-to-its-biginteger-equivalent-negates-the-value
            var biggerP = "0ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024" +
                            "e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd" +
                            "3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec" +
                            "6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f" +
                            "24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361" +
                            "c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552" +
                            "bb9ed529077096966d670c354e4abc9804f1746c08ca237327fff" +
                            "fffffffffffff";

            var bignum = BigInteger.Parse(biggerP, System.Globalization.NumberStyles.HexNumber);

            var biggerDh = new DiffieHellman(bignum, 2, DiffieHellman.ModExp);

            Assert.IsTrue(biggerDh.IsValid);
        }

        [TestMethod]
        public void Challenge34NormalProtocol()
        {
            var p = new BigInteger(37);
            var g = new BigInteger(5);
            var dh = new DiffieHellman(p, g, BigInteger.ModPow);

            // A -> B "send A"
            dh.bob.GenerateSecret(dh.alice.A);

            // B -> A "send B"
            dh.alice.GenerateSecret(dh.bob.B);

            // A -> B
            var message = "YELLOW SUBMARINE";
            var encrypted = dh.alice.Encrypt(message);
            var response = dh.bob.Echo(encrypted);

            var secret = SHA1.Hash(dh.bob.GetSecret(dh.alice.A).ToByteArray()).
                            Take(16).ToArray();
            var responseAscii = Encoding.ASCII.GetString(AESWrapper.CBC.Decrypt(response.Data, secret, response.Iv));

            Assert.IsTrue(responseAscii == message);
        }


    }
}
