My take on Matasano Cryptochallenges http://cryptopals.com/

Done in C# to brush up on the language.

My take is different from most - rather than making Challenge1.cs, challenge2.sc,..
I've tried to make the solution(s) more general purpose by splitting 
the tools needed for the challenges into appropriate classes. (Analysis, Operations, 
Converter, AESWrapper, etc..) 

The overall structure is still evident in the testclasses for the
different challenge sets. 